from django.urls import path, re_path

from lti.views import lti_init

app_name = 'lti'

urlpatterns = [
    re_path(r'^$', lti_init, name='lti_init'),
    path('<uuid:tool_pk>/', lti_init, name='lti_init'),
]
