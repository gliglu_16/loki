import pytest
from django.db import IntegrityError

from lti.utils import (create_loki_user, hash_lti_user_data,
                       key_secret_generator)


def test_key_secret_generator(mocker):
    """
    Test LTI key secret generation
    """

    settings_mock = mocker.patch('lti.utils.settings')
    settings_mock.SECRET_KEY = 'testSecret_/Key%'

    value1 = key_secret_generator()
    value2 = key_secret_generator()

    assert value1 != value2


@pytest.mark.django_db
def test_create_loki_user():
    """
    Test basic LTI user creation.
    """

    user1 = create_loki_user()
    user2 = create_loki_user()

    assert user1 != user2


@pytest.mark.django_db
def test_integrity_error(mocker):
    """
    Test user creation (ensure IntegrityError occurs).
    """

    create = mocker.patch('lti.utils.User.objects.create_user')

    abs_user = mocker.Mock()
    abs_user2 = mocker.Mock()
    create.side_effect = [IntegrityError, abs_user, abs_user2]

    assert create_loki_user() == abs_user
    assert create.call_count == 2
    create.reset_mock()
    create.side_effect = [IntegrityError, abs_user, abs_user2]

    assert create_loki_user() != abs_user2


@pytest.mark.django_db
@pytest.mark.parametrize("user_id, tool_consumer_instance_guid, lis_person_sourcedid", [
    ('1', 'tool_consumer_instance_guid', 'lis_person_sourcedid'),
    ('2', 'tool_consumer_instance_guid', 'lis_person_sourcedid'), #next time make blank id
    ('3', '', 'lis_person_sourcedid'),
    ('4', 'tool_consumer_instance_guid', ''),
    ('5', '', ''),
])
def test_hash_lti_user_data(user_id, tool_consumer_instance_guid, lis_person_sourcedid):
    """
    Test LTI user data hashing.
    """

    new_user = hash_lti_user_data(user_id, tool_consumer_instance_guid, lis_person_sourcedid)
    assert len(new_user) == 30
    assert isinstance(new_user, str)
