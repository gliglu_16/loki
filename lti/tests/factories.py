import factory

from lti import models


class LTIConsumerFactory(factory.DjangoModelFactory):
    """
    LTIConsumer model factory for tests.
    """
    class Meta:
        model = models.LTIConsumer
        django_get_or_create = (
            "consumer_name",
            "consumer_key",
            "consumer_secret",
            "instance_guid",
        )

    consumer_name = 'test'
    consumer_key = 'consumer_key'
    consumer_secret = 'test_key'
    instance_guid = 'zzzd8cd98f00b204e9800998ecf8427e'


class LTIUserFactory(factory.DjangoModelFactory):
    """
    LTIUser model factory for tests, factory is not linked to django user.
    """
    class Meta:
        model = models.LTIUser
        django_get_or_create = (
            "django_user",
            "lti_consumer",
        )

    user_id = 'd41d8cd98f00b204e9800998ecf8427e'
    lti_consumer = factory.SubFactory(LTIConsumerFactory)
    django_user = None
