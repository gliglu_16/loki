# coding=utf-8
import uuid
from datetime import date, timedelta

import oauth2
from ddt import data, ddt
from django.contrib.auth.models import User
from django.test import Client, TestCase
from mock import Mock, patch

from drawing_tool.tests.factories import DrawingToolFactory
from loki.constants import INSTRUCTOR
from lti.models import LTIUser
from lti.utils import hash_lti_user_data

from .factories import LTIConsumerFactory, LTIUserFactory


class LTITestCase(TestCase):
    """
    Base class for LTI related tests.
    """

    def setUp(self):
        """
        Initialize all necessary variables.
        """
        self.client = Client()

        mocked_nonce = '135685044251684026041377608307'
        mocked_timestamp = '1234567890'
        mocked_decoded_signature = 'my_signature='
        self.headers = {
            'user_id': 'd41d8cd98f00b204e9800998ecf8427e',
            'lis_person_name_full': 'Test Username',
            'lis_person_name_given': 'First',
            'lis_person_name_family': 'Second',
            'lis_person_contact_email_primary': 'test@test.com',
            'lis_person_sourcedid': 'Test_Username',
            'launch_presentation_locale': 'en',
            'oauth_callback': 'about:blank',
            'launch_presentation_return_url': '',
            'lti_message_type': 'basic-lti-launch-request',
            'lti_version': 'LTI-1p0',
            'roles': 'Student',
            'context_id': 1,
            'tool_consumer_info_product_family_code': 'moodle',
            'context_title': 'Test title',
            'tool_consumer_instance_guid': 'test.dot.com',

            'resource_link_id': 'dfgsfhrybvrth',
            'lis_result_sourcedid': 'wesgaegagrreg',

            'oauth_nonce': mocked_nonce,
            'oauth_timestamp': mocked_timestamp,
            'oauth_consumer_key': 'consumer_key',
            'oauth_signature_method': 'HMAC-SHA1',
            'oauth_version': '1.0',
            'oauth_signature': mocked_decoded_signature
        }

        self.lti_consumer = LTIConsumerFactory.create()

        username = hash_lti_user_data(
            self.headers['user_id'],
            self.lti_consumer.consumer_key,
            self.headers['lis_person_sourcedid'],
        )
        self.user = User.objects.create_user(username, 'test@test.com', 'test')


@patch('lti.views.DjangoToolProvider')
class LTIInitMethodsTest(LTITestCase):
    """
    Test correct request method is passed in view and preconditions are observed.
    """

    def test_post(self, mocked):
        """
        Test templates are accessible for valid request.

        Student can access tool after instructor created it.
        """
        mocked.return_value.is_valid_request.return_value = True

        post_data = self.headers.copy()
        post_data['roles'] = INSTRUCTOR
        response = self.client.post('/lti/', data=post_data, follow=True)
        self.assertTemplateUsed(response, template_name='frontend/index.html')

        response = self.client.post('/lti/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, template_name='frontend/index.html')

    def test_failure_post(self, mocked):
        """
        Test error template is used for invalid request.
        """
        mocked.return_value.is_valid_request.return_value = False
        response = self.client.post('/lti/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')

        post_data = self.headers.copy()
        post_data['roles'] = INSTRUCTOR
        response = self.client.post('/lti/', data=post_data, follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')

    def test_get(self, mocked):
        """
        Test error template is used for GET request.
        """
        mocked.return_value.is_valid_request.return_value = True
        response = self.client.get('/lti/', follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')

    def test_tool_not_created(self, mocked):
        """
        Test student can't access tool if instructor didn't create it.
        """
        mocked.return_value.is_valid_request.return_value = True
        response = self.client.post('/lti/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')


@patch('lti.views.DjangoToolProvider')
class LTIInitDrawingToolDirectTest(LTITestCase):
    """
    Test for correct uuid passed in url.
    """

    def setUp(self):
        """
        Create a test drawing_tool instance.
        """
        super().setUp()
        self.drawing_tool = DrawingToolFactory.create(lti_consumer=self.lti_consumer)
        self.headers['roles'] = INSTRUCTOR

    def test_valid_uuid(self, mocked):
        """
        Test instructor can directly access drawing_tool if all preconditions are obsered.
        """
        mocked.return_value.is_valid_request.return_value = True
        response = self.client.post(
            f'/lti/{self.drawing_tool.uuid}/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, template_name='frontend/index.html')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(INSTRUCTOR in self.client.session["ROLES"])

    def test_uuid_tool_not_found(self, mocked):
        """
        Test instructor can't access drawing_tool if a non-existing UUID is passed.
        """
        mocked.return_value.is_valid_request.return_value = True

        not_db_uuid = uuid.uuid4()
        response = self.client.post(f'/lti/{not_db_uuid}/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')
        self.assertEqual(response.status_code, 200)

    def test_invalid_uuid(self, mocked):
        """
        Test instructor can't access drawing_tool with wrong UUID.
        """
        mocked.return_value.is_valid_request.return_value = True

        invalid_uuid = str(self.drawing_tool.uuid)[:-1]
        response = self.client.post(f'/lti/{invalid_uuid}/', data=self.headers, follow=True)
        self.assertEqual(response.status_code, 404)

    def test_success_student(self, mocked):
        """
        Test the LTI init flow for a student.
        """
        mocked.return_value.is_valid_request.return_value = True

        del self.headers['roles']  # The goal is to remove `INSTRUCTOR` from roles

        response = self.client.post(
            f'/lti/{self.drawing_tool.uuid}/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, template_name='frontend/index.html')
        self.assertEqual(response.status_code, 200)
        self.assertFalse(INSTRUCTOR in self.client.session["ROLES"])


@ddt
@patch('lti.views.DjangoToolProvider')
class LTIInitParamsTest(LTITestCase):
    """
    Test different params handling when LTI initializing.
    """

    def test_user_id_not_provided(self, mocked):
        """
        Test required param user_id isn't passed in lti_init request.
        """
        del self.headers['user_id']
        mocked.return_value.is_valid_request.return_value = True
        response = self.client.post('/lti/',
                                    data=self.headers,
                                    follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')

    def test_invalid_user_id(self, mocked):
        """
        Test required param user_id not in the form of edx user_id.
        """
        self.headers['user_id'] = '1'
        mocked.return_value.is_valid_request.return_value = True
        response = self.client.post('/lti/',
                                    data=self.headers,
                                    follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')

    def test_lti_user_creation_success(self, mocked):
        """
        Default LTI user creation process.
        """
        mocked.return_value.is_valid_request.return_value = True
        self.client.post('/lti/', data=self.headers, follow=True)
        self.assertTrue(
            LTIUser.objects.filter(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"],
            ).exists())
        self.assertEqual(
            LTIUser.objects.get(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"],
            ).django_user,
            self.user
        )

    def test_lti_users_same_full_name(self, mocked):
        """
        Test user creation w/ the same `lis_person_name_full`.
        """
        mocked.return_value.is_valid_request.return_value = True

        # Link LtiUser to Django user by email
        self.client.post('/lti/', data=self.headers, follow=True)
        self.assertEqual(
            self.user, LTIUser.objects.get(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"],
            ).django_user
        )

    @patch('lti.models.hash_lti_user_data')
    def test_lti_user_unicode_username(self, hash_lti_user_data, mocked):
        """
        Test unicode full name from LTI.
        """
        mocked.return_value.is_valid_request.return_value = True
        hashvalue = 'somehashvalue'
        hash_lti_user_data.return_value = hashvalue
        self.headers['user_id'] = '9e107d9d372bb6826bd81d3542a419d6'
        self.headers['lis_person_contact_email_primary'] = 'new_email@mail.com'
        self.headers['lis_person_name_full'] = 'きつね'
        self.client.post('/lti/', data=self.headers, follow=True)

    def test_lti_user_no_email(self, mocked):
        """
        Test that LTI inits just fine if no email is passed.
        """
        del self.headers['lis_person_contact_email_primary']
        mocked.return_value.is_valid_request.return_value = True
        self.client.post('/lti/',
                         data=self.headers,
                         follow=True)
        self.assertTrue(
            LTIUser.objects.filter(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"]
            ).exists()
        )
        self.assertEqual(
            LTIUser.objects.get(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"]
            ).django_user,
            User.objects.get(id=self.user.id)
        )

    @patch('lti.models.hash_lti_user_data')
    def test_lti_user_no_username_no_email(self, hash_lti_user_data, mocked):
        """
        Test for non-existent username field.

        If there is no username in POST
        we create user with random username.
        """
        test_random_username = 'c'*32

        del self.headers['lis_person_name_full']
        del self.headers['lis_person_contact_email_primary']
        mocked.return_value.is_valid_request.return_value = True
        hash_lti_user_data.return_value = test_random_username[:30]

        self.client.post('/lti/', data=self.headers, follow=True)
        self.assertTrue(
            LTIUser.objects.filter(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"],
            ).exists()
        )
        self.assertNotEqual(
            LTIUser.objects.get(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"],
            ).django_user,
            User.objects.get(id=self.user.id)
        )
        self.assertEqual(
            LTIUser.objects.get(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"],
            ).django_user.username,
            test_random_username[:30]
        )
        self.assertEqual(
            len(LTIUser.objects.get(
                lti_consumer=self.lti_consumer,
                user_id=self.headers["user_id"],
            ).django_user.username),
            30
        )

    def test_lti_user_additional_data(self, mocked):
        """
        Test a case when all additional user data is provided.

        Additional user data:

        #. user_username
        #. user_email
        #. user_language
        #. user_fullname
        """
        mocked.return_value.is_valid_request.return_value = True
        self.client.post('/lti/', data=self.headers, follow=True)
        self._check_lti_user_additional_data()

    def test_lti_user_additional_data_update(self, mocked):
        """
        Test additional user data update cases.

        Cases:

        #. LTI user data arrive updated (all at once) - new data gets stored.
        #. LTI user data gets removed in the campus system - old data is preserved.
        #. sub-case of data removal: empty values are passed - old data is preserved.

        """
        mocked.return_value.is_valid_request.return_value = True

        self.headers['lis_person_name_full'] = 'Test Username Updated'
        self.headers['lis_person_contact_email_primary'] = 'test_updated@test.com'
        self.headers['lis_person_sourcedid'] = 'Test_Username_Updated'
        self.headers['launch_presentation_locale'] = 'uk'
        self.client.post('/lti/', data=self.headers, follow=True)
        self._check_lti_user_additional_data()

        del self.headers['lis_person_name_full']
        del self.headers['lis_person_contact_email_primary']
        del self.headers['lis_person_sourcedid']
        del self.headers['launch_presentation_locale']
        self.client.post('/lti/', data=self.headers, follow=True)
        lti_user = LTIUser.objects.get(
            lti_consumer=self.lti_consumer,
            user_id=self.headers["user_id"],
        )
        self.assertIsNotNone(lti_user.user_username)
        self.assertIsNotNone(lti_user.user_email)
        self.assertIsNotNone(lti_user.user_language)
        self.assertIsNotNone(lti_user.user_fullname)

        self.headers['lis_person_name_full'] = ''
        self.headers['lis_person_contact_email_primary'] = ''
        self.headers['lis_person_sourcedid'] = ''
        self.headers['launch_presentation_locale'] = ''
        self.client.post('/lti/', data=self.headers, follow=True)
        lti_user = LTIUser.objects.get(
            lti_consumer=self.lti_consumer,
            user_id=self.headers["user_id"],
        )
        self.assertNotEqual(lti_user.user_username, '')
        self.assertNotEqual(lti_user.user_email, '')
        self.assertNotEqual(lti_user.user_language, '')
        self.assertNotEqual(lti_user.user_fullname, '')

    def _check_lti_user_additional_data(self):
        """
        Utility function to test LTI user's additional data.

        Compliance with input data (`self.headers` from `self.setUp()`) is checked.
        """
        lti_user = LTIUser.objects.get(
            lti_consumer=self.lti_consumer,
            user_id=self.headers["user_id"],
        )
        self.assertEqual(lti_user.user_username, self.headers.get("lis_person_sourcedid"))
        self.assertEqual(lti_user.user_email, self.headers.get("lis_person_contact_email_primary"))
        self.assertEqual(lti_user.user_language, self.headers.get("launch_presentation_locale"))
        self.assertEqual(lti_user.user_fullname, self.headers.get("lis_person_name_full"))


@ddt
@patch('lti.views.DjangoToolProvider')
class LTIInitExceptionTest(LTITestCase):
    """
    Test raising exception.
    """
    @data(oauth2.MissingSignature, oauth2.Error, KeyError, AttributeError)
    def test_exceptions(self, exception, mocked):
        """
        Test exceptions.
        """
        mocked.return_value.is_valid_request.side_effect = exception()
        response = self.client.get('/lti/', follow=True)
        self.assertTemplateUsed(response, template_name='lti/error.html')


class LTIModelTest(LTITestCase):
    """
    Test model LTIUser.
    """

    def test_lti_user_create_links(self):
        """
        Creating LTIUser without Django user.

        Testing Django user creation process.
        """
        lti_user = LTIUserFactory(user_id=str(self.user.id),
                                  lti_consumer=self.lti_consumer)

        lti_user.save()

        self.assertFalse(lti_user.is_linked)
        lti_user.create_links(self.headers)
        self.assertTrue(lti_user.is_linked)


class LTIInitAcceptanceTests(LTITestCase):
    """
    Acceptance test to check different flows of handling LTI requests.
    """

    def test_expired_consumer(self):
        """
        Checking that expired consumer will not be used.
        """
        response = self.client.post('/lti/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, 'lti/error.html')

    @patch('lti.views.LTIConsumer.objects.filter')
    def test_short_term_consumer(self, mocked_consumer):
        """
        Test that user w/ short_term flag will be treated correctly.
        """
        self.headers['custom_short_term'] = 'true'
        lti_consumer_mock = Mock()
        lti_consumer_mock.expiration_date = date.today() + timedelta(days=1)
        first_mock = Mock()
        first_mock.first.return_value = lti_consumer_mock
        mocked_consumer.return_value = first_mock
        self.client.post('/lti/', data=self.headers, follow=True)
        mocked_consumer.assert_called_once_with(
            consumer_key=self.headers['oauth_consumer_key']
        )

    @patch('lti.views.LTIConsumer.get_or_combine')
    def test_typical_consumer(self, mocked_consumer):
        """
        Typical LTi request (w/o short_term flag) will be treated w/ get_or_combine.
        """
        lti_consumer_mock = Mock()
        lti_consumer_mock.expiration_date = date.today() + timedelta(days=1)
        mocked_consumer.return_value = lti_consumer_mock
        self.client.post('/lti/', data=self.headers, follow=True)
        mocked_consumer.assert_called_once_with(
            self.headers['tool_consumer_instance_guid'],
            self.headers['oauth_consumer_key'],
        )

    def test_no_consumer_found(self):
        """
        If there is no LTIConsumer found throw error.
        """
        self.lti_consumer.delete()
        response = self.client.post('/lti/', data=self.headers, follow=True)
        self.assertTemplateUsed(response, 'lti/error.html')
