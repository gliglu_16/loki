from django.contrib import admin
from django.forms.models import ModelForm

from lti.models import LTIConsumer, LTIUser


class LtiConsumerForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LtiConsumerForm, self).__init__(*args, **kwargs)

        class Meta:
            model = LTIConsumer

    def clean(self):
        cleaned_data = self.cleaned_data
        instance_guid = cleaned_data.get('instance_guid')

        if not instance_guid:
            cleaned_data['instance_guid'] = None

        return cleaned_data


class LTIUserAdmin(admin.ModelAdmin):
    """
    Admin for LTI User.
    """
    list_display = ('user_id', 'lti_consumer', 'django_user')


class LtiConsumerAdmin(admin.ModelAdmin):
    """
    Admin for LTI Consumer.
    """
    form = LtiConsumerForm

    search_fields = ('consumer_name', 'consumer_key', 'instance_guid', 'expiration_date')
    list_display = ('consumer_name', 'consumer_key', 'instance_guid', 'expiration_date')


admin.site.register(LTIUser, LTIUserAdmin)
admin.site.register(LTIConsumer, LtiConsumerAdmin)
