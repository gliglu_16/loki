from django.contrib.auth import login
from django.contrib.auth.models import User
from django.db import models
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext as _

from lti.utils import hash_lti_user_data, key_secret_generator


class LTIConsumer(models.Model):
    """
    Model to manage LTI consumers.
    """
    consumer_name = models.CharField(max_length=255, unique=True)
    consumer_key = models.CharField(max_length=64, unique=True,
                                    db_index=True, default=key_secret_generator)
    consumer_secret = models.CharField(max_length=64, unique=True, default=key_secret_generator)
    instance_guid = models.CharField(max_length=255, blank=True, null=True, unique=True)
    expiration_date = models.DateField(
        verbose_name='Consumer Key expiration date', null=True, blank=True)

    @staticmethod
    def get_or_combine(instance_guid, consumer_key):
        """
        Search for LTIConsumer instance by `instance_guid`.

        LTIConsumer will be searched by `consumer_key`.
        Also `instance_guid` will be added to found by `consumer_key`
        instance.
        """
        consumer = None

        if consumer_key:
            consumer = LTIConsumer.objects.filter(
                consumer_key=consumer_key
            ).first()

        if not consumer:
            return None

        if instance_guid and not consumer.instance_guid:
            consumer.instance_guid = instance_guid
            consumer.save()
        return consumer

    def __str__(self):
        return self.consumer_name


class LTIUser(models.Model):
    """
    Model for LTI user.

    **Fields:**

      .. attribute:: user_id
      uniquely identifies the user within LTI Consumer

      .. attribute:: lti_consumer
      uniquely identifies the Tool Consumer

      .. attribute:: django_user
      Django user to store study progress
    """
    user_id = models.CharField(max_length=255, blank=False)
    user_username = models.CharField(max_length=255, blank=True, null=True)
    user_email = models.CharField(max_length=255, blank=True, null=True)
    user_language = models.CharField(max_length=255, blank=True, null=True)
    user_fullname = models.CharField(max_length=255, blank=True, null=True)
    lti_consumer = models.ForeignKey(LTIConsumer, null=True, on_delete=models.CASCADE)
    django_user = models.ForeignKey(
        User, null=True, on_delete=models.CASCADE, related_name='lti_auth')

    class Meta:  # pragma: no cover
        unique_together = ('user_id', 'lti_consumer')

    @staticmethod
    def get_lti_user_or_404(user_id, lti_consumer_id=None, tool=None):
        """
        Fetch lti user object.

        Arguments:
            lti_consumer_id (str): LTI consumer id.
            user_id (str): user id provided by a campus system in the
                LTI init flow.
            tool (`drawing_tool.models.DrawingTool` obj)

        Returns:
            lti_user (`LTIUser` obj)
        """
        lti_consumer = tool.lti_consumer if tool else get_object_or_404(
            LTIConsumer,
            pk=lti_consumer_id,
        )
        lti_user = get_object_or_404(
            LTIUser,
            user_id=user_id,
            lti_consumer=lti_consumer,
        )
        return lti_user

    def create_links(self, request_data=None):
        """
        Create all needed links to Django.
        """
        if request_data:
            extra_data = request_data
        else:
            saved_data = self.tool_data.filter(extra_data__is_null=False).first()
            extra_data = saved_data.extra_data if saved_data else {}

        first_name = extra_data.get('lis_person_name_given', '')
        last_name = extra_data.get('lis_person_name_family', '')
        email = extra_data.get('lis_person_contact_email_primary', '').lower()

        defaults = {
            'first_name': first_name,
            'last_name': last_name,
            'email': email
        }

        username = hash_lti_user_data(
            self.user_id,
            self.lti_consumer.consumer_key,
            extra_data.get('lis_person_sourcedid', '')
        )
        django_user, _ = User.objects.get_or_create(
            username=username, defaults=defaults
        )

        self.django_user = django_user
        self.save()

    def login(self, request):
        """
        Login linked Django user.
        """
        if self.django_user:
            self.django_user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, self.django_user)

    @property
    def is_linked(self):
        """
        Check link to some Django user.
        """
        return bool(self.django_user)
