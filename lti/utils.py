import hashlib
import logging
from functools import wraps
from uuid import uuid4

from django.conf import settings
from django.contrib.auth.models import User
from django.db import IntegrityError, transaction
from django.core.exceptions import PermissionDenied

from loki.constants import INSTRUCTOR


LOGGER = logging.getLogger(__name__)


def only_lti(fn):
    """
    Decorator ensures that user comes from LTI session.
    """
    @wraps(fn)
    def wrapped(request, *args, **kwargs):
        try:
            _ = request.session['LTI_POST']
        except KeyError:
            LOGGER.error('Lti session is not found, Request cannot be processed')
            raise PermissionDenied("Course content is available only through LTI protocol.")
        else:
            return fn(request, *args, **kwargs)
    return wrapped


def instructor_only(fn):
    """
    Ensure a user is instructor if required.
    """

    @wraps(fn)
    def wrapped(request, *args, **kwargs):
        if kwargs.get("is_instructor"):
            if INSTRUCTOR in request.session.get('ROLES', ()):
                return fn(request, *args, **kwargs)
            LOGGER.error('A user is not an instructor, request cannot be processed')
            raise PermissionDenied("A user must hold an instructor role.")
        return fn(request, *args, **kwargs)

    return wrapped


def create_loki_user():
    """
    Create Loki user w/ random username.

    We can't trust LTI request w/o email in details.
    Using random username we need to check for
    IntegrityError DB exception to avoid race condition.
    """
    password = str(uuid4())

    created = False
    while not created:
        try:
            username = uuid4().hex[:30]
            with transaction.atomic():
                loki_user = User.objects.create_user(
                    username=username,
                    password=password,
                )
            created = True
        except IntegrityError:
            pass

    return loki_user


def key_secret_generator():
    """
    Generate a key/secret for LTIConsumer.
    """
    sha = hashlib.sha1(bytes(uuid4().hex, 'latin-1'))
    sha.update(bytes(settings.SECRET_KEY, 'latin-1'))
    return sha.hexdigest()[::2]


def hash_lti_user_data(user_id, tool_consumer_key, lis_person_sourcedid):
    """
    Create unique ID for Django user based on user.
    """
    sha = hashlib.new('ripemd160')
    sha.update(user_id.encode('utf-8'))
    sha.update(tool_consumer_key.encode('utf-8'))
    sha.update(lis_person_sourcedid.encode('utf-8'))
    return sha.hexdigest()[:30]
