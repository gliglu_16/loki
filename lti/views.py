import logging
import re
from datetime import date

import oauth2
from django.shortcuts import redirect, render
from django.utils.safestring import mark_safe
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from ims_lti_py.tool_provider import DjangoToolProvider

from drawing_tool.models import DrawingTool, DrawingToolLTIData, StudentResult
from loki.constants import (
    INSTRUCTOR,
    LTI_ROLES_MAP,
    STUDENT,
)
from lti import app_settings as settings
from lti.models import LTIConsumer, LTIUser
from lti.utils import only_lti


LOGGER = logging.getLogger(__name__)


@xframe_options_exempt
@csrf_exempt
def lti_init(request, tool_pk=None):
    """
    LTI init view.

    Analyze LTI POST request to start LTI session.
    """
    if settings.LTI_DEBUG:
        LOGGER.debug(request.META)
        LOGGER.debug(request.POST)
    session = request.session
    session.clear()

    instance_guid = request.POST.get('tool_consumer_instance_guid')
    consumer_key = request.POST.get('oauth_consumer_key')

    lti_consumer = LTIConsumer.get_or_combine(instance_guid, consumer_key)

    if not lti_consumer:
        LOGGER.error('Consumer with key {} was not found.'.format(consumer_key))
        return render(request, 'lti/error.html', {'message': 'LTI consumer not found'})

    try:
        if lti_consumer.expiration_date and lti_consumer.expiration_date < date.today():
            raise oauth2.Error('Consumer Key has expired.')
        if lti_consumer.consumer_key != consumer_key:
            raise oauth2.Error('Wrong Consumer Key: {}'.format(consumer_key))
        consumer_key = lti_consumer.consumer_key
        secret = lti_consumer.consumer_secret
        tool = DjangoToolProvider(consumer_key, secret, request.POST)
        is_valid = tool.is_valid_request(request)
        session['target'] = '_blank'
    except (oauth2.MissingSignature,
            oauth2.Error,
            KeyError,
            AttributeError) as err:
        is_valid = False
        session['message'] = "{}".format(err)
        LOGGER.error("Error during processing LTI request: {}".format(err.__str__()))

    session['is_valid'] = 'is_valid'
    session['LTI_POST'] = request.POST

    if settings.LTI_DEBUG:
        msg = 'session: is_valid = {}'.format(session.get('is_valid'))
        LOGGER.debug(msg)
        if session.get('message'):
            msg = 'session: message = {}'.format(session.get('message'))
            LOGGER.debug(msg)
    if not is_valid:
        return render(request, 'lti/error.html', {'message': 'LTI request is not valid'})
    return lti_redirect(request, lti_consumer, tool_pk)


@only_lti
@xframe_options_exempt
def lti_redirect(request, lti_consumer, tool_uuid):
    """
    Create user and redirect to a tool

    |  Create LTIUser with all needed link to Django user
    |  Login Django user
    |  Finally redirect to a tool page according to the requested role
    """
    request_dict = request.session['LTI_POST']

    user_id = str(request_dict.get('user_id'))
    roles_from_request = request_dict.get('roles', '').split(',')
    roles = list(set((LTI_ROLES_MAP.get(role, STUDENT) for role in roles_from_request)))
    request.session['ROLES'] = roles
    resource_link_id = request_dict.get('resource_link_id', '')
    lis_outcome_service_url = request_dict.get('lis_outcome_service_url', '')
    lis_result_sourcedid = request_dict.get('lis_result_sourcedid', '')

    context = dict()
    if not user_id or not re.match(r"([a-fA-F\d]{32})", user_id):
        context['message'] = mark_safe(
            'Required parameter user_id is not provided.<br>Try to access LTI tool from LMS'
        )
        return render(request, 'lti/error.html', context)

    user, _ = LTIUser.objects.get_or_create(
        user_id=user_id,
        lti_consumer=lti_consumer,
    )
    if not user.is_linked:
        user.create_links(request_dict)
    user.login(request)

    user_fields = {
        'user_username': request_dict.get('lis_person_sourcedid'),
        'user_email': request_dict.get('lis_person_contact_email_primary'),
        'user_language': request_dict.get('launch_presentation_locale'),
        'user_fullname': request_dict.get('lis_person_name_full'),
    }
    significant_user_fields = {k: v for k, v in user_fields.items() if v}
    if significant_user_fields:
        LTIUser.objects.filter(
            user_id=user_id,
            lti_consumer=lti_consumer
        ).update(**significant_user_fields)

    query_tool = dict(
        lti_consumer=lti_consumer,
    )
    if tool_uuid:
        try:
            query_tool['uuid'] = tool_uuid
            drawing_tool = DrawingTool.objects.get(**query_tool)
        except DrawingTool.DoesNotExist:
            context['message'] = 'Tool UUID is not valid'
            return render(request, 'lti/error.html', context)
    else:
        try:
            query_tool['resource_link_id'] = resource_link_id
            drawing_tool = DrawingTool.objects.get(**query_tool)
        except DrawingTool.DoesNotExist:
            drawing_tool = None

    if INSTRUCTOR in roles:
        if not drawing_tool:
            drawing_tool = DrawingTool.objects.create(
                display_name='Change display name...',
                question='Enter instructions here...',
                **query_tool
            )
        lti_data, _ = DrawingToolLTIData.objects.get_or_create(
            tool=drawing_tool,
            lti_user=user,
            instructor_role=True
        )
        lti_data.extra_data = request_dict
        lti_data.save()
        request.session['TOOL_UUID'] = str(drawing_tool.uuid)
        request.session['USER_ID'] = str(user_id)
        request.session['LTI_CONSUMER_ID'] = str(lti_consumer.id)
        return redirect('frontend:instructor-index')

    if not drawing_tool:
        context['message'] = 'Tool does not exist'
        return render(request, 'lti/error.html', context)

    try:
        student_result = StudentResult.objects.get(tool=drawing_tool, lti_user=user)
        student_result.lis_outcome_service_url = lis_outcome_service_url
        student_result.lis_result_sourcedid = lis_result_sourcedid
        student_result.save()
    except StudentResult.DoesNotExist:
        student_result = StudentResult(
            tool=drawing_tool,
            lti_user=user,
            lis_outcome_service_url=lis_outcome_service_url,
            lis_result_sourcedid=lis_result_sourcedid,
        )
        student_result.save()

    request.session['TOOL_UUID'] = str(drawing_tool.uuid)
    request.session['USER_ID'] = str(user_id)
    request.session['LTI_CONSUMER_ID'] = str(lti_consumer.id)

    return redirect('frontend:student-index')
