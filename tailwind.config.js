module.exports = {
  theme: {
    extend: {
      container: {
        center: true,
      },
    },
    maxWidth: {
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
    },
    minHeight: {
      half: '50vh',
    },
    height: {
      sm: '8px',
      md: '48px',
      lg: '256px',
      xl: '400px',
      auto: 'auto',
      full: '100%',
      screen: '100vh',
      4: '1rem',
      40: '10rem',
    },
  },
  variants: {},
  plugins: [],
}
