"""
Celery setup.
"""

from datetime import timedelta
import os

import django
from celery import Celery
from django.conf import settings

django.setup()

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'loki.settings.base')

app = Celery('loki')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

# NOTE: `CELERYBEAT_SCHEDULE` setting doesn't work for now
app.conf.beat_schedule = {
    'send_outcomes': {
        'task': 'drawing_tool.tasks.send_outcomes',
        'schedule': timedelta(seconds=5),
    },
}
