class SameSiteMiddleware:
    """
    Middleware to add a `SameSite=None` attribute to the session cookie for
    `webapp` responses. This is a work-around in-place of
    :class:`django.http.HttpResponse.set_cookie`, which does not
    allow `samesite` values of ``None``.
    Add this class to Django app's ``settings.MIDDLEWARE``.
    Specifically, the class must be listed `above`
    :class:`django.contrib.sessions.middleware.SessionMiddleware`

    Fix: https://github.com/django/django/pull/11894/files
    Boilerplate code from:
    https://docs.djangoproject.com/en/2.2/topics/http/middleware
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        from django.conf import settings

        if settings.CSRF_COOKIE_NAME in response.cookies and request.session.get('LTI_POST'):
            response.cookies[settings.CSRF_COOKIE_NAME]["samesite"] = "None"
            response.cookies[settings.CSRF_COOKIE_NAME]["secure"] = True
        if settings.SESSION_COOKIE_NAME in response.cookies and request.session.get('LTI_POST'):
            response.cookies[settings.SESSION_COOKIE_NAME]["samesite"] = "None"
            response.cookies[settings.SESSION_COOKIE_NAME]["secure"] = True

        return response


import_path = "loki.middleware.SameSiteMiddleware"
