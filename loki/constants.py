"""
Project constants related to business logic.
"""

INSTRUCTOR = "Instructor"
STUDENT = "Student"

LTI_ROLES_MAP = {
    'Instructor': INSTRUCTOR,
    'Administrator': INSTRUCTOR,
    'Staff': INSTRUCTOR,
    'Learner': STUDENT,
}
