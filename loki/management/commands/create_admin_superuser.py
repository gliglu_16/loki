import os

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

import loki.settings.test as settings


class Command(BaseCommand):
    help = 'Create superuser with provided data or with defaults for DEV environment.'


    def add_arguments(self, parser):
        parser.add_argument('--email', type=str,
                            help="Specifies the email for the superuser.")
        parser.add_argument('--username', type=str,
                            help="Specifies the username for the superuser.")
        parser.add_argument('--password', type=str,
                            help="Specifies the password for the superuser.")

    def handle(self, *args, **options):
        User = get_user_model()
        if os.getenv('env') in ['prod', 'stage'] and not options['password']:
            raise CommandError("Please specify a password for the superuser.")
        email = options['email'] or settings.DEV_SUPERUSER_EMAIL
        username = options['username'] or settings.DEV_SUPERUSER_USERNAME
        password = options['password'] or settings.DEV_SUPERUSER_PASSWORD
        if not User.objects.filter(email=email).exists():
            User.objects.create_superuser(
                username=username,
                email=email,
                password=password,
            )
        self.stdout.write(self.style.SUCCESS(
            f"!!! Pay attention: New superuser {username} is active !!!"
        ))
