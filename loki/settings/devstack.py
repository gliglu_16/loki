from loki.settings.base import *

DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG


ALLOWED_HOSTS = ['*']

# LTI Parameters
LTI_DEBUG = True

debug_toolbar_enabled = False
#try:
#    import debug_toolbar
#    INSTALLED_APPS += ('debug_toolbar',)
#    MIDDLEWARE += (
#        'debug_toolbar.middleware.DebugToolbarMiddleware',
#    )
#
#    DEBUG_TOOLBAR_PANELS = [
#        'debug_toolbar.panels.versions.VersionsPanel',
#        'debug_toolbar.panels.timer.TimerPanel',
#        'debug_toolbar.panels.settings.SettingsPanel',
#        'debug_toolbar.panels.headers.HeadersPanel',
#        'debug_toolbar.panels.request.RequestPanel',
#        'debug_toolbar.panels.sql.SQLPanel',
#        # 'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#        'debug_toolbar.panels.templates.TemplatesPanel',
#        'debug_toolbar.panels.cache.CachePanel',
#        'debug_toolbar.panels.signals.SignalsPanel',
#        'debug_toolbar.panels.logging.LoggingPanel',
#        'debug_toolbar.panels.redirects.RedirectsPanel',
#    ]
#    debug_toolbar_enabled = True
#except ImportError:
#    pass
