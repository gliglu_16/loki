"""This module defines only abstract models, they may be used as base models."""

import uuid

from django.db import models


def question_image_upload_path(instance, filename):
    """Define path to store question images."""
    return f'description/{instance.pk}/{filename}'


def background_image_upload_path(instance, filename):
    """Define path to store background images for method-draw."""
    return f'method_draw/{instance.pk}/{filename}'


class CommonTool(models.Model):
    """
    Common tool settings for one particular resource.
    """
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    lti_consumer = models.ForeignKey('lti.LTIConsumer', on_delete=models.CASCADE)
    resource_link_id = models.CharField(max_length=255, blank=False, unique=True)
    display_name = models.CharField(verbose_name='Display Name', max_length=255, blank=False)
    question = models.TextField(verbose_name='Instructions', blank=False)
    question_image = models.ImageField(
        verbose_name='Instructions Image',
        upload_to=question_image_upload_path,
        blank=True
    )
    hints = models.TextField(blank=True)
    enable_grading = models.BooleanField(verbose_name='Enable Grading', default=False)
    max_attempts = models.PositiveSmallIntegerField(
        verbose_name='Max Attempts',
        default=0,
        blank=True,
        null=True
    )
    background_image = models.ImageField(
        verbose_name='Background Image',
        upload_to=background_image_upload_path,
        blank=True
    )

    class Meta:
        abstract = True
