"""
Factories for comments' models.
"""
import factory

from comments import models
from drawing_tool.tests.factories import AnswerHistoryFactory, StudentResultFactory
from lti.tests.factories import LTIUserFactory


class CommentFactory(factory.DjangoModelFactory):
    """
    `Comment` model factory for tests.
    """

    class Meta:
        model = models.Comment

    lti_user = factory.SubFactory(LTIUserFactory)
    student_result = factory.SubFactory(StudentResultFactory)
    answer_history = factory.SubFactory(AnswerHistoryFactory)

    comment = "Like the drawing!"
