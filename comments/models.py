"""
Models for the `comments` app.
"""
from datetime import datetime

from django.contrib.auth.models import User
from django.db import models

from lti.models import LTIUser
from drawing_tool.models import AnswerHistory, StudentResult


class Comment(models.Model):
    """
    Comment model.
    """

    lti_user = models.ForeignKey(LTIUser, on_delete=models.CASCADE)
    student_result = models.ForeignKey(StudentResult, on_delete=models.CASCADE)
    answer_history = models.ForeignKey(AnswerHistory, on_delete=models.SET_NULL, blank=True, null=True)
    comment = models.TextField()
    submit_date = models.DateTimeField(default=datetime.now, editable=False)
