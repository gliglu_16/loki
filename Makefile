LOCAL_ENV := local
DEV_ENV := dev
PROD_ENV := prod

DEFAULT_APP_PWD = loki
DOCKER_COMPOSE = `which docker-compose`

APP = app

export DJANGO_ADMIN_PASSWORD
export DJANGO_ADMIN_EMAIL
export DJANGO_ADMIN_USERNAME
export APP_IMAGE

ifneq ($(filter $(env),$(STAGE_ENV) $(PROD_ENV)),)
	DOCKERCOMPOSE_PATH := docker-compose.prod.yml
else
	DOCKERCOMPOSE_PATH := docker-compose.yml
endif

sh:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) exec $(APP) bash

up:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) up -d

dev.build: build up .migrate .build_static_local collectstatic .createadmin

prod.up: up .migrate collectstatic .createadmin

prod.rebuild: stop rm rm_img up .migrate collectstatic

.createadmin:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) exec -T $(APP) \
		python manage.py create_admin_superuser --password $(DJANGO_ADMIN_PASSWORD) --email $(DJANGO_ADMIN_EMAIL) --username $(DJANGO_ADMIN_USERNAME)

start:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) start

restart:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) restart

stop:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) stop

rm:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) rm -f

rm_img:
	-docker image rm $(APP_IMAGE)

debug:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) run -T --service-ports $(APP)

build:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) build

.migrate:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) exec -T app \
		python manage.py migrate --no-input

test:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) exec -T $(APP) \
		bash -c \
		" \
		find . | grep -E \"(__pycache__|\.pyc|\.pyo$\)\" | xargs rm -rf && \
		pytest -s && \
		coverage xml && \
		diff-cover coverage.xml --fail-under=60 \
		"

makemigrations:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) exec -T $(APP) \
		python manage.py makemigrations --no-input

collectstatic:
	$(DOCKER_COMPOSE) -f $(DOCKERCOMPOSE_PATH) exec -T $(APP) \
		python manage.py collectstatic --no-input

.build_static_local:
	 npm install && npm run build:styles && npm run build

update_db: .migrate
