module.exports = {
    "env": {
        "es6": true,
        "node": true,
        "browser": true,
        "jest": true
    },
    "parser": "babel-eslint",
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:flowtype/recommended"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "flowtype",
        "react-hooks"
    ],
    "settings": {
        "version": "detect"
    },
    "rules": {

        //-------------------------------BASE---------------------------------------

        "semi": ["error", "never"],

        // require trailing commas in multiline object literals
        "comma-dangle": ["error", {
          arrays: "always-multiline",
          objects: "always-multiline",
          imports: "always-multiline",
          exports: "always-multiline",
          functions: "always-multiline",
        }],

        // enforce newline at the end of file, with no multiple empty lines
        "eol-last": ["error", "always"],

        // specify whether double or single quotes should be used in JSX attributes
        // https://eslint.org/docs/rules/jsx-quotes
        "jsx-quotes": ["error", "prefer-single"],

        // enforces spacing between keys and values in object literal properties
        "key-spacing": ["error", { beforeColon: false, afterColon: true }],

        // disallow mixed 'LF' and 'CRLF' as linebreaks
        // https://eslint.org/docs/rules/linebreak-style
        "linebreak-style": ["error", "unix"],

        // specify the maximum length of a line in your program
        // https://eslint.org/docs/rules/max-len
        "max-len": ["error", 99, 2, {
          ignoreUrls: true,
          ignoreComments: false,
          ignoreRegExpLiterals: true,
          ignoreStrings: true,
          ignoreTemplateLiterals: true,
        }],

        // disallow multiple empty lines and only one newline at the end
        "no-multiple-empty-lines": ["error", { max: 2, maxEOF: 1 }],

        // disallow declaring the same variable more then once
        "no-redeclare": ["error"],

        // disallow use of assignment in return statement
        "no-return-assign": ["error", "always"],

        // disallow declaration of variables that are not used in the code
        "no-unused-vars": ["error", {
            ignoreRestSiblings: true,
            caughtErrors: "all",
        }],

        // specify whether double or single quotes should be used
        "quotes": ["error", "single", { avoidEscape: true }],

        "react/prop-types": 0,

        // require quotes around object literal property names
        // https://eslint.org/docs/rules/quote-props.html
        "quote-props": ["error", "as-needed", {
            keywords: false,
            unnecessary: true,
            numbers: false,
        }],

        // https://eslint.org/docs/rules/no-prototype-builtins
        "no-prototype-builtins": "error",

        // https://eslint.org/docs/rules/array-callback-return
        "array-callback-return": ["error", { allowImplicit: true }],

        // encourages use of dot notation whenever possible
        "dot-notation": ["error", { allowKeywords: true }],

        // disallow use of chained assignment expressions
        // https://eslint.org/docs/rules/no-multi-assign
        "no-multi-assign": ["error"],

        // disallow else after a return in an if
        // https://eslint.org/docs/rules/no-else-return
        "no-else-return": ["error", { allowElseIf: false }],

        // specify the max number of lines in a file
        // https://eslint.org/docs/rules/max-lines
        "max-lines": ["off", {
          max: 499,
          skipBlankLines: true,
          skipComments: true,
        }],

        // disallow use of eval()
        "no-eval": "error",

        // suggest using the spread operator instead of .apply()
        // https://eslint.org/docs/rules/prefer-spread
        "prefer-spread": "error",

        // suggest using arrow functions as callbacks
        "prefer-arrow-callback": ["error", {
            allowNamedFunctions: false,
            allowUnboundThis: true,
        }],

        // require space before/after arrow function's arrow
        // https://eslint.org/docs/rules/arrow-spacing
        "arrow-spacing": ["error", { before: true, after: true }],

        // disallow arrow functions where they could be confused with comparisons
        // https://eslint.org/docs/rules/no-confusing-arrow
        "no-confusing-arrow": ["error"],

        // disallow unnecessary constructor
        // https://eslint.org/docs/rules/no-useless-constructor
        "no-useless-constructor": "error",

        // disallow duplicate class members
        // https://eslint.org/docs/rules/no-dupe-class-members
        "no-dupe-class-members": "error",

        "no-duplicate-imports": "error",

        //------------------------------SPACES-----------------------------------

        // enforce spacing before and after comma
        "comma-spacing": ["error", { before: false, after: true }],

        "spaced-comment": ["error", "always", { exceptions: ["-"] }],

        // require or disallow spaces inside parentheses
        "space-in-parens": ["error", "always"],

        // enforce spacing inside array brackets
        "array-bracket-spacing": ["error", "never"],

        // require padding inside curly braces
        "object-curly-spacing": ["error", "always"],

        // enforce spacing inside single-line blocks
        // https://eslint.org/docs/rules/block-spacing
        "block-spacing": ["error", "always"],

        // disallow mixed spaces and tabs for indentation
        "no-mixed-spaces-and-tabs": ["error"],

        // disallow multiple whitespace around logical expressions
        "no-multi-spaces": ["error"],

        // disallow trailing whitespace at the end of lines
        "no-trailing-spaces": ["error", {
            skipBlankLines: false,
            ignoreComments: false,
        }],

        // enforce the spacing around the * in generator functions
        // https://eslint.org/docs/rules/generator-star-spacing
        "generator-star-spacing": ["error", { before: false, after: true }],

        // require or disallow space before blocks
        "space-before-blocks": "error",

        // enforce spacing between functions and their invocations
        // https://eslint.org/docs/rules/func-call-spacing
        "func-call-spacing": ["error", "never"],

        // require a space before & after certain keywords
        "keyword-spacing": ["error", {
          before: true,
          after: true,
          overrides: {
            return: { after: true },
            throw: { after: true },
            case: { after: true }
          }
        }],

        // require spaces around operators
        "space-infix-ops": "error",

        // enforce usage of spacing in template strings
        // https://eslint.org/docs/rules/template-curly-spacing
        "template-curly-spacing": "error",

        // require or disallow space before blocks
        "space-before-blocks": "error",

        // this option sets a specific tab width for your code
        // https://eslint.org/docs/rules/indent
        "indent": ["error", 2, {
          SwitchCase: 1,
          VariableDeclarator: 1,
          MemberExpression: 1,
          ArrayExpression: 1,
          ObjectExpression: 1,
          ImportDeclaration: 1,
          flatTernaryExpressions: false,
          // list derived from https://github.com/benjamn/ast-types/blob/master/def/jsx.ts
          ignoredNodes: [
            "JSXElement",
            "JSXElement > *",
            "JSXAttribute",
            "JSXIdentifier",
            "JSXNamespacedName",
            "JSXMemberExpression",
            "JSXSpreadAttribute",
            "JSXExpressionContainer",
            "JSXOpeningElement",
            "JSXClosingElement",
            "JSXText",
            "JSXEmptyExpression",
            "JSXSpreadChild",
          ],
          ignoreComments: false,
        }],

        //-------------------------ES-6---------------------------------------

        // suggest using of const declaration for variables that are never modified after declared
        "prefer-const": ["error", {
            destructuring: "any",
            ignoreReadBeforeAssign: true,
        }],

        // disallow modifying variables that are declared using const
        "no-const-assign": "error",

        // require let or const instead of var
        "no-var": "error",

        // suggest using template literals instead of string concatenation
        // https://eslint.org/docs/rules/prefer-template
        "prefer-template": "error",

        // disallow to use this/super before super() calling in constructors.
        // https://eslint.org/docs/rules/no-this-before-super
        "no-this-before-super": "error",

        // Prefer destructuring from arrays and objects
        // https://eslint.org/docs/rules/prefer-destructuring
        "prefer-destructuring": ["error", {
          VariableDeclarator: {
            array: false,
            object: true,
          },
          AssignmentExpression: {
            array: true,
            object: true,
          },
        }, {
          enforceForRenamedProperties: false,
        }],

        // suggest using the spread operator instead of .apply()
        // https://eslint.org/docs/rules/prefer-spread
        "prefer-spread": "error",

        //---------------------------ERRORS-------------------------------

        // disallow use of console
        "no-console": ["warn"],

        // disallow use of debugger
        "no-debugger": ["error"],

        // disallow double-negation boolean casts in a boolean context
        // https://eslint.org/docs/rules/no-extra-boolean-cast
        "no-extra-boolean-cast": ["error"],

        // disallow use of constant expressions in conditions
        "no-constant-condition": "warn",

        // disallow duplicate keys when creating object literals
        "no-dupe-keys": "error",

        // disallow a duplicate case label.
        "no-duplicate-case": "error",

        // disallow unnecessary semicolons
        "no-extra-semi": "warn",

        // disallow overwriting functions written as function declarations
        "no-func-assign": "error",

        // disallow unreachable statements after a return, throw, continue, or break statement
        "no-unreachable": "error",

        // disallow comparisons with the value NaN
        "use-isnan": "error",

        // disallow use of unary operators, ++ and --
        // https://eslint.org/docs/rules/no-plusplus
        "no-plusplus": "error",

        // disallow tab characters entirely
        "no-tabs": "error",

        // disallow reassignments of native objects or read-only globals
        // https://eslint.org/docs/rules/no-global-assign
        "no-global-assign": ["error", { exceptions: [] }],

        // disallow use of multiline strings
        "no-multi-str": "error",

        //-----------------------REACT-HOOKS-------------------------------
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
    }
}
