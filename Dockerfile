FROM node:17 AS static

ADD . /tmp

WORKDIR /tmp

RUN npm install

RUN npm run build:styles && npm run dev


FROM python:3.10-slim as app
LABEL maintainer="glugov1998@gmail.com"

# move node installation away, use staging
RUN apt-get -y update && \
    apt-get install -y \
    git gcc build-essential && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /requirements
ADD ./requirements/* /requirements/

WORKDIR /requirements
RUN pip install --upgrade pip && pip install -r dev.txt && rm -rf ~/.cache

RUN apt-get purge -y --auto-remove git
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

ENV PYTHONUNBUFFERED 1

RUN mkdir /app
ADD . /app

WORKDIR /app

COPY --from=static /tmp/frontend/dist frontend/dist
