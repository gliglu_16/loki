from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from lti.utils import instructor_only, only_lti

decorators = [
    only_lti,
    login_required,
    xframe_options_exempt,
    csrf_exempt,
    instructor_only,
]


@method_decorator(decorators, name='dispatch')
class FrontendView(TemplateView):
    """
    Render react template.
    """
    template_name = 'frontend/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tool_uuid = self.request.session.get('TOOL_UUID')
        context['tool_uuid'] = tool_uuid
        return context


@method_decorator(xframe_options_exempt, name='dispatch')
class MethodDrawView(TemplateView):
    """
    Render react template.
    """
    template_name = 'method_draw/index.html'
