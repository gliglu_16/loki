// @flow

export const setPlaceholder = ( formErrors: Object, keys: Object ) => {
  const key = Object.keys( keys )[0]
  const placeholder = Object.prototype.hasOwnProperty.call( formErrors, key )
    ? formErrors[key]
    : `Start typing ${keys[key]} name`
  return placeholder
}
