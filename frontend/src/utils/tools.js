// @flow

export const transformDate = ( dateString: string ) => {
  const parsedDate = new Date( dateString )
  return parsedDate.toUTCString()
}

export const keepOnPage = ( e ) => {
  const message = 'Do you want to leave this site?\nChanges you made may not be saved.'
  e.returnValue = message
  return message
}

export const scrollToBottom = ( elementId: string ) => {
  const objDiv = document.getElementById( elementId )
  objDiv.scrollTop = objDiv.scrollHeight
}

export const processUrlParam = ( param: any, type: string ) => {
  if ( typeof param === type ) {
    return param
  } if ( typeof param === 'number' ) {
    return param.toString()
  }
  return ''
}

export const getBoolImage = ( check: boolean ) => {
  const images = require.context( '../../static/images', true )
  const icon = check ? 'icon-yes' : 'icon-no'
  try {
    return images( `./${icon}.svg` )
  } catch ( e ) {
    // eslint-disable-next-line no-console
    console.error( `Error rendering icon: ${e}` )
  }
}

export const getToolUuid = () => {
  return document.getElementById( 'app' ).getAttribute( 'toolRef' )
}

export const getLTIUserRepr = ( ltiUser: Object ) => {
  return ltiUser.user_fullname || ltiUser.user_username || ltiUser.user_id
}

export const compareLTIUsers = ( firstLTIUser: Object, secondLTIUser: Object ) => {
  return firstLTIUser.user_id === secondLTIUser.user_id
}

export const transformSvgPreview = ( svgString: string ) => {
  const sizeRegex = /width="(\d+)" height="(\d+)"/
  svgString = svgString.replace( sizeRegex, 'viewbox="0 0 $1 $2" class="w-48"' )
  return { __html: svgString }
}
