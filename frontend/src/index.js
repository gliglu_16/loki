import React from 'react'
import ReactDOM from 'react-dom'

import App from './pages/app'
import 'styles/styles.css'

const render = ( Component ) => {
  ReactDOM.render(
    <Component/>,
    document.getElementById( 'app' ),
  )
}

render( App )
