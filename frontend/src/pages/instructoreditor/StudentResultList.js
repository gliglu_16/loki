import React from 'react'
import Button from '../../components/generic/Button'
import fetch from 'unfetch'
import LoadingPlaceholder from '../../components/generic/LoadingPlaceholder'
import LinkButton from '../../components/generic/LinkButton'
import StudentResult from '../../components/instructoreditor/StudentResult'
import GradeSkillsModal from '../../components/instructoreditor/GradeSkillsModal'

/**
 * Container for students' results data.
 *
 * Data are provided per-tool.
 */
class StudentResultList extends React.Component {

  constructor( props ) {
    super( props )
    this.state = {
      studentResultData: [],
      filteredStudentResultData: [],
      hasMore: false,
      nextHref: null,
      isSent: false,
      selectedStudentResultId: null,
      showGradeModal: false,
      filterValue: 'all',
    }
    this.getToolUuid = this.getToolUuid.bind( this )
    this.loadMore = this.loadMore.bind( this )
    this.handleFilterChange = this.handleFilterChange.bind( this )
    this.filterStudentResults = this.filterStudentResults.bind( this )
    this.handleOpenGradeModal = this.handleOpenGradeModal.bind( this )
    this.handleCloseGradeModal = this.handleCloseGradeModal.bind( this )
  }

  getToolUuid () {
    return document.getElementById( 'app' ).getAttribute( 'toolRef' )
  }

  async handleOpenGradeModal( e ) {
    await this.setState( {
      showGradeModal: true,
      selectedStudentResultId: e.currentTarget.attributes.studentresult.value,
    } )
  }

  handleCloseGradeModal() {
    this.setState( { showGradeModal: false } )
  }

  handleFilterChange( e ) {
    this.setState( {
      filterValue: e.target.value,
    } )
    this.filterStudentResults( e.target.value )
  }

  filterStudentResults( filterValue ) {
    const { studentResultData } = this.state
    switch ( filterValue ) {
      case 'notSubmitted':
        this.setState( {
          filteredStudentResultData: studentResultData.filter(
            studentRes => !studentRes.submitted,
          ),
        } )
        break
      case 'graded':
        this.setState( {
          filteredStudentResultData: studentResultData.filter(
            studentRes => ( studentRes.submitted && studentRes.grade ),
          ),
        } )
        break
      case 'notGraded':
        this.setState( {
          filteredStudentResultData: studentResultData.filter(
            studentRes => ( studentRes.submitted && !studentRes.grade ),
          ),
        } )
        break
      default:
        this.setState( { filteredStudentResultData: studentResultData } )
    }
  }

  /**
   * Fetch student result list by tool uuid and update state.
   *
   * @param {String} toolUuid: student uuid to fetch entries by.
   */
  fetchItems( toolUuid ) {
    fetch( `/api/v1/student-result/list/${toolUuid}/` )
      .then( response => response.json() )
      .then(
        ( data ) => {
          if ( typeof data.results !== 'undefined' && data.results.length > 0 ) {
            this.setState( {
              studentResultData: data.results,
              hasMore: !!data.next,
              nextHref: data.next,
              isSent: true,
            } )
          }
          this.setState( {
            isSent: true,
          } )
          this.filterStudentResults( 'all' )
        },
      )
  }

  componentDidMount() {
    const toolUuid = this.getToolUuid()
    this.fetchItems( toolUuid )
  }

  loadMore() {
    if ( this.state.hasMore ) {
      fetch( this.state.nextHref )
        .then( response => response.json() )
        .then( data => {
          if ( typeof data.results !== 'undefined' && data.results.length > 0 ) {
            this.setState( {
              studentResultData: [...this.state.studentResultData, ...data.results],
              hasMore: !!data.next,
              nextHref: data.next,
              isSent: true,
            }, () => {
              this.filterStudentResults( this.state.filterValue )
            } )
          }
        } )
    }
  }

  render() {
    return (
      <div className='container bg-white p-4'>
        <div className='flex justify-between px-4'>
          <LinkButton
            className='inline-flex items-center'
            to={ '/instructor' }
            icon='chevron-left-solid'
          >
            Main Page
          </LinkButton>
          <h1 className='text-center text-xl'>Students' Results</h1>
          <label className='inline-block relative'>
            Filter results:
            <select className='appearance-none ml-1 bg-white border border-gray-400 hover:border-gray-500 rounded shadow leading-tight focus:outline-none focus:shadow-outline' value={ this.state.filterValue } onChange={ this.handleFilterChange }>
              <option value='all'>All</option>
              <option value='notSubmitted'>Not Submitted</option>
              <option value='graded'>Submitted & Graded</option>
              <option value='notGraded'>Submitted & Not Graded</option>
            </select>
            <div className='pointer-events-none absolute inset-y-0 right-0 pb-2 flex items-center px-2 text-gray-700'>
              <svg className='fill-current h-4 w-4' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'><path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z'/></svg>
            </div>
          </label>
        </div>
        <div className='p-4 m-4 border overflow-x-hidden'>
        {
          ( Array.isArray( this.state.filteredStudentResultData ) &&
            this.state.filteredStudentResultData.length )
            ?
            <React.Fragment>
                <div className='flex border-b border-gray-200'>
                  <div className='w-2/5 p-2 text-center'>Latest Answer</div>
                  <div className='w-1/5 p-2 text-center'>Student</div>
                  <div className='w-1/5 p-2 text-center'>Grade</div>
                  <div className='w-1/5 p-2 text-center'>Is Submitted</div>
                  <div className='w-1/5 p-2 text-center'>Attempts</div>
                  <div className='w-1/5 p-2 text-center'></div>
                </div>
                {
                  this.state.filteredStudentResultData.map( ( studRes, i ) => (
                      <StudentResult
                        key={ i }
                        studentResult={ studRes }
                        openGradeModal={ this.handleOpenGradeModal }
                      />
                  ) )
                }

              { this.state.hasMore && ( <div className='flex justify-center'>
                  <Button onClick={ this.loadMore }>Load More</Button>
                </div> )
              }
            </React.Fragment>
            :
          <LoadingPlaceholder condition={ this.state.isSent } />
        }
        </div>
        { this.state.showGradeModal &&
          <GradeSkillsModal
            studentResultId={ this.state.selectedStudentResultId }
            isOpen={ this.state.showGradeModal }
            closeModal={ this.handleCloseGradeModal }
          />
        }
      </div>
    )
  }

}

export default StudentResultList
