import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router'
import fetch from 'unfetch'
import Button from '../../components/generic/Button'
import LinkButton from '../../components/generic/LinkButton'
import LoadingPlaceholder from '../../components/generic/LoadingPlaceholder'
import GradeSkillsModal from '../../components/instructoreditor/GradeSkillsModal'
import { transformDate, getBoolImage, transformSvgPreview } from '../../utils/tools'

const renderAnswerList = ( versions, studentResultId ) => {

  const answersList = versions.map( ( version, i ) => (
    <LinkButton
      className='w-full border-none my-4'
      to={ `/instructor/editor/${studentResultId}/${version.id}` }
      key={ i }
      version={ i }
    >
      <div className='flex p-4 items-center' key={ i }>

        <div className='flex h-40 w-2/5 p-2 text-center justify-center' key={ `${i}-img` } dangerouslySetInnerHTML={ transformSvgPreview( version.answer ) } />
        <div className='w-1/5 p-2 text-center' key={ `${i}-label` }>{ version.label }</div>
        <div className='w-1/5 p-2 text-center' key={ `${i}-date` }>
          { transformDate( version.submit_date ) }
        </div>
        <div className='w-1/5 p-2 text-center content-center' key={ `${i}-is-submission` }>
          <img className='block mx-auto' src={ getBoolImage( version.is_submission ) } alt={ version.is_submission.toString() }></img>
        </div>
        <div className='w-1/5 p-2 text-center content-center' key={ `${i}-is-instructor` }>
          <img className='block mx-auto' src={ getBoolImage( version.instructor_user !== null ) } alt={ ( version.instructor_user !== null ).toString()}></img>
        </div>
      </div>
    </LinkButton>
  ),
  )
  return (
    <React.Fragment>
      <div className='flex border-b border-gray-200'>
        <div className='w-2/5 p-2 text-center'>Answer</div>
        <div className='w-1/5 p-2 text-center'>Label</div>
        <div className='w-1/5 p-2 text-center'>Submit Date</div>
        <div className='w-1/5 p-2 text-center'>Is Submission</div>
        <div className='w-1/5 p-2 text-center'>Is Instructor</div>
      </div>
      { answersList }
    </React.Fragment>
  )
}


const StudentVersions = props => {

  const { studentResultId } = useParams()

  const [versions, setVersions] = useState( [] )
  const [isSent, setIsSent] = useState( false )
  const [hasMore, setHasMore] = useState( false )
  const [nextHref, setNextHref] = useState( '' )
  const [showGradeModal, setShowGradeModal] = useState( false )

  useEffect( () => {
    fetch( `/api/v1/student-answer-history/list/${studentResultId}/` )
      .then( response => response.json() )
      .then( data => {
        setVersions( data.results )
        setIsSent( true )
        setNextHref( data.next )
        setHasMore( !!data.next )
      } )
  }, [studentResultId] )

  const loadMore = () => {
    if ( hasMore ) {
      fetch( nextHref )
        .then( response => response.json() )
        .then( data => {
          setVersions( [...versions, ...data.results] )
          setIsSent( true )
          setNextHref( data.next )
          setHasMore( !!data.next )
        },
        )
    }
  }

  const openGradeModal = () => {
    setShowGradeModal( true )
  }

  const closeGradeModal = () => {
    setShowGradeModal( false )
  }

  const ItemList = () => (
    <React.Fragment>
      { renderAnswerList( versions, studentResultId ) }
      { hasMore && ( <div className='flex justify-center'>
          <Button onClick={ loadMore }>Load More</Button>
        </div> )
      }
    </React.Fragment>
  )

  return (
    <div className='container bg-white p-4'>
      <div className='flex px-4'>
        <LinkButton
          className='w-1/5 inline-flex items-center'
          to={ '/student-results' }
          icon='chevron-left-solid'
        >
          Back to the students' results
        </LinkButton>
        <h1 className='w-3/5 text-center text-xl'>Student's answers</h1>
        {
          ( versions.length && versions[0].student_result.tool.enable_grading )
            ?
            <Button
              className='w-1/5'
              onClick={ openGradeModal }
            >
              Grade
            </Button>
            :
            null
        }
      </div>
      <div className='p-4 m-4 overflow-x-hidden'>
      {
        ( Array.isArray( versions ) && versions.length )
          ?
          <ItemList />
          :
          <LoadingPlaceholder condition={isSent} />
      }
      </div>
      { showGradeModal &&
        <GradeSkillsModal
          studentResultId={ studentResultId }
          isOpen={ showGradeModal }
          closeModal={ closeGradeModal }
        />
      }
    </div>
  )
}

export default StudentVersions
