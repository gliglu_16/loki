import React from 'react'
import LinkButton from '../../components/generic/LinkButton'
import Button from '../../components/generic/Button'
import { getToolUuid } from '../../utils/tools'

const NavigationInstructorsPage = props => {

  const openToolSetup = () => {
    window.location.replace( `/drawing-tool/edit/${getToolUuid()}` )
  }

  return (
    <React.Fragment>
      <h1 className='text-xl text-center my-8'>Drawing Tool LTI provider</h1>
      <div className='flex align-center h-xl text-xl'>
        <Button className='max-w-sm rounded shadow-lg w-1/2 m-8 p-8 text-center bg-white' onClick={openToolSetup}>Set Up Drawing Tool</Button>
        <LinkButton className='w-1/2 m-8 p-8 text-center  bg-white' to='/student-results'>View Students' Results</LinkButton>
      </div>
    </React.Fragment>
  )
}

export default NavigationInstructorsPage
