import DrawingTool from '../../components/studenteditor/DrawingTool'
import Instructions from '../../components/studenteditor/Instructions'
import { keepOnPage } from '../../utils/tools'
import React from 'react'
import fetch from 'unfetch'
import { withRouter } from 'react-router'

class StudentEditor extends React.Component {
  constructor( props ) {
    super( props )
    this.state = {
      showTool: props.isInstructor,
      versionLoaded: false,
      studentResultLoaded: false,
      toolLoaded: false,
      drawingToolData: {},
      error: null,
      currentVersion: {
        id: props.match.params.studentVersionId,
        isSaved: true,
        student_result: {
          student_user: {
            user_id: '',
            user_username: '',
            user_fullname: '',
          },
        },
      },
      comments: [],
      studentResultId: props.match.params.studentResultId,
    }

    this.handleOpenTool = this.handleOpenTool.bind( this )
    this.handleCloseTool = this.handleCloseTool.bind( this )
    this.getToolUuid = this.getToolUuid.bind( this )
    this.handleSVGSave = this.handleSVGSave.bind( this )
    this.updateCurrentVersion = this.updateCurrentVersion.bind( this )
    this.appendComment = this.appendComment.bind( this )
    this.fetchComments = this.fetchComments.bind( this )
    this.fetchResultsAndToolData = this.fetchResultsAndToolData.bind( this )
    this.fetchVersion = this.fetchVersion.bind( this )
    this.fetchDrawingToolAndComments = this.fetchDrawingToolAndComments.bind( this )
  }

  getToolUuid () {
    return document.getElementById( 'app' ).getAttribute( 'toolRef' )
  }

  handleOpenTool () {
    this.setState( { showTool: true } )
  }

  handleCloseTool () {
    this.setState( { showTool: false } )
  }

  /**
   * Update `currentVersion` object with a new version object.
   */
  updateCurrentVersion( value ) {
    this.setState( {
      currentVersion: {
        ...this.state.currentVersion,
        ...value,
        ...{
          isSaved: true,
        },
      },
    } )
  }

  /**
   * Update `currentVersion.answer` with a new SVG string.
   */
  handleSVGSave( value ) {
    this.setState( {
      currentVersion: {
        ...this.state.currentVersion,
        ...{
          answer: value,
          isSaved: false,
        },
      },
    } )
  }

  /**
   * Update `comments` with a new comment object.
   */
  appendComment( comment ) {
    this.setState( {
      comments: [...this.state.comments, comment],
    } )
  }

  /**
   * Fetch comments and update state.
   */
  async fetchVersion() {
    if ( this.props.isInstructor && this.state.currentVersion.id ) {
      await fetch( `/api/v1/answer-details/${this.state.currentVersion.id}/` )
        .then( response => response.json() )
        .then( ( data ) => {
          this.updateCurrentVersion( data )
        } )
        .catch( err => {
          console.log( `Specified student version doesn't exist. Error: ${err}` )
        } )
    }
    else {
      await fetch( `/api/v1/student-answer-history/list/${this.getToolUuid()}/?last-entry` )
        .then( response => response.json() )
        .then(
          ( data ) => {
            if ( typeof data.results !== 'undefined' && data.results.length > 0 ) {
              this.updateCurrentVersion( data.results[0] )
            }
          },
        )
    }
    this.setState( {
      versionLoaded: true,
    } )
  }

  /**
   * Fetch comments and update state.
   *
   * @param {Number} studentResultId: student result id
   *   to fetch comments by.
   */
  fetchComments( studentResultId ) {
    fetch( `/api/v1/comment/list/${studentResultId}/` )
      .then( response => response.json() )
      .then(
        ( data ) => {
          if ( typeof data !== 'undefined' && data.length > 0 ) {
            this.setState( {
              comments: data,
            } )
          }
        },
      )
      .catch( err => {
        console.log( `An error happened while fetching comments. Error: ${err}` )
      } )
  }

  /**
   * Fetch student results by tool uuid and update state.
   *
   * @param {String} toolUuid: student uuid to fetch entries by.
   */
  async fetchResultsAndToolData( toolUuid ) {
    if ( !this.state.studentResultId ) {
      await fetch( `/api/v1/student-result/list/${toolUuid}/` )
        .then( response => response.json() )
        .then(
          ( data ) => {
            if ( typeof data.results !== 'undefined' && data.results.length > 0 ) {
              this.setState( {
                studentResultId: String( data.results[0].id ),
              } )
            }
          },
        )
    }
    this.setState( {
      studentResultLoaded: true,
    } )
    this.fetchDrawingToolAndComments( toolUuid )
  }

  /**
   * Fetch drawing tool details, comments, and update state.
   *
   * @param {String} toolUuid: student uuid to fetch entries by.
   */
  fetchDrawingToolAndComments( toolUuid ) {
    fetch( `/api/v1/drawingtool/${toolUuid}/` )
      .then( response => response.json() )
      .then(
        ( data ) => {
          this.setState( {
            toolLoaded: true,
            drawingToolData: data,
          }, () => {
            if ( this.state.drawingToolData.enable_comments ) {
              this.fetchComments( this.state.studentResultId )
              this.intervalId = setInterval(
                () => this.fetchComments( this.state.studentResultId ),
                30000,
              )
            }
          } )
        },

        ( error ) => {
          this.setState( {
            toolLoaded: true,
            error,
          } )
        },
      )
  }

  componentDidMount() {
    window.addEventListener( 'beforeunload', keepOnPage )
    const toolUuid = this.getToolUuid()
    this.fetchVersion()
    this.fetchResultsAndToolData( toolUuid )
  }

  componentWillUnmount() {
    window.removeEventListener( 'beforeunload', keepOnPage )
    clearInterval( this.intervalId )
  }

  render() {
    const {
      showTool,
      drawingToolData,
      error,
      toolLoaded,
      studentResultLoaded,
      versionLoaded,
      currentVersion,
      comments,
      studentResultId,
    } = this.state

    const {
      isInstructor,
    } = this.props

    if ( error ) {
      return <div className='text-center'>LTI Error</div>
    } if ( !( toolLoaded && versionLoaded && studentResultLoaded ) ) {
      return ( <p className='text-center'>Loading...</p> )
    }
    const toolUuid = this.getToolUuid()
    return (
        <React.Fragment>
          { !showTool && <Instructions
            openTool={this.handleOpenTool}
            title={drawingToolData.display_name}
            question={drawingToolData.question}
            questionImage={drawingToolData.question_image}
            /> }
           { showTool && <DrawingTool
            isInstructor={isInstructor}
            closeTool={this.handleCloseTool}
            title={drawingToolData.display_name}
            question={drawingToolData.question}
            questionImage={drawingToolData.question_image}
            backgroundImage={drawingToolData.background_image}
            hints={drawingToolData.hints}
            showAnswer={drawingToolData.show_answer}
            answer={drawingToolData.answer}
            maxAttempts={drawingToolData.max_attempts}
            enabledGrading={drawingToolData.enable_grading}
            enabledComments={drawingToolData.enable_comments}
            currentSvgVersion={currentVersion}
            svgSave={this.handleSVGSave}
            updateCurrentVersion={this.updateCurrentVersion}
            toolUuid={toolUuid}
            studentResultId={studentResultId}
            comments={comments}
            appendComment={this.appendComment}
            /> }
        </React.Fragment>
    )

  }
}

export default withRouter( StudentEditor )
