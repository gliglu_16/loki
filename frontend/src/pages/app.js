import React from 'react'
import ReactModal from 'react-modal'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import StudentEditor from './studenteditor/StudentEditor'
import StudentVersions from './instructoreditor/StudentVersions'
import StudentResultList from './instructoreditor/StudentResultList'
import NavigationInstructorsPage from './instructoreditor/NavigationInstructorsPage'

ReactModal.setAppElement( '#app' )

const App = () => (
  <Router>
      <Switch>
        <Route path='/instructor/editor/:studentResultId/:studentVersionId'>
          <StudentEditor isInstructor={true} />
        </Route>
        <Route path='/versions/:studentResultId'>
          <StudentVersions />
        </Route>
        <Route path='/student-results'>
          <StudentResultList />
        </Route>
        <Route path='/instructor'>
          <NavigationInstructorsPage />
        </Route>
        <Route exact path='/'>
          <StudentEditor isInstructor={false} />
        </Route>
        <Route component={ () => ( <div className='text-center'>LTI Error</div> ) }/>
      </Switch>
  </Router>
)

export default App
