import React from 'react'
import Button from '../generic/Button'
import PropTypes from 'prop-types'

const SubmitForm = props => {
  const {
    isSuccess, isSent, handleLabelChange, handleSubmit, isSubmit,
  } = props

  return (
    <div>
      {
        ( !isSuccess && isSent ) && <div className='flex justify-center mb-4 text-red-600'>
          <p>An error occurred, please try again later!</p>
        </div>
      }
      <div className='md:flex md:items-center mb-6'>
        <div className='md:w-1/3'>
          <label className='block text-black text-md font-normal' htmlFor='answer-label'>
            Version Label:
          </label>
        </div>
        <div className='md:w-2/3'>
          <input
            className='bg-white border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight'
            type='text'
            onChange={ handleLabelChange }
          />
        </div>
      </div>
      <div>
        <Button
          className='float-right bg-blue-500 hover:bg-blue-700 py-2 px-4 text-white border-none'
          type='submit'
          onClick={ handleSubmit }
        >
          { isSubmit ? 'Submit' : 'Save' }
        </Button>
      </div>
    </div>
  )
}


SubmitForm.propTypes = {
  isSuccess: PropTypes.bool,
  isSent: PropTypes.bool,
  handleLabelChange: PropTypes.func,
  handleSubmit: PropTypes.func,
}

export default SubmitForm
