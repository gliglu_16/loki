import Button from '../generic/Button'
import Comments from '../generic/Comments'
import CloseButton from '../generic/CloseButton'
import LinkButton from '../generic/LinkButton'
import GradeSkillsModal from '../instructoreditor/GradeSkillsModal'
import Modal from '../generic/Modal'
import Controls from './Controls'
import MethodDrawIframe from './MethodDrawIframe'
import SubmitForm from './SubmitForm'
import VersioningModal from './VersioningModal'
import React from 'react'
import PropTypes from 'prop-types'
import fetch from 'unfetch'
import InstructionText from '../generic/InstructionText'

class DrawingTool extends React.Component {
  constructor( props ) {
    super( props )
    this.state = {
      showAnswerModal: false,
      showHintsModal: false,
      showSubmitModal: false,
      showVersioningModal: false,
      isSubmit: false,
      isSuccess: false,
      isSent: false,
      draftLabel: '',
      draftComment: '',
      showGradeModal: false,
      isSubmitSuccess: false,
      isSaveSuccess: false,
      currentLTIUser: {},
    }

    this.methodDraw = React.createRef()
    this.handleSaveAndCloseTool = this.handleSaveAndCloseTool.bind( this )
    this.handleOpenAnswerModal = this.handleOpenAnswerModal.bind( this )
    this.handleCloseAnswerModal = this.handleCloseAnswerModal.bind( this )
    this.handleOpenHintsModal = this.handleOpenHintsModal.bind( this )
    this.handleCloseHintsModal = this.handleCloseHintsModal.bind( this )
    this.handleSubmitVersion = this.handleSubmitVersion.bind( this )
    this.handleSaveVersion = this.handleSaveVersion.bind( this )
    this.handleCloseSubmitModal = this.handleCloseSubmitModal.bind( this )
    this.handleOpenVersioningModal = this.handleOpenVersioningModal.bind( this )
    this.handleCloseVersioningModal = this.handleCloseVersioningModal.bind( this )
    this.handleOpenGradeModal = this.handleOpenGradeModal.bind( this )
    this.handleCloseGradeModal = this.handleCloseGradeModal.bind( this )
    this.handleSubmit = this.handleSubmit.bind( this )
    this.setDraftLabel = this.setDraftLabel.bind( this )
    this.handleUpdateVersion = this.handleUpdateVersion.bind( this )
    this.handleCommentChange = this.handleCommentChange.bind( this )
    this.handleSubmitComment = this.handleSubmitComment.bind( this )
  }

  componentDidMount() {
    fetch( '/api/v1/current-lti-user/' )
      .then( response => response.json() )
      .then(
        ( data ) => {
          this.setState( {
            currentLTIUser: data,
          } )
        },

        ( error ) => {
          // eslint-disable-next-line no-console
          console.error( `Current LTI User not fetched: ${error}` )
        },
      )
  }

  componentDidUpdate( prevProps, prevState ) {
    if ( prevProps.currentSvgVersion.answer !== this.props.currentSvgVersion.answer ) {
      this.handleUpdateVersion( this.props.currentSvgVersion )
    }

    let changedControlsStatus = null
    if ( this.state.isSubmitSuccess ) {
      changedControlsStatus = 'isSubmitSuccess'
    } else if ( this.state.isSaveSuccess ) {
      changedControlsStatus = 'isSaveSuccess'
    }
    if ( changedControlsStatus ) {
      this.controlsMessagesTimeout = setTimeout( () => {
        this.setState( () => ( { [changedControlsStatus]: false } ) )
      }, 5000 )
    }
  }

  componentWillUnmount() {
    clearTimeout( this.controlsMessagesTimeout )
  }

  setDraftLabel( e ) {
    this.setState( { draftLabel: e.target.value } )
  }

  handleSaveAndCloseTool() {
    this.methodDraw.current.saveSvg()
    setTimeout( this.props.closeTool, 50 )
  }

  handleUpdateVersion( data ) {
    this.props.updateCurrentVersion( data )
    this.methodDraw.current.updateSvg()
  }

  handleOpenAnswerModal() {
    this.setState( { showAnswerModal: true } )
  }

  handleCloseAnswerModal() {
    this.setState( { showAnswerModal: false } )
  }

  handleOpenHintsModal() {
    this.setState( { showHintsModal: true } )
  }

  handleCloseHintsModal() {
    this.setState( { showHintsModal: false } )
  }

  handleOpenGradeModal() {
    this.setState( { showGradeModal: true } )
  }

  handleCloseGradeModal() {
    this.setState( { showGradeModal: false } )
  }


  handleSubmitVersion() {
    this.methodDraw.current.saveSvg()
    this.setState( {
      showSubmitModal: true,
      isSent: false,
      isSubmit: true,
    } )
  }

  handleSaveVersion() {
    this.methodDraw.current.saveSvg()
    this.setState( {
      showSubmitModal: true,
      isSent: false,
      isSubmit: false,
    } )
  }

  handleCommentChange( e ) {
    this.setState( { draftComment: e.target.value } )
  }

  handleSubmitComment( e ) {
    e.preventDefault()
    if ( this.state.draftComment ) {
      fetch( '/api/v1/comment/create/', {
        method: 'POST',
        headers: {
          'X-CSRFToken': Cookies.get( 'csrftoken' ),
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify( {
          student_result_id: this.props.studentResultId,
          version_id: this.props.currentSvgVersion.id,
          comment: this.state.draftComment,
        } ),
      } )
        .then( response => {
          if ( !response.ok ) {
            return response
          }

          response.json().then(
            ( data ) => {
              this.props.appendComment( data )
            } )
        },
        )
    }

    this.setState( { draftComment: '' } )
  }

  handleCloseSubmitModal() {
    this.setState( { showSubmitModal: false } )
  }

  handleOpenVersioningModal() {
    this.setState( { showVersioningModal: true } )
  }

  handleCloseVersioningModal() {
    this.setState( { showVersioningModal: false } )
  }

  handleSubmit( e ) {
    e.preventDefault()
    fetch( '/api/v1/answer-history/create/', {
      method: 'POST',
      headers: {
        'X-CSRFToken': Cookies.get( 'csrftoken' ),
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify( {
        tool_uuid: this.props.toolUuid,
        answer: this.props.currentSvgVersion.answer,
        label: this.state.draftLabel,
        is_submission: this.state.isSubmit,
        student_user_id: this.props.isInstructor
          ? this.props.currentSvgVersion.student_result.student_user.user_id
          : null,
      } ),
    } )
      .then( response => {
        if ( !response.ok ) {
          this.setState( {
            isSent: true,
            isSuccess: false,
            isSubmitSuccess: false,
            isSaveSuccess: false,
          } )
          return response
        }

        response.json().then(
          ( data ) => {
            this.setState( {
              isSent: true,
              isSuccess: true,
              isSubmitSuccess: this.state.isSubmit,
              isSaveSuccess: !this.state.isSubmit,
            } )
            this.handleUpdateVersion( data )
            this.handleCloseSubmitModal()
          } )
      },
      )
  }

  render() {
    const {
      isInstructor,
      title,
      question,
      questionImage,
      hints,
      showAnswer,
      answer,
      maxAttempts,
      enabledGrading,
      enabledComments,
      currentSvgVersion,
      backgroundImage,
      svgSave,
      comments,
      studentResultId,
      toolUuid,
    } = this.props

    return (
    <div className='mx-auto bg-white border-gray-700 px-10 py-10 rounded-lg overflow-hidden'>
      { isInstructor ?
        <LinkButton
          className='inline-flex items-center'
          to={ `/versions/${studentResultId}` }
          icon='chevron-left-solid'
        >
          Back to answers
        </LinkButton>
        : <Button
        className='inline-flex items-center'
        onClick={ this.handleSaveAndCloseTool }
        icon='chevron-left-solid'
      >
        Back to instructions
      </Button> }
      <div className='pt-5 flex flex-inline'>
        <h1 className='text-2xl text-gray-900 leading-normal font-semibold mr-1 my-auto'>{ title }</h1>
        { !isInstructor
          ? <button
              className='mx-1 bg-white hover:bg-gray-100 border border-gray-400 text-black py-2 px-4'
              onClick={ this.handleOpenVersioningModal }
            >
              Version: { currentSvgVersion.label }{ !currentSvgVersion.isSaved && '*' }
            </button>
          : null
        }
      </div>

      <div className='pt-5'>
        <InstructionText name='Instructions' content={ question } image={ questionImage } isCollapsible={ true }/>
      </div>

      <div className='block lg:flex xl:flex ml-4 py-10 mx-auto h-auto'>
        <MethodDrawIframe
          ref={ this.methodDraw }
          onSave={ svgSave }
          svgDrawing={ currentSvgVersion ? currentSvgVersion.answer : '' }
          backgroundImage={ backgroundImage || '' }
        />
      </div>

      <Controls
        showAnswerModal={ this.handleOpenAnswerModal }
        showHintsModal={ this.handleOpenHintsModal }
        showSubmitModal={ this.handleSubmitVersion }
        showAnswer={ showAnswer }
        maxAttempts={ maxAttempts }
        handleSaveVersion = { this.handleSaveVersion }
        hints={ hints }
        disabledSubmit={ isInstructor }
        enabledGrading={ enabledGrading }
        openGradeModal={ this.handleOpenGradeModal }
        isSubmitSuccess = { this.state.isSubmitSuccess }
        isSaveSuccess = { this.state.isSaveSuccess }
      />

      <div className='block lg:flex xl:flex ml-4 py-10 mx-auto h-auto'>
        { enabledComments && <Comments
            comments={ comments }
            handleCommentChange={ this.handleCommentChange }
            submitComment={ this.handleSubmitComment }
            draftComment={ this.state.draftComment }
            currentLTIUser={ this.state.currentLTIUser }
          />
        }
      </div>
      { this.state.showAnswerModal &&
        <Modal
          isOpen={ this.state.showAnswerModal }
          onRequestClose={ this.handleCloseAnswerModal }
          contentLabel='Answer Modal'
          className='max-w-6xl w-full rounded'
        >
          <CloseButton onClick={ this.handleCloseAnswerModal }></CloseButton>
          {
            answer && <div className='flex justify-center'>
                <img className='max-w-md' src={ answer }></img>
              </div>
          }
        </Modal>
      }
      { this.state.showHintsModal &&
        <Modal
          isOpen={this.state.showHintsModal}
          onRequestClose={this.handleCloseHintsModal}
          contentLabel='Hints Modal'
          className='max-w-6xl w-full rounded'
        >
          <CloseButton onClick={this.handleCloseHintsModal}></CloseButton>
          {
            hints && <div className='flex justify-center'>
                <p>{ hints }</p>
              </div>
          }
        </Modal>
      }
      { this.state.showSubmitModal &&
        <Modal
          isOpen={this.state.showSubmitModal}
          onRequestClose={this.handleCloseSubmitModal}
          contentLabel='Submit Modal'
          className='max-w-sm rounded border-none'
        >
          <CloseButton onClick={this.handleCloseSubmitModal}></CloseButton>
          <SubmitForm
            isSuccess={this.state.isSuccess}
            isSent={this.state.isSent}
            isSubmit={this.state.isSubmit}
            handleLabelChange={this.setDraftLabel}
            handleSubmit={this.handleSubmit}
          />
        </Modal>
      }
      { this.state.showVersioningModal &&
        <VersioningModal
          toolUuid={toolUuid}
          isOpen={this.state.showVersioningModal}
          closeModal={this.handleCloseVersioningModal}
          updateCurrentVersion={this.handleUpdateVersion}
        />
      }
      { this.state.showGradeModal &&
        <GradeSkillsModal
          studentResultId={ String( studentResultId ) }
          isOpen={ this.state.showGradeModal }
          closeModal={ this.handleCloseGradeModal }
        />
      }
    </div> )
  }
}

DrawingTool.propTypes = {
  isInstructor: PropTypes.bool,
  closeTool: PropTypes.func,
  title: PropTypes.string,
  hints: PropTypes.string,
  showAnswer: PropTypes.bool,
  answer: PropTypes.string,
  maxAttempts: PropTypes.number,
  enabledGrading: PropTypes.bool,
  currentSvgVersion: PropTypes.object,
  svgSave: PropTypes.func,
  updateCurrentVersion: PropTypes.func,
  comments: PropTypes.array,
  appendComment: PropTypes.func,
  toolUuid: PropTypes.string.isRequired,
  studentResultId: PropTypes.string.isRequired,
}

export default DrawingTool
