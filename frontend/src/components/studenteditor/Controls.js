import React from 'react'
import Button from '../generic/Button'
import PropTypes from 'prop-types'

const SidebarButton = ( props ) => (
  <span>
    <Button
      className='inline-flex items-center py-2 px-4 mx-1'
      icon={ props.icon }
      onClick={ props.onClick }
    >
      { props.name }
    </Button>
  </span>
)

const AttemptsString = ( props ) => (
  <React.Fragment>
    { ( props.maxAttempts > 0 && !props.disabled ) && <span>
        You have used 0 of { props.maxAttempts } attempts.
      </span>
    }
  </React.Fragment>
)

const Controls = props => {
  const {
    showAnswerModal,
    showHintsModal,
    showSubmitModal,
    showAnswer,
    maxAttempts,
    handleSaveVersion,
    hints,
    disabledSubmit,
    enabledGrading,
    openGradeModal,
    isSubmitSuccess,
    isSaveSuccess,
  } = props

  const getControlsStatusMessage = () => {
    let statusMessage
    if ( isSubmitSuccess ) {
      statusMessage = 'Your results have been submitted!'
    } else if ( isSaveSuccess ) {
      statusMessage = 'Your results have been saved!'
    }
    return statusMessage
  }

  return (
      <div>
        <div className='p-4'>
          <AttemptsString
            maxAttempts={ maxAttempts }
            disabled={ disabledSubmit }
          />
        </div>
        <div className='inline-block'>
          <Button
            className='py-2 px-4 mx-1'
            onClick={ handleSaveVersion }
          >
            { disabledSubmit ?
              'Save as an instructor version'
              : 'Save'
            }
          </Button>
          { !disabledSubmit && <Button
              className='py-2 px-4 mx-4 bg-blue-500 hover:bg-blue-700 text-white border-none'
              onClick={ showSubmitModal }
            >
              Submit
            </Button>
          }
          { ( disabledSubmit && enabledGrading ) && <Button
              className='py-2 px-4 mx-4'
              onClick={ openGradeModal }
            >
              Grade
            </Button>
          }
          { ( isSubmitSuccess || isSaveSuccess )
            && <span
              className='justify-center mb-4 text-green-600'
            >
              { getControlsStatusMessage() }
            </span>
          }
        </div>
        <div className='float-right'>
          { hints && <SidebarButton
            icon='question-solid'
            name='Hints'
            onClick={ showHintsModal }/>
          }
          { showAnswer && <SidebarButton
            icon='exclamation-circle-solid'
            name='Show answer'
            onClick={ showAnswerModal }/>
          }
        </div>
      </div>
  )
}

Controls.propTypes = {
  showAnswerModal: PropTypes.func,
  showHintsModal: PropTypes.func,
  showSubmitModal: PropTypes.func,
  showAnswer: PropTypes.bool,
  maxAttempts: PropTypes.number,
  hints: PropTypes.string,
  disabledSubmit: PropTypes.bool,
  enabledGrading: PropTypes.bool,
}

export default Controls
