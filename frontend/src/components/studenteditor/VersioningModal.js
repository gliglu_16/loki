import React, { useState, useEffect } from 'react'
import Button from '../generic/Button'
import LoadingPlaceholder from '../generic/LoadingPlaceholder'
import Modal from 'react-responsive-modal'
import { transformDate, keepOnPage, getBoolImage, transformSvgPreview } from '../../utils/tools'
import fetch from 'unfetch'
import PropTypes from 'prop-types'

const customModalStyle = {
  overlay: {
    overflowX: 'auto',
  },
}

const renderAnswerList = ( answers, selectVersion ) => {
  const answersList = answers.map( ( answer, i ) => (
    <button className='w-full shadow my-4 ' key={ i } answer={ i } onClick={ selectVersion }>
      <div className='flex p-4 items-center' key={ i }>

        <div className='flex h-40 w-2/5 p-2 text-center justify-center shadow' key={ `${i}-img` } dangerouslySetInnerHTML={ transformSvgPreview( answer.answer ) } />
        <div className='w-1/5 p-2 text-center' key={ `${i}-label` }>{ answer.label }</div>
        <div className='w-1/5 p-2 text-center' key={ `${i}-date` }>
          { transformDate( answer.submit_date ) }
        </div>
        <div className='w-1/5 p-2 text-center content-center' key={ `${i}-is-instructor` }>
          <img className='block mx-auto' src={ getBoolImage( answer.instructor_user !== null ) } alt={ ( answer.instructor_user !== null ).toString()}></img>
        </div>

      </div>
    </button>
  ),
  )
  return (
    <React.Fragment>
      <div className='flex'>
        <div className='w-2/5 p-2 text-center'>Preview</div>
        <div className='w-1/5 p-2 text-center'>Label</div>
        <div className='w-1/5 p-2 text-center'>Submit Date</div>
        <div className='w-1/5 p-2 text-center'>Is Instructor</div>
      </div>
      { answersList }
    </React.Fragment>
  )
}

const VersioningModal = props => {
  const {
    toolUuid, isOpen, closeModal, updateCurrentVersion,
  } = props
  const [answers, setAnswers] = useState( [] )
  const [isSent, setIsSent] = useState( false )
  const [hasMore, setHasMore] = useState( false )
  const [nextHref, setNextHref] = useState( '' )

  useEffect( () => {
    fetch( `/api/v1/student-answer-history/list/${toolUuid}/` )
      .then( response => response.json() )
      .then( data => {
        setAnswers( data.results )
        setIsSent( true )
        setNextHref( data.next )
        setHasMore( !!data.next )
      } )
  }, [toolUuid] )

  const loadMore = () => {
    if ( hasMore ) {
      fetch( nextHref )
        .then( response => response.json() )
        .then( data => {
          setAnswers( [...answers, ...data.results] )
          setIsSent( true )
          setNextHref( data.next )
          setHasMore( !!data.next )
        },
        )
    }
  }

  const selectVersion = ( e ) => {
    e.preventDefault()
    const index = e.currentTarget.attributes.answer.value
    if ( index ) {
      const ok = confirm( keepOnPage( {} ) )
      if ( ok == true ) {
        const answer = answers[index]
        updateCurrentVersion( answer )
        closeModal()
      }
    }
  }

  const ItemList = () => (
    <React.Fragment>
      { renderAnswerList( answers, selectVersion ) }
      { hasMore && ( <div className='flex justify-center'>
          <Button onClick={ loadMore }>Load More</Button>
        </div> )
      }
    </React.Fragment>
  )

  return (
    <Modal
      open={ isOpen }
      onClose={ closeModal }
      contentLabel='Versioning Modal'
      className='max-w-6xl w-full rounded'
      styles={ customModalStyle }
    >
        <div className='p-4 overflow-x-hidden'>
        { ( Array.isArray( answers ) && answers.length )
          ?
            <ItemList />
          :
            <LoadingPlaceholder condition={isSent} />
        }
        </div>
    </Modal>
  )
}

VersioningModal.propTypes = {
  toolUuid: PropTypes.string,
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  updateCurrentVersion: PropTypes.func,
}

export default VersioningModal
