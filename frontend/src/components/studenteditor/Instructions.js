import Button from '../generic/Button'
import InstructionText from '../generic/InstructionText'
import React from 'react'
import PropTypes from 'prop-types'

const Instructions = ( props ) => {
  return (
    <div className='container table mx-auto bg-white border-gray-700 px-10 py-5 \
                    rounded-lg overflow-hidden'>
      <div className='min-h-half'>
        <InstructionText name='Title' content={ props.title }/>
        <InstructionText name='Instructions' content={ props.question } image={ props.questionImage }/>
      </div>
      <Button onClick={ props.openTool } className='float-right'>Go to Drawing Tool</Button>
    </div>
  )
}

Instructions.propTypes = {
  openTool: PropTypes.func,
  title: PropTypes.string,
  question: PropTypes.string,
}

export default Instructions
