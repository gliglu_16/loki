import React from 'react';

const iframeStyle = {
    height: '100vh',
    width: '100%',
};

class MethodDrawIframe extends React.Component {
  constructor(props) {
    super(props);

    this.svgCanvas = {};

    this.initSvgEmbed = this.initSvgEmbed.bind(this);
    this.embeddedSvgEdit = this.embeddedSvgEdit.bind(this);
    this.updateSvg = this.updateSvg.bind(this);
    this.saveSvg = this.saveSvg.bind(this);
  };

  embeddedSvgEdit(frame) {
    this.frame = frame;

    this.callbacks = {};
    this.encode = function (obj) {
      if (window.JSON && JSON.stringify) return JSON.stringify(obj);
      var enc = arguments.callee;

      if (typeof obj == 'boolean' || typeof obj == 'number') {
        return obj + '';
      }else if (typeof obj == 'string') {
        return '"' +
              obj.replace(
                /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
                function (a) {
                  return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                })

              + '"';
      }else if (obj.length) {
        for (var i = 0; i < obj.length; i++) {
          obj[i] = enc(obj[i]);
        }

        return '[' + obj.join(',') + ']';
      }else {
        var pairs = [];
        for (var k in obj) {
          pairs.push(enc(k) + ':' + enc(obj[k]));
        }

        return '{' + pairs.join(',') + '}';
      }
    };

    this.send = function (name, args, callback) {
        var cbid = Math.floor(Math.random() * 31776352877 + 993577).toString();
        this.callbacks[cbid] = callback;
        for (var argstr = [], i = 0; i < args.length; i++) {
          argstr.push(this.encode(args[i]));
        }

        var t = this;
        setTimeout(function () {
          t.frame.contentWindow.postMessage(
            cbid + ";svgCanvas['" + name + "'](" + argstr.join(',') + ')', '*'
          );
        }, 0);

        return cbid;
      };

    var functions = [
      'updateElementFromJson', 'embedImage', 'fixOperaXML', 'clearSelection', 'addToSelection',
      'removeFromSelection', 'addNodeToSelection', 'open', 'save', 'getSvgString', 'setSvgString',
      'createLayer', 'deleteCurrentLayer', 'getCurrentDrawing', 'setCurrentLayer',
      'renameCurrentLayer', 'setCurrentLayerPosition', 'setLayerVisibility', 'moveSelectedToLayer',
      'clear', 'clearPath', 'getNodePoint', 'clonePathNode', 'deletePathNode', 'getResolution',
      'getImageTitle', 'setImageTitle', 'setResolution', 'setBBoxZoom', 'setZoom', 'getMode',
      'setMode', 'getStrokeColor', 'setStrokeColor', 'getFillColor', 'setFillColor',
      'setStrokePaint', 'setFillPaint', 'getStrokeWidth', 'setStrokeWidth', 'getStrokeStyle',
      'setStrokeStyle', 'getOpacity', 'setOpacity', 'getFillOpacity', 'setFillOpacity',
      'getStrokeOpacity', 'setStrokeOpacity', 'getTransformList', 'getBBox', 'getRotationAngle',
      'setRotationAngle', 'each', 'bind', 'setIdPrefix', 'getBold', 'setBold', 'getItalic',
      'setItalic', 'getFontFamily', 'setFontFamily', 'getFontSize', 'setFontSize', 'getText',
      'setTextContent', 'setImageURL', 'setRectRadius', 'setSegType', 'quickClone',
      'changeSelectedAttributeNoUndo', 'changeSelectedAttribute', 'deleteSelectedElements',
      'groupSelectedElements', 'ungroupSelectedElement', 'moveToTopSelectedElement',
      'moveToBottomSelectedElement', 'moveSelectedElements', 'getStrokedBBox', 'getVisibleElements',
      'cycleElement', 'getUndoStackSize', 'getRedoStackSize', 'getNextUndoCommandText',
      'getNextRedoCommandText', 'undo', 'redo', 'cloneSelectedElements', 'alignSelectedElements',
      'getZoom', 'getVersion', 'setIconSize', 'setLang', 'setCustomHandlers',
      'setBackgroundImage',
  ];

    for (var i = 0; i < functions.length; i++) {
      this[functions[i]] = (function (d) {
        return function () {
          var t = this;
          for (var g = 0, args = []; g < arguments.length; g++) {
            args.push(arguments[g]);
          }

          var cbid = t.send(d, args, function () {});

          return function (newcallback) {
            t.callbacks[cbid] = newcallback;
          };
        };
      })(functions[i]);
    }

    var t = this;
    window.addEventListener('message', function (e) {
      if (String(e.data).substr(0, 4) == 'SVGe') {
        var data = e.data.substr(4);
        var cbid = data.substr(0, data.indexOf(';'));
        if (t.callbacks[cbid]) {
          if (data.substr(0, 6) != 'error:') {
            t.callbacks[cbid](eval('(' + data.substr(cbid.length + 1) + ')'));
          } else {
            t.callbacks[cbid](data, 'error');
          }
        }
      }
    }, false);
  };

  initSvgEmbed() {
    const frame = document.getElementById( 'svgedit' )
    this.svgCanvas = new this.embeddedSvgEdit( frame )
    if ( this.props.svgDrawing === undefined && this.props.backgroundImage !== null ) {
      this.svgCanvas.setBackgroundImage( this.props.backgroundImage )
      return
    }
    this.svgCanvas.setSvgString( this.props.svgDrawing )
  }

  updateSvg() {
    try {
      this.svgCanvas.setSvgString( this.props.svgDrawing )
    } catch ( error ) {
      console.log( `Can't update SVG. Error: ${error}` )
    }
  }

  saveSvg() {
    this.svgCanvas.getSvgString()( ( data, error ) => {
      if ( error ) {
        console.log( `Error while saving svg: ${error}` )
      } else {
        this.props.onSave( data )
      }
    } )
  }

  render() {
    return (
      <div className='overflow-auto block md:w-full lg:flex xl:flex w-full'>
        <iframe
          src='/method-draw'
          style={ iframeStyle }
          id='svgedit'
          onLoad={ this.initSvgEmbed }
          />
      </div>
    );
  }
}

MethodDrawIframe.propTypes

export default MethodDrawIframe;
