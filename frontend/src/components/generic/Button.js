import React from 'react'
import PropTypes from 'prop-types'

const Button = ( props ) => {
  const {
    children,
    className,
    icon,
    disabled,
    ...buttonProps
  } = props

  const images = require.context( '../../../static/images', true )
  let img = ''
  try {
    img = images( `./${icon}.svg` )
  } catch ( e ) {}

  const disabledClasses = `${className} cursor-not-allowed opacity-50`

  return (
    <button
      className={ 'px-2 py-1 border border-gray-400 leading-normal rounded-lg \
        hover:bg-gray-100 ' + `${disabled ? disabledClasses : className}` }
      disabled={ disabled }
      { ...buttonProps }
    >
      { icon && <img className='fill-current w-4 h-4' src={ img } alt={ icon }></img> }
      <span>{ children }</span>
    </button>
  )
}

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  icon: PropTypes.string,
  disabled: PropTypes.bool,
}

Button.defaultProps = {
  className: '',
}

export default Button
