import React from 'react'
import Button from './Button'
import PropTypes from 'prop-types'

const renderSkills = ( skills, handleGradeChange ) => {
  const skillsList = []
  skills.map( ( skill, i ) => {
    skillsList.push(
      ( <div key={ i } className='flex'>
        <div className='w-2/5 py-4 px-8 text-center' key={ `${i}-name` }>
          { skill.skill }
        </div>
        <div className='w-1/5 py-4 px-8 text-center' key={ `${i}-weight` }>
          { skill.weight }
        </div>
        <div className='w-2/5 py-4 px-4 text-center' key={ `${i}-grade` }>
          { skill.grade !== null
            ?
            skill.grade
            :
              <input
                className='bg-white border-2 border-gray-200 w-full rounded py-2 px-4 text-gray-700 leading-tight'
                type='number'
                min='0'
                max='100'
                key={ i }
                skill={ i }
                onChange={ handleGradeChange }
              />
            }
        </div>
        <div
          className='w-1/5 py-4 pr-8 text-left text-gray-500 m-auto items-center text-xs'
          key={ `${i}-scale` }
        >
          (0-100)
        </div>
      </div> ),
    )
  } )
  return (
    <React.Fragment>
      <div className='flex border-b border-gray-200'>
        <div className='w-2/5 p-2 text-center'>Skill</div>
        <div className='w-1/5 p-2 text-center'>Weight</div>
        <div className='w-2/5 p-2 text-center'>Grade</div>
        <div className='w-1/5 p-2 text-center'> </div>
      </div>
      { skillsList }
    </React.Fragment>
  )
}

const Skills = props => {
  const {
    skills,
    handleGradeChange,
    handleSubmit,
  } = props

  return (
    <div>
      { renderSkills( skills, handleGradeChange ) }
      <div>
        { skills.every( skill => skill.grade !== null )
          ? null
          : ( <Button
            className='float-right bg-blue-500 hover:bg-blue-700 m-2 py-2 px-4 text-white border-none'
            type='submit'
            onClick={ handleSubmit }
          >
            Submit
          </Button> )
        }
      </div>
    </div>
  )
}

Skills.propTypes = {
  skills: PropTypes.array,
  handleGradeChange: PropTypes.func,
  handleSubmit: PropTypes.func,
}

export default Skills
