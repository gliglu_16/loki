import React, { useState } from 'react'

const CollapsibleContent = props => {

  const [isOpened, setIsOpened] = useState( false )

  const toggle = () => setIsOpened( s => !s )

  const isContentBigEnough = props.text.length > 200 || props.image

  const collapsibleContentHeight = isOpened ? 'auto' : '5em' // 2 rows

  return (
      <React.Fragment>
        <div className='overflow-hidden' style={{ height: isContentBigEnough ? collapsibleContentHeight : 'auto' }}>
          { props.content }
        </div>
        { isContentBigEnough &&
          <a role='button' className='mx-6 px-2 py-1 text-blue-600' onClick={toggle}>
            {isOpened ? 'Show less' : 'Show more'}
          </a>
        }
      </React.Fragment>
  )
}

export default CollapsibleContent
