import React from 'react'
import CollapsibleContent from './CollapsibleContent'
import Instructions from '../studenteditor/Instructions'
import PropTypes from 'prop-types'

const InstructionText = props => {

  const content = (
    <React.Fragment>
      <p className='text-base leading-normal break-words whitespace-pre-wrap p-8'>
        { props.content }
      </p>
      { props.image &&
        <div className='bg-contain bg-center bg-no-repeat h-xl my-4 text-center' style={{ backgroundImage: `url(${props.image})` }}></div>
      }
    </React.Fragment>
  )

  return (
    <div className='instructions-text'>
      <h4 className='text-2xl leading-tight'>{ props.name }</h4>
      <hr />
      { props.isCollapsible
        ? <CollapsibleContent
            text={ props.content }
            image={ props.image }
            content={ content }
          />
        : content
      }
    </div>
  )
}

Instructions.propTypes = {
  isCollapsible: PropTypes.bool,
  name: PropTypes.string,
  content: PropTypes.string,
  image: PropTypes.string,
}

Instructions.defaultProps = {
  isCollapsible: false,
}

export default InstructionText
