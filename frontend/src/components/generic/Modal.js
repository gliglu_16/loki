import React from 'react'
import ReactModal from 'react-modal'
import PropTypes from 'prop-types'

ReactModal.setAppElement( '#app' )

const customStyles = {
  content: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    border: '1px solid rgb(204, 204, 204)',
    background: 'rgb(255, 255, 255) none repeat scroll 0% 0%',
    overflow: 'auto',
    borderRadius: '4px',
    outline: 'currentcolor none medium',
    padding: '28px',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
}

const Modal = ( props ) => {
  const {
    children,
    onRequestClose,
    ...modalProps
  } = props

  return (
    <ReactModal
     style={ customStyles }
     onRequestClose={ onRequestClose }
     { ...modalProps }
    >
      { children }
    </ReactModal>
  )
}

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  onRequestClose: PropTypes.func,
}

export default Modal
