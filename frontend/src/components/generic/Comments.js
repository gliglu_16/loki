import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import Button from './Button'
import { transformDate, scrollToBottom, getLTIUserRepr, compareLTIUsers } from '../../utils/tools'


const renderCommentsList = ( comments, currentLTIUser ) => {
  const commentsList = comments.map( ( comment, i ) => (
    comment.lti_user ?
      <div className='comment block my-8' key={ i }>
        <div className='flex'>
          <div className='author w-2/3 text-lg break-words pr-2' key={ `${i}-author` }>
            { getLTIUserRepr( comment.lti_user ) }
            { compareLTIUsers( comment.lti_user, currentLTIUser )
            && <span className='text-base text-gray-600 ml-1'>(you)</span> }
          </div>
          <div className='metadata overflow-x-hidden w-1/3 text-lg ml-32' key={ `${i}-metadata` }>{ transformDate( comment.submit_date ) }</div>
        </div>
        <div className='text-base break-words whitespace-pre-wrap text-normal mr-8 ml-4' key={ `${i}-text` }>{ comment.comment }</div>
        { comment.answer_history &&
          <div
            className='text-xs'
            key={ `${i}-version` }
          >
            { comment.answer_history.label }
          </div>
        }
      </div>
      :
      <div key={ i }>Error</div>
  ),
  )
  return (
  <div id='commentsList' className='overflow-y-auto w-full h-full p-1 my-4 border-b border-gray-400'>
    { commentsList }
  </div>
  )
}

const SubmitComment = ( props ) => {
  const {
    handleCommentChange,
    submitComment,
    draftComment,
    ...rest
  } = props

  return (
    <div className=''>
      <textarea
        className='resize-none border rounded focus:outline-none focus:shadow-outline w-full'
        onChange={ handleCommentChange }
        value={ draftComment }
        rows='5' cols='100'
      >
      </textarea>
      <Button
        className='bg-white'
        onClick={ ( event ) => {
          submitComment && submitComment( event )
        } }
      >
        Add comment
      </Button>
    </div>
  )
}

const Comments = ( props ) => {
  const {
    comments,
    handleCommentChange,
    submitComment,
    draftComment,
    currentLTIUser,
    ...rest
  } = props

  useEffect( () => (
    scrollToBottom( 'commentsList' )
  ),
  )

  return (
    <div className='flex flex-col justify-between relative bg-gray-100 p-4 w-auto rounded md:w-full m-auto max-h-screen'>
      <div className='block text-center border-b border-gray-400'>
        <h2 className='text-2xl'>Comments</h2>
      </div>
      { renderCommentsList( comments, currentLTIUser ) }
      <SubmitComment
        handleCommentChange={ handleCommentChange }
        submitComment={ submitComment }
        draftComment= { draftComment }
      />
  </div>
  )
}

Comments.propTypes = {
  comments: PropTypes.array,
  handleCommentChange: PropTypes.func,
  submitComment: PropTypes.func,
  draftComment: PropTypes.string,
  currentLTIUser: PropTypes.object,
}

SubmitComment.propTypes = {
  handleCommentChange: PropTypes.func,
  submitComment: PropTypes.func,
  draftComment: PropTypes.string,
}

export default Comments
