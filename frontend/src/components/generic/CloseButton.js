import React from 'react'

const CloseButton = ( props ) => {
  const {
    children,
    className,
    ...buttonProps
  } = props

  const icon = require( '../../../static/images/close-icon.svg' )

  return (
    <button
      className='absolute top-0 right-0 p-1'
      {...buttonProps}
    >
      {icon && <img src={icon} alt='close'></img>}
    </button>
  )
}

export default CloseButton
