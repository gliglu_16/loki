import React from 'react'
import PropTypes from 'prop-types'

const LoadingPlaceholder = props => {
  const {
    condition,
    loadingMsg,
    errorMsg,
  } = props

  const loading = loadingMsg || 'Loading...'
  const error = errorMsg || 'No results found'
  return (
    <div className='text-center m-8' key={1}>
      { !condition ? loading : error }
    </div>
  )
}

LoadingPlaceholder.propTypes = {
  condition: PropTypes.bool,
  loadingMsg: PropTypes.string,
  errorMsg: PropTypes.string,
}

export default LoadingPlaceholder
