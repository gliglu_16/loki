import React from 'react'
import { getBoolImage, getLTIUserRepr, transformSvgPreview } from '../../utils/tools'
import Button from '../generic/Button'
import LinkButton from '../../components/generic/LinkButton'
import PropTypes from 'prop-types'

/**
 * Container for a particular student entry data.
 */
const StudentResult = ( props ) => {
  const {
    studentResult,
    openGradeModal,
  } = props

  return (
        <div className='flex p-4 items-center'>
            <div className='flex w-2/5 p-2 text-center justify-center' dangerouslySetInnerHTML={ transformSvgPreview( studentResult.last_version ? studentResult.last_version.answer : '-' ) } />
            <div className='w-1/5 p-2 break-all text-center' >
              { getLTIUserRepr( studentResult.student_user ) }
            </div>
            <div className='w-1/5 p-2 text-center'>
              { ( studentResult.tool.enable_grading && studentResult.last_version )
                ? [(
                  studentResult.grade
                    ? studentResult.grade
                    :
                    [(
                      studentResult.submitted
                        ? <Button
                          key={ studentResult.id }
                          studentresult={ studentResult.id }
                          onClick={ openGradeModal }
                        >
                          Grade
                        </Button>
                        : '-'
                    ),
                    ]
                ),
                ]
                : '-'
              }

            </div>

            <div className='w-1/5 p-2 text-center content-center' >
              {
                studentResult.last_version &&
                <img className='block mx-auto' src={ getBoolImage( studentResult.submitted ) } alt={ studentResult.submitted.toString() } />
                ||
                '-'
              }
            </div>

            <div className='w-1/5 p-2 text-center' >
              {studentResult.submissions_count}
              { studentResult.tool.max_attempts ? ` / ${studentResult.tool.max_attempts}` : ''}
            </div>

            <div className='w-1/5 p-2 text-center' >
              <LinkButton
                className='text-normal'
                to={ `/versions/${studentResult.id}` }
              >
                Show versions
              </LinkButton>
          </div>
          </div>
  )

}

StudentResult.propTypes = {
  studentResult: PropTypes.object,
}

export default StudentResult
