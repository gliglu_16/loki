import React, { useState, useEffect } from 'react'
import LoadingPlaceholder from '../generic/LoadingPlaceholder'
import Skills from '../generic/Skills'
import Modal from 'react-responsive-modal'
import fetch from 'unfetch'
import PropTypes from 'prop-types'

const customModalStyle = {
  overlay: {
    overflowX: 'auto',
  },
}

const GradeSkillsModal = props => {
  const {
    studentResultId, isOpen, closeModal,
  } = props
  const [skills, setSkills] = useState( [] )
  const [isSent, setIsSent] = useState( false )
  const [isPosted, setIsPosted] = useState( false )
  const [isSuccess, setIsSuccess] = useState( false )
  const [gradesError, setGradesError] = useState( '' )
  const [gradesNotification, setGradesNotification] = useState( '' )

  useEffect( () => {
    fetch( `/api/v1/skill/list/${studentResultId}/` )
      .then( response => response.json() )
      .then( data => {
        setSkills( data )
        setIsSent( true )
      } )
      .catch( error => {
        setIsSent( true )
      } )
  }, [studentResultId] )

  const handleGradeChange = e => {
    e.preventDefault()
    const index = e.currentTarget.attributes.skill.value
    if ( index ) {
      const updatedSkills = skills
      updatedSkills[index].chandedGrade = e.target.value
      setSkills( [...updatedSkills] )
    }
  }

  const checkAllGraded = ( skills ) => {
    return skills.every(
      skill => (
        skill.chandedGrade !== undefined
          ? skill.chandedGrade !== null && skill.chandedGrade !== ''
          : skill.grade !== null && skill.grade !== undefined // NOTE: temporary check (re-grading)
      ) )
  }

  const checkGradingScale = ( skills ) => {
    return skills.every(
      skill => (
        skill.chandedGrade !== undefined
          ? skill.chandedGrade >= 0 && skill.chandedGrade <= 100
          : skill.grade >= 0 && skill.grade <= 100 // NOTE: temporary check (re-grading)
      ) )
  }

  const handleSubmitGrades = ( e ) => {
    setIsPosted( false )
    setGradesError( '' )
    e.preventDefault()
    const allGraded = checkAllGraded( skills )
    const gradingScaleRespected = checkGradingScale( skills )
    if ( allGraded && gradingScaleRespected ) {
      skills.forEach( ( skill, i ) => {
        // NOTE: Temporary `chandedGrade` check should be removed once we disable re-grading
        if ( skill.chandedGrade !== undefined && skill.chandedGrade !== null ) {
          fetch( '/api/v1/grade/create/', {
            method: 'POST',
            headers: {
              'X-CSRFToken': Cookies.get( 'csrftoken' ),
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify( {
              student_result: studentResultId,
              skill: skill.id,
              grade: skill.chandedGrade,
            } ),
          } )
            .then( response => {
              setIsPosted( true )
              if ( !response.ok ) {
                setIsSuccess( false )
                return response
              }
            } )
        }
      } )
      setIsSuccess( true )
      setGradesNotification( 'Successfully submitted.' )
      setTimeout( closeModal, 1000 )
    } else {
      let errorMessage = 'All skills must be graded.'
      if ( allGraded && !gradingScaleRespected ) {
        errorMessage = 'Please use the values from 0 to 100.'
      }
      setGradesError( errorMessage )
    }
  }

  return (
    <Modal
      open={ isOpen }
      onClose={ closeModal }
      contentLabel='Grade Skills Modal'
      className='max-w-6xl w-full rounded'
      styles={ customModalStyle }
    >
        <div className='p-4 overflow-x-hidden'>
          { gradesNotification &&
          <div className='flex justify-center mb-4 text-green-600'>
            { gradesNotification }
          </div>}
          { gradesError &&
            <div className='flex justify-center mb-4 text-red-600'>
            { gradesError }
          </div>}
        {
          ( !isSuccess && isPosted ) && <div className='flex justify-center mb-4 text-red-600'>
            <p>An error occurred, please try again later!</p>
          </div>
        }

        { ( Array.isArray( skills ) && skills.length )
          ?
            <Skills
              skills={ skills }
              handleGradeChange={ handleGradeChange }
              handleSubmit={ handleSubmitGrades }
            />
          :
            <LoadingPlaceholder condition={ isSent } errorMsg='No skills found.' />
        }
        </div>
    </Modal>
  )
}

GradeSkillsModal.propTypes = {
  toolUuid: PropTypes.string,
  studentResultId: PropTypes.string,
  isOpen: PropTypes.bool,
  closeModal: PropTypes.func,
}

export default GradeSkillsModal
