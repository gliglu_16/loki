"""
Integration tests for frontend views.
"""

from ddt import data, ddt
from django.contrib.auth.models import User
from django.test import TestCase
from mock import patch

from drawing_tool.tests.factories import DrawingToolFactory, StudentResultFactory
from loki.constants import INSTRUCTOR, STUDENT
from lti.tests.factories import LTIConsumerFactory, LTIUserFactory


@ddt
@patch('lti.views.DjangoToolProvider')
class FrontendViewsTest(TestCase):
    """
    Test frontend views rendering.
    """

    def setUp(self):
        self.user = User.objects.create_user('test', 'test@test.com', 'test123')
        self.lti_consumer = LTIConsumerFactory.create()
        self.drawing_tool = DrawingToolFactory.create(lti_consumer=self.lti_consumer)
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.client.force_login(user=self.user)

    def _set_up_lti_init(self, is_instructor=True):
        """
        Set up lti to pass `lti_only` check.

        Since mocking the `lti_only` decorator didn't help...

        NOTE: DRY.
        """
        mocked_nonce = '135685044251684026041377608307'
        mocked_timestamp = '1234567890'
        mocked_decoded_signature = 'my_signature='
        self.headers = {
            'user_id': 'd41d8cd98f00b204e9800998ecf8427e',
            'lis_person_name_full': 'Test Username',
            'lis_person_name_given': 'First',
            'lis_person_name_family': 'Second',
            'lis_person_contact_email_primary': 'test@test.com',
            'lis_person_sourcedid': 'Test_Username',
            'oauth_callback': 'about:blank',
            'launch_presentation_return_url': '',
            'lti_message_type': 'basic-lti-launch-request',
            'lti_version': 'LTI-1p0',
            'roles': INSTRUCTOR if is_instructor else STUDENT,
            'context_id': 1,
            'tool_consumer_info_product_family_code': 'moodle',
            'context_title': 'Test title',
            'tool_consumer_instance_guid': 'test.dot.com',

            'resource_link_id': 'dfgsfhrybvrth',
            'lis_result_sourcedid': 'wesgaegagrreg',

            'oauth_nonce': mocked_nonce,
            'oauth_timestamp': mocked_timestamp,
            'oauth_consumer_key': 'consumer_key',
            'oauth_signature_method': 'HMAC-SHA1',
            'oauth_version': '1.0',
            'oauth_signature': mocked_decoded_signature
        }
        lti_init_response = self.client.post(
            f'/lti/{self.drawing_tool.uuid}/', data=self.headers, follow=True)
        self.assertEqual(lti_init_response.status_code, 200)

    @data("/", "/instructor/editor/1", "/student-results", "/versions/1")
    def test_permissions_check_success(self, url, mocked):
        """
        Test permissions check success.

        Ensure is_instructor check is called for frontend views.
        """
        self._set_up_lti_init()
        mocked.return_value.is_valid_request.return_value = True

        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='frontend/index.html')

    def test_index_page_student_success(self, mocked):
        """
        Ensure index page renders successfully for student.

        Instuctor role isn't required for an index page.
        """
        self._set_up_lti_init(is_instructor=False)
        mocked.return_value.is_valid_request.return_value = True

        response = self.client.get("/", follow=True)
        self.assertEqual(response.status_code, 200)

    @data("/instructor/editor/1", "/student-results", "/versions/1")
    def test_permissions_check_failure(self, url, mocked):
        """
        Test permissions check success.

        Ensure 403 Forbidden is raised if instructor_only check isn't passed.
        """
        self._set_up_lti_init(is_instructor=False)
        mocked.return_value.is_valid_request.return_value = True

        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 403)
