from django.urls import path, re_path

from frontend import views

app_name = 'frontend'

urlpatterns = [
    path('method-draw', views.MethodDrawView.as_view(), name='method-draw'),
    re_path('instructor', views.FrontendView.as_view(),
            {'is_instructor': True}, name='instructor-index'),
    re_path(r'instructor/editor/(?P<tool_student_result_id>.*)/(?P<student_answer_id>.*)',
            views.FrontendView.as_view(), {'is_instructor': True}),
    re_path(r'versions/(?P<ver_student_result_id>.*)',
            views.FrontendView.as_view(), {'is_instructor': True}),
    re_path(r'student-results', views.FrontendView.as_view(), {'is_instructor': True}),
    path('', views.FrontendView.as_view(), {'is_instructor': False}, name='student-index'),
]
