"""
API utils.
"""
import re
import xml.etree.cElementTree as et


def is_svg(xml_string):
    """
    Check if an xml string is SVG.

    Validate XML structure as well.

    Ref.: https://stackoverflow.com/a/15136684
    """
    try:
        xml_data = et.fromstringlist(xml_string)
        tag = xml_data.tag
    except et.ParseError:
        return False
    return tag == '{http://www.w3.org/2000/svg}svg'


def validate_uuid(uuid):
    """
    Validate uuid.
    """

    uuid_pattern = _create_uuid_pattern('[1-5]')
    return bool(uuid_pattern.match(uuid))


def _create_uuid_pattern(version):
    """
    Create uuid validation pattern.

    Ref.:
        https://gist.github.com/kgriffs/c20084db6686fee2b363fdc1a8998792
    """
    return re.compile(
        (
            '[a-f0-9]{8}-' +
            '[a-f0-9]{4}-' +
            version + '[a-f0-9]{3}-' +
            '[89ab][a-f0-9]{3}-' +
            '[a-f0-9]{12}$'
        ),
        re.IGNORECASE
    )
