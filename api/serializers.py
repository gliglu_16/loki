"""
API serializers.
"""
from collections import OrderedDict

from django.utils.translation import gettext as _
from rest_framework import serializers

from api.mixins import ValidateInstructorRole
from api.utils import is_svg, validate_uuid
from comments.models import Comment
from drawing_tool.models import (AnswerHistory, DrawingTool, Skill, SkillGrade,
                                 StudentResult)
from loki.constants import INSTRUCTOR
from lti.models import LTIUser


class LTIUserSerializer(serializers.ModelSerializer):
    """
    Serializer for a `lti.models.LTIUser` model.
    """

    class Meta:
        model = LTIUser
        fields = [
            "user_id",
            "user_username",
            "user_fullname",
        ]


class DrawingToolSerializer(serializers.ModelSerializer):
    """
    Serializer for a `drawing_tool.models.DrawingTool` model.
    """

    class Meta:
        model = DrawingTool
        fields = [
            "display_name",
            "question",
            "question_image",
            "hints",
            "answer",
            "show_answer",
            "enable_grading",
            "max_attempts",
            "enable_comments",
            "background_image",
        ]


class StudentResultSerializer(serializers.ModelSerializer):
    """
    Serializer for a `drawing_tool.models.StudentResult` model.
    """

    student_user = LTIUserSerializer(source="lti_user", many=False, read_only=True)
    tool = serializers.SerializerMethodField()
    submissions_count = serializers.SerializerMethodField()
    last_version = serializers.SerializerMethodField()

    class Meta:
        model = StudentResult
        fields = [
            "id",
            "student_user",
            "tool",
            "submissions_count",
            "last_version",
            "grade",
            "submitted",
        ]

    @staticmethod
    def get_tool(obj):
        """
        Related `DrawingTool` instance's fields.
        """
        return OrderedDict({
            "enable_grading": obj.tool.enable_grading,
            "max_attempts": obj.tool.max_attempts,
        })

    @staticmethod
    def get_submissions_count(obj):
        """
        Related `AnswerHistory` submissions count.
        """
        return AnswerHistory.objects.filter(
            student_result=obj,
            is_submission=True,
        ).count() or 0

    @staticmethod
    def get_last_version(obj):
        """
        Latest related `AnswerHistory` instance's fields.
        """
        qs = AnswerHistory.objects.filter(student_result=obj)
        answer_history = qs.latest("submit_date") if qs else None
        return OrderedDict({
            "answer": answer_history.answer,
            "is_submission": answer_history.is_submission,
        }) if answer_history else None


class AnswerHistorySerializer(serializers.ModelSerializer):
    """
    Serializer for a `drawing_tool.models.AnswerHistory` model.
    """

    instructor_user = LTIUserSerializer(many=False, read_only=True)
    student_result = StudentResultSerializer(many=False, read_only=True)

    class Meta:
        model = AnswerHistory
        fields = [
            "id",
            "label",
            "submit_date",
            "answer",
            "is_submission",
            "instructor_user",
            "student_result",
        ]

    @staticmethod
    def validate_answer(value):
        """
        Check that the answer is SVG.

        Example of a valid value:
            ```
            <svg xmlns="http://www.w3.org/2000/svg" width="200" height="100" version="1.1">
                <rect width="200" height="100" stroke="black" stroke-width="6" fill="green"/>
            </svg>
            ```
        """
        if not is_svg(value):
            raise serializers.ValidationError(
                _("An answer should contain SVG data in valid XML format."))
        return value

    def validate_is_submission(self, value):
        """
        Validate `is_submission`.

        If `student_user_id` is defined (an instructor is saving a version for a student),
        this can't be a submission (an instructor can not post a submission for their student).
        """
        data = self.initial_data
        if data.get("is_submission") == "True" and data.get("student_user_id"):
            raise serializers.ValidationError(
                _("An instructor can not post a submission for their student."))
        return value

    def validate(self, attrs):
        """
        Validate additional data.
        """
        tool_uuid = self.context.get("tool_uuid")
        student_user_id = self.context.get("student_user_id")
        current_student_user_id = self.context.get("current_student_user_id")

        if not validate_uuid(tool_uuid):
            raise serializers.ValidationError(_("Tool id should be of UUID type."))

        if student_user_id:
            # Case #1: An instructor is creating an entry for a specified student.
            if INSTRUCTOR not in self.context.get("roles", []):
                raise serializers.ValidationError(
                    _("Instructor role is required in the current session."))
        else:
            # Case #2: Regular student submission
            if not DrawingTool.validate_attempts(tool_uuid, current_student_user_id):
                raise serializers.ValidationError(_("Can not exceed max submissions attempts."))
        return attrs


class AnswerHistoryShortcutSerializer(serializers.ModelSerializer):
    """
    Serializer with minimum fields for a `drawing_tool.models.AnswerHistory` model.
    """

    class Meta:
        model = AnswerHistory
        fields = [
            "id",
            "label",
        ]


class AnswerHistoryListSerializer(serializers.ListSerializer, ValidateInstructorRole):
    """
    List serializer for a `drawing_tool.models.AnswerHistory` model w/ grouping by student result.

    Answer history entries are grouped by student result (user id as a key).
    """

    def update(self, instance, validated_data):
        raise NotImplementedError(_("This method isn't supposed to be used."))

    def to_representation(self, data):
        """
        List of object instances -> List of dicts of primitive datatypes.

        Example of representation:
        ::
            [
                [
                    "d41d8cd98f00b204e9800998ecf8427e", [
                        {
                            "id": 1,
                            "label": "test_label1",
                            # other fields
                        },
                        {
                            "id": 1,
                            "label": "test_label2",
                            # other fields
                        }
                    ]
                ]
            ]

        """
        # NOTE: this logic doesn't belong here, move it to `validate()` (doesn't work there though)
        self._validate_instructor_role_session()

        grouped = dict()
        for obj in data:
            grouped.setdefault(
                obj.student_result.lti_user.user_id, []
            ).append(self.child.to_representation(obj))
        return [[k, v] for k, v in grouped.items()]

    def _validate_instructor_role_session(self):
        """
        Ensure an instructor role is present in the current session.
        """
        if not self._is_instructor():
            raise serializers.ValidationError(
                _("Instructor role is required in the current session."))


class GroupedAnswerHistorySerializer(AnswerHistorySerializer):
    """
    Serializer for a `drawing_tool.models.AnswerHistory` model w/ grouping by student result.

    For a list view, answer history entries are grouped by student result (user isd as a key).
    """

    class Meta:
        model = AnswerHistory
        list_serializer_class = AnswerHistoryListSerializer
        fields = [
            "id",
            "label",
            "submit_date",
            "answer",
            "is_submission",
            "instructor_user",
        ]


class CommentSerializer(serializers.ModelSerializer, ValidateInstructorRole):
    """
    Serializer for a `comment.models.Comment` model.
    """

    lti_user = LTIUserSerializer(many=False, read_only=True)
    student_result = StudentResultSerializer(many=False, read_only=True)
    answer_history = AnswerHistoryShortcutSerializer(many=False, read_only=True)

    class Meta:
        model = Comment
        fields = [
            "lti_user",
            "student_result",
            "answer_history",
            "comment",
            "submit_date",
        ]

    def validate(self, attrs):
        """
        Validate context data.

        A student is eligible to comment on their own student results only,
        while an instructor can comment on any result.
        """
        if not self._is_instructor():
            if self.context.get("student_result_user_id") != self.context.get("current_user_id"):
                raise serializers.ValidationError(_(
                    "A student is not eligible to comment on other students' results."
                ))
        return attrs


class SkillSerializer(serializers.ModelSerializer):
    """
    Serializer for a `drawing_tool.models.Skill` model.
    """

    grade = serializers.SerializerMethodField()

    class Meta:
        model = Skill
        fields = [
            "id",
            "skill",
            "weight",
            "grade",
        ]

    def get_grade(self, obj):
        """
        Get grade based on list of `SkillGrades` objects from context.
        """
        skill_grades = self.context.get('skill_grades', [])
        grade_list = filter(lambda x: (x.skill.id == obj.id), skill_grades)
        skill_grade = next(grade_list, None)
        grade = skill_grade.grade if skill_grade else None
        return grade


class SkillGradeSerializer(serializers.ModelSerializer, ValidateInstructorRole):
    """
    Serializer for a `drawing_tool.models.SkillGrade` model.
    """

    class Meta:
        model = SkillGrade
        fields = [
            "skill",
            "grade",
            "student_result",
        ]

    def validate(self, attrs):
        """
        Custom serializer validation.

        A student is not eligible to grade.
        """
        if not self._is_instructor():
            raise serializers.ValidationError(_(
                "A student is not eligible to grade."
            ))
        return attrs


class SkillTitleListSerializer(serializers.ListSerializer):
    """
    List serializer for a `drawing_tool.models.Skill` model.

    Skills are represented by their titles only.
    """

    def to_representation(self, data):
        """
        List of object instances -> List of dicts of primitive datatypes.

        Example or representation:
        ::

            [
                "Javascript",
                "CSS",
                "HTML",
                "Python",
                "Java"
            ]

        """
        return [obj.skill for obj in data]


class SkillTitleSerializer(serializers.ModelSerializer):
    """
    Serializer for a `drawing_tool.models.Skill` model.
    """

    class Meta:
        model = Skill
        fields = [
            "skill",
        ]
        list_serializer_class = SkillTitleListSerializer
