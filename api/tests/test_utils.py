"""
Utils testing logic.
"""

import pytest

from api.utils import (
    is_svg,
    validate_uuid,
)


@pytest.mark.parametrize("description, xml_string, result", [
    (
        "1. Success",
        "<svg xmlns='http://www.w3.org/2000/svg' width='200' height='100' version='1.1'>"
        "<rect width='200' height='100' stroke='black' stroke-width='6' fill='green'/></svg>",
        True
    ),
    (
        "2. Success, comment in svg tag",
        "<svg xmlns='http://www.w3.org/2000/svg' width='200' height='100' version='1.1'>"
        "<!-- Comment --></svg>",
        True
    ),
    (
        "3. Success, many elements in svg tag",
        "<svg xmlns='http://www.w3.org/2000/svg' width='200' height='100' version='1.1'>"
        "<g><path></path><rect></rect></g><img></img></svg>",
        True
    ),
    (
        "4. Failure, invalid XML",
        "nada nada <svg xmlns='http://www.w3.org/2000/svg' width='200' height='100' version='1.1'>"
        "<rect width='200' height='100' stroke='black' stroke-width='6' fill='green'/></svg>",
        False
    ),
    (
        "5. Failure, rect is a top tag (svg is expected).",
        "<rect width='200' height='100' stroke='black' stroke-width='6'>"
        "<svg xmlns='http://www.w3.org/2000/svg' width='200' height='100' version='1.1'>"
        "</svg></rect>",
        False
    ),

])
def test_is_svg(description, xml_string, result):
    """
    Test SVG validation util.

    Cases:

    #. Success, comment in svg tag.
    #. Success, many elements in svg tag.
    #. Failure, invalid XML.
    #. Failure, rect is a top tag (svg is expected).
    """

    assert is_svg(xml_string) == result


@pytest.mark.parametrize("description, value, result", [
    (
        "1. Success (lowercase).",
        "0bcd02ec-583d-4f09-8b27-4936bb9f28e5",
        True
    ),
    (
        "2. Success (uppercase).",
        "0BCD02EC-583d-4f09-8b27-4936bb9f28e5",
        True
    ),
    (
        "3. Failure (invalid uuid value).",
        "0bcd02ec",
        False
    ),
])
def test_validate_uuid(description, value, result):
    """
    Test UUID validation util.

    Cases:

    #. Success (lowercase).
    #. Success (uppercase).
    #. Failure (invalid uuid value).
    """

    assert validate_uuid(value) == result
