"""
Test REST API v1 views.
"""

from ddt import data, ddt, unpack
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from api.serializers import DrawingToolSerializer
from comments.models import Comment
from comments.tests.factories import CommentFactory
from drawing_tool.models import AnswerHistory, StudentResult, Skill
from drawing_tool.tests.factories import (
    AnswerHistoryFactory,
    DrawingToolFactory,
    StudentResultFactory,
    SkillFactory,
    SkillGradeFactory,
)
from loki.constants import INSTRUCTOR
from lti.tests.factories import LTIConsumerFactory, LTIUserFactory


class ApiV1Test(TestCase):
    """
    Base API v1 testing logic.
    """

    def setUp(self):
        self.client = APIClient()
        self.lti_consumer = LTIConsumerFactory.create()
        self.user = User.objects.create_user('test', 'test@test.com', 'test')
        self.drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id1',
            max_attempts=1000,
        )
        self.set_session_lti_flag()
        self.set_session_lti_data()

    def tearDown(self):
        self.client.logout()

    def set_instructor_role(self):
        """
        Add Instructor role to the test session.
        """
        session = self.client.session
        session['ROLES'] = [INSTRUCTOR]
        session.save()

    def set_session_lti_flag(self):
        """
        Add Instructor role to the test session.
        """
        session = self.client.session
        session['LTI_POST'] = {'id': 'test_LTI_post'}
        session.save()

    def set_session_lti_data(self):
        """
        Add LTI data to the test session.
        """
        session = self.client.session
        session['LTI_CONSUMER_ID'] = str(self.lti_consumer.id)
        session.save()

    def update_session_lti_data(self, user_id=None):
        """
        Update LTI data with LTI user id.
        """
        session = self.client.session
        session['USER_ID'] = str(user_id) if user_id else (
            str(self.lti_user.user_id) if getattr(self, "lti_user") else None
        )
        session.save()


class DrawingToolDetailViewTestCases(ApiV1Test):
    """
    Drawing Tool Detail View tests.
    """

    def test_drawing_tool_detail_view_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(f"/api/v1/drawingtool/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_drawing_tool_detail_view_authenticated_success(self):
        """
        Test successful calls.

        Cases:

        #. Typical success case. Ensure all fields are present in the response.
        #. With two tools stored in a system, a particular one is returned.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get(f"/api/v1/drawingtool/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Ensure all listed fields are present in the response

        self.assertEqual(len(DrawingToolSerializer.Meta.fields), len(response.data))

        # Two tools, particular one is returned
        second_drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id2',
        )
        response = self.client.get(f"/api/v1/drawingtool/{second_drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(DrawingToolSerializer.Meta.fields), len(response.data))

    def test_drawing_tool_detail_view_authenticated_not_found(self):
        """
        Test not-found cases.

        Cases:

        #. uuid of wrong format;
        #. uuid of correct format, but no such object exists.
        """
        self.client.force_authenticate(user=self.user)

        # uuid of wrong format
        response = self.client.get("/api/v1/drawingtool/123/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # uuid of correct format, but no such object exists
        response = self.client.get("/api/v1/drawingtool/111d02ec-583d-4f09-8b27-4936bb9f28e5/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class AnswerHistoryDetailViewTestCases(ApiV1Test):
    """
    Answer History Detail View tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
            user_id='d41d8cd98f00b204e9800998ecf8zzzz',
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.answer_history = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label1"
        )

    def test_answer_history_detail_view_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(f"/api/v1/answer-details/{self.answer_history.id}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_answer_history_detail_view_authenticated_success(self):
        """
        Test successful calls.

        Cases:

        #. Typical success case. Ensure all fields are present in the response.
        #. With two answers stored in a system, a particular one is returned.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get(f"/api/v1/answer-details/{self.answer_history.id}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Ensure all listed fields are present in the response
        self.assertEqual(7, len(response.data))

        # Two tools, particular one is returned
        AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label2"
        )
        response = self.client.get(f"/api/v1/answer-details/{self.answer_history.id}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(7, len(response.data))
        self.assertEqual("test_label1", response.data.get("label"))

    def test_answer_history_detail_view_authenticated_not_found(self):
        """
        Test not-found cases.

        Cases:

        #. id is not integer;
        #. no such object exists.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get("/api/v1/answer-details/aaa/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        response = self.client.get("/api/v1/answer-details/3241/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


@ddt
class AnswerHistoryCreateViewTestCases(ApiV1Test):
    """
    Answer History create view tests.
    """

    valid_answer = "<svg xmlns='http://www.w3.org/2000/svg' width='200' height='100' version='1.1'>" \
                   "<rect width='200' height='100' stroke='black' stroke-width='6' fill='green'/></svg>"

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.update_session_lti_data()

    def test_answer_history_creation_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.post(
            "/api/v1/answer-history/create/",
            data={
                "tool_uuid": self.drawing_tool.uuid,
                "answer": self.valid_answer,
                "label": "test_label",
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @data(
        (
            "1. Typical successful call.",
            {
                "answer": valid_answer,
                "label": "test_label",
                "is_submission": True,
            },
            1,
        ),
        (
            "2. Ensure label is optional.",
            {
                "answer": valid_answer,
                "is_submission": True,
            },
            1,
        ),
        (
            "3. Param 'is_submission' is processed correctly (marked as such only if provided in a request).",
            {
                "answer": valid_answer,
                "is_submission": False,
            },
            1,
        ),
    )
    @unpack
    def test_answer_history_creation_success(self, description, payload, entries_number):
        """
        Test successful calls.

        Cases:

        #. Typical successful call.
        #. Ensure label is optional.
        #. Param `is_submission` is processed correctly (marked as such only if provided in a request).
        """
        self.client.force_authenticate(user=self.user)

        payload["tool_uuid"] = self.drawing_tool.uuid
        response = self.client.post("/api/v1/answer-history/create/", data=payload)
        response_keys = list(response.data.keys())

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AnswerHistory.objects.all().count(), entries_number)
        # Check response fields nomenclature
        self.assertTrue("id" in response_keys)
        self.assertTrue("submit_date" in response_keys)
        self.assertTrue("answer" in response_keys)
        self.assertTrue("label" in response_keys)
        # Ensure an entry is saved as a student submission.
        self.assertEqual(response.data.get("is_submission"), payload.get("is_submission"))
        self.assertIsNone(response.data.get("instructor_user"))
        # Ensure `StudentResult` belongs to a respective student user
        student_result = StudentResult.objects.filter(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        ).first()
        self.assertEqual(student_result.id, self.student_result.id)

    def test_answer_history_creation_several_entries(self):
        """
        Test consecutive creation.

        Ensure entries immutability (created, not updated).
        """
        self.client.force_authenticate(user=self.user)

        payload = {
            "tool_uuid": self.drawing_tool.uuid,
            "answer": self.valid_answer,
            "label": "test_label",
        }
        self.client.post("/api/v1/answer-history/create/", data=payload)
        response = self.client.post("/api/v1/answer-history/create/", data=payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AnswerHistory.objects.all().count(), 2)

    def test_answer_history_creation_attempts_check_failure(self):
        """
        Test attempts check failure during answer history entry creation.
        """
        self.client.force_authenticate(user=self.user)
        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id11',
            max_attempts=1,
        )
        lti_user = LTIUserFactory.create(
            lti_consumer=self.lti_consumer,
            django_user=self.user,
        )
        StudentResultFactory.create(
            tool=drawing_tool,
            lti_user=lti_user
        )
        payload = {
            "tool_uuid": drawing_tool.uuid,
            "answer": self.valid_answer,
            "label": "test_label",
            "is_submission": True,
        }
        self.client.post("/api/v1/answer-history/create/", data=payload)
        response = self.client.post("/api/v1/answer-history/create/", data=payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data.get("non_field_errors")[0], 'Can not exceed max submissions attempts.'
        )

    def test_answer_history_creation_no_resources(self):
        """
        Test the cases when related resources are not found.

        Cases:

        #. Non-existing drawing tool (404 not found);
        #. Non-existing student response (404 not found; a student result isn't created).
        """
        self.client.force_authenticate(user=self.user)

        # Non-existing drawing tool
        response = self.client.post(
            "/api/v1/answer-history/create/",
            data={
                "tool_uuid": "0bcd02ec-583d-4f09-8b27-4936bb9f28e5",
                "answer": self.valid_answer,
                "label": "test_label",
            }
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Non-existing student response
        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id2',
        )
        response = self.client.post(
            "/api/v1/answer-history/create/",
            data={
                "tool_uuid": drawing_tool.uuid,
                "answer": self.valid_answer,
                "label": "test_label",
            }
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_answer_history_creation_invalid_payload(self):
        """
        Test the cases with invalid request params.

        Cases:

        #. invalid `answer`;
        #. invalid `tool_uuid`.
        """
        self.client.force_authenticate(user=self.user)

        # Test invalid `answer`
        response = self.client.post(
            "/api/v1/answer-history/create/",
            data={
                "tool_uuid": self.drawing_tool.uuid,
                "answer": f"INVALID_DATA {self.valid_answer}",
                "label": "test_labels",
            }
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Test invalid `tool_uuid`
        response = self.client.post(
            "/api/v1/answer-history/create/",
            data={
                "tool_uuid": "INVALID_DATA",
                "answer": self.valid_answer,
                "label": "test_label",
            }
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @data(
        (
            "1. Typical successful call (instructor is saving their version of an image for a particular student).",
            {
                "is_present_student_user_id": True,  # Present in the request
                "is_instructor_role": True,
            },
            {
                "status": status.HTTP_201_CREATED,
                "is_present_instructor_user": True,  # Present in the response
                "is_submission": False,
            },
        ),
        (
            "2. Instructor role is present in a session, but no student id passed -> success "
            "(it's a student's submission, with instructor being a student).",
            {
                "is_present_student_user_id": True,
                "is_instructor_role": True,
            },
            {
                "status": status.HTTP_201_CREATED,
                "is_submission": False,
                "is_present_instructor_user": True,
            },
        ),
    )
    @unpack
    def test_answer_history_creation_by_instructor_success(self, description, input_instructions, output):
        """
        Test a case when an instructor saves their version of an image (success cases).

        Cases:

        #. Typical successful call (instructor is saving their version of an image for a particular student).
        #. Instructor role is present in a session, but no student id passed -> success (it's a student submission, with instructor being a student).
        """
        instructor_user = User.objects.create_user(
            'test_instructor', 'test_instructor@test.com', 'test_instructor')
        self.client.force_authenticate(user=instructor_user)
        if input_instructions.get("is_instructor_role"):
            self.set_instructor_role()

        instructor_lti_user = LTIUserFactory(
            django_user=instructor_user,
            lti_consumer=self.lti_consumer,
            user_id="f41d8cd98f00b204e9800998ecf8427e",
        )
        self.update_session_lti_data(user_id=instructor_lti_user.user_id)

        payload = {
            "tool_uuid": self.drawing_tool.uuid,
            "answer": self.valid_answer,
            "label": "test_label",
        }
        if input_instructions.get("is_present_student_user_id"):
            payload["student_user_id"] = self.lti_user.user_id
        if input_instructions.get("is_submission"):
            payload["is_submission"] = True

        response = self.client.post("/api/v1/answer-history/create/", data=payload)

        self.assertEqual(response.status_code, output.get("status"))

        self.assertEqual(response.data.get("is_submission"), output.get("is_submission"))

        self.assertEqual(AnswerHistory.objects.all().count(), 1)
        # Ensure `StudentResult` belongs to a specified student user, not instructor
        student_result = StudentResult.objects.filter(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        ).first()
        self.assertEqual(student_result.id, self.student_result.id)

        if output.get("is_present_instructor_user"):
            # Ensure `instructor_user` is assigned
            self.assertEqual(response.data.get("instructor_user", {}).get(
                "user_id"), instructor_lti_user.user_id)
        else:
            self.assertIsNone(response.data.get("instructor_user"))

    @data(
        (
            "1. Instructor role not present in a session -> validation error.",
            {
                "is_instructor_role": False,
                "is_submission": False,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
                # Serializer non-field validation
                "non_field_errors": "Instructor role is required in the current session.",
            },
        ),
        (
            "2. Instructor posting submission for their student -> validation error.",
            {
                "is_instructor_role": True,
                "is_submission": True,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
                # Serializer field validation
                "is_submission": ["An instructor can not post a submission for their student."],
            },
        ),
        (
            "3. An instructor saves a version for themselves -> validation error.",
            {
                "is_instructor_role": True,
                "is_student": True,
                "is_submission": False,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
                "error": "An instructor shouldn't be saving a version for themselves.",
            },
        ),
        (
            "4. An instructor saves a version for a non-existing user -> validation error.",
            {
                "is_instructor_role": True,
                "student_user_id": "non_existing_student_user_id",
                "is_submission": False,
            },
            {
                "status": status.HTTP_404_NOT_FOUND,
                "error": "No LTIUser matches the given query.",  # Default Django msg
            },
        ),
    )
    @unpack
    def test_answer_history_creation_by_instructor_errors(self, description, input_instructions, output):
        """
        Test error cases of history creation by an instructor.

        Cases:

        #. Instructor role not present in a session -> 400, validation error.
        #. Instructor posting submission for their student -> 400, validation error.
        #. An instructor saves a version for themselves -> 400, validation error.
        #. An instructor saves a version for a non-existing user -> 404, validation error.
        """
        instructor_user = User.objects.create_user(
            'test_instructor', 'test_instructor@test.com', 'test_instructor')
        self.client.force_authenticate(user=instructor_user)

        if input_instructions.get("is_instructor_role"):
            self.set_instructor_role()

        student_user_id = input_instructions.get("student_user_id") or self.lti_user.user_id
        if input_instructions.get("is_student"):
            instructor_lti_user = LTIUserFactory(
                django_user=instructor_user,
                lti_consumer=self.lti_consumer,
                user_id="f41d8cd98f00b204e9800998ecf8427e",
            )
            self.update_session_lti_data(user_id=instructor_lti_user.user_id)
            student_user_id = instructor_lti_user.user_id

        payload = {
            "tool_uuid": self.drawing_tool.uuid,
            "answer": self.valid_answer,
            "label": "test_label",
            "student_user_id": student_user_id,
            "is_submission": input_instructions.get("is_submission"),
        }

        response = self.client.post("/api/v1/answer-history/create/", data=payload)

        self.assertEqual(response.status_code, output.get("status"))

        if output.get("error"):
            self.assertEqual(response.data.get("error"), output.get("error"))

        if output.get("non_field_errors"):
            self.assertEqual(response.data.get("non_field_errors")
                             [0], output.get("non_field_errors"))

        self.assertEqual(response.data.get("is_submission"), output.get("is_submission"))


class StudentAnswerHistoryListViewTestCases(ApiV1Test):
    """
    Student Answer History list view tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
            user_id='d41d8cd98f00b204e9800998ecf8zzzz',
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.answer_history = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label1"
        )
        self.update_session_lti_data()

    def test_answer_history_list_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(
            f"/api/v1/student-answer-history/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_answer_history_list_success(self):
        """
        Test typical successful call.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get(
            f"/api/v1/student-answer-history/list/{self.drawing_tool.uuid}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get("results")), 1)

        # First and single in this case
        first_answer_entry_results = response.data.get("results")[0]

        self.assertTrue("id" in first_answer_entry_results)
        self.assertTrue("label" in first_answer_entry_results)
        self.assertTrue("submit_date" in first_answer_entry_results)
        self.assertTrue("answer" in first_answer_entry_results)
        self.assertTrue("student_result" in first_answer_entry_results)
        self.assertTrue("student_user" in first_answer_entry_results.get("student_result"))

        self.assertEqual(first_answer_entry_results.get("label"), self.answer_history.label)
        self.assertEqual(first_answer_entry_results.get("answer"), self.answer_history.answer)

    def test_answer_history_list_success_empty_list(self):
        """
        Test a successful call when no corresponding entries are found.
        """
        self.client.force_authenticate(user=self.user)
        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id2',
        )

        response = self.client.get(f"/api/v1/student-answer-history/list/{drawing_tool.uuid}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("results"), [])

    def test_answer_history_list_pagination(self):
        """
        Test pagination.
        """
        self.client.force_authenticate(user=self.user)
        for i in range(8):
            AnswerHistoryFactory.create(
                student_result=self.student_result,
                label=f"test_label_{i}"
            )

        response_page_1 = self.client.get(
            f"/api/v1/student-answer-history/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response_page_1.status_code, status.HTTP_200_OK)
        self.assertEqual(response_page_1.data.get("count"), 9)
        self.assertEqual(len(response_page_1.data.get("results")), 5)

        response_page_2 = self.client.get(
            f"/api/v1/student-answer-history/list/{self.drawing_tool.uuid}/?page=2")
        self.assertEqual(response_page_2.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_page_2.data.get("results")), 4)


class AnswerHistoryByStudentResultListViewTestCases(ApiV1Test):
    """
    Answer History By Student Result list view tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
            user_id='d41d8cd98f00b204e9800998ecf8kkkk',
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.answer_history = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label1"
        )

    def test_answer_history_list_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(
            f"/api/v1/student-answer-history/list/{self.student_result.pk}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_answer_history_list_success(self):
        """
        Test typical successful call.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get(
            f"/api/v1/student-answer-history/list/{self.student_result.pk}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get("results")), 1)

        # First and single in this case
        first_answer_entry_results = response.data.get("results")[0]

        self.assertTrue("id" in first_answer_entry_results)
        self.assertTrue("label" in first_answer_entry_results)
        self.assertTrue("submit_date" in first_answer_entry_results)
        self.assertTrue("answer" in first_answer_entry_results)
        self.assertTrue("student_result" in first_answer_entry_results)
        self.assertTrue("student_user" in first_answer_entry_results.get("student_result"))

        self.assertEqual(first_answer_entry_results.get("label"), self.answer_history.label)
        self.assertEqual(first_answer_entry_results.get("answer"), self.answer_history.answer)

    def test_answer_history_list_success_empty_list(self):
        """
        Test a successful call when no corresponding entries are found.
        """
        self.client.force_authenticate(user=self.user)
        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id2',
        )
        student_result = StudentResultFactory(
            tool=drawing_tool,
            lti_user=self.lti_user,
        )
        response = self.client.get(f"/api/v1/student-answer-history/list/{student_result.pk}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("results"), [])

    def test_answer_history_list_pagination(self):
        """
        Test pagination.
        """
        self.client.force_authenticate(user=self.user)
        for i in range(8):
            AnswerHistoryFactory.create(
                student_result=self.student_result,
                label=f"test_label_{i}"
            )

        response_page_1 = self.client.get(
            f"/api/v1/student-answer-history/list/{self.student_result.pk}/")
        self.assertEqual(response_page_1.status_code, status.HTTP_200_OK)
        self.assertEqual(response_page_1.data.get("count"), 9)
        self.assertEqual(len(response_page_1.data.get("results")), 5)

        response_page_2 = self.client.get(
            f"/api/v1/student-answer-history/list/{self.student_result.pk}/?page=2")
        self.assertEqual(response_page_2.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_page_2.data.get("results")), 4)


class InstructorAnswerHistoryListViewTestCases(ApiV1Test):
    """
    Instructor Answer History list view tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.instructor_user = User.objects.create_user(
            'test_instructor', 'test_instructor@test.com', 'test_instructor')
        self.instructor_lti_user = LTIUserFactory(
            django_user=self.instructor_user,
            lti_consumer=self.lti_consumer,
            user_id="f41d8cd98f00b204e9800998ecf8427e",
        )
        self.update_session_lti_data(user_id=self.instructor_lti_user.user_id)
        self.answer_history = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label1",
            instructor_user=self.instructor_lti_user,
        )

    def test_answer_history_list_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(
            f"/api/v1/instructor-answer-history/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_answer_history_list_success(self):
        """
        Test typical successful call.

        Ensure:

        #. 200 OK status (once authenticated);
        #. grouped by student id;
        #. all required fields are present in the response, with expected values;
        """
        self.client.force_authenticate(user=self.instructor_user)
        self.set_instructor_role()

        response = self.client.get(
            f"/api/v1/instructor-answer-history/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        # Ensure grouped by student id
        response_student_ids = [entry[0] for entry in response.data]
        self.assertIn(self.student_result.lti_user.user_id, response_student_ids)

        # First and single in this case
        first_answer_entry = response.data[0][1][0]

        self.assertTrue("id" in first_answer_entry)
        self.assertTrue("label" in first_answer_entry)
        self.assertTrue("submit_date" in first_answer_entry)
        self.assertTrue("answer" in first_answer_entry)
        self.assertTrue("instructor_user" in first_answer_entry)
        self.assertFalse("student_result" in first_answer_entry)

        # Ensure instructor user data is provided
        self.assertIn("user_id", first_answer_entry.get("instructor_user"))
        self.assertFalse(first_answer_entry.get("is_submission"))

        self.assertEqual(first_answer_entry.get("label"), self.answer_history.label)
        self.assertEqual(first_answer_entry.get("answer"), self.answer_history.answer)

    def test_answer_history_list_success_empty_list(self):
        """
        Test a successful call when no corresponding entries are found.
        """
        self.client.force_authenticate(user=self.instructor_user)
        self.set_instructor_role()

        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id2',
        )

        response = self.client.get(f"/api/v1/instructor-answer-history/list/{drawing_tool.uuid}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [])

    def test_answer_history_list_success_relevant_results(self):
        """
        Ensure that only particular tool's student results are fetched.
        """
        self.client.force_authenticate(user=self.instructor_user)
        self.set_instructor_role()

        # Set up multiple tools' student results
        another_user = User.objects.create_user('test3', 'test3@test.com', 'test3')
        another_lti_user = LTIUserFactory.create(
            django_user=another_user,
            lti_consumer=self.lti_consumer,
            user_id='z41d8cd98f00b204e9800998ecf8427e',
        )
        another_drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id3',
            max_attempts=1000,
        )
        another_student_result = StudentResultFactory(
            tool=another_drawing_tool,
            lti_user=another_lti_user,
        )
        response = self.client.get(
            f"/api/v1/instructor-answer-history/list/{self.drawing_tool.uuid}/")
        response_student_id = response.data[0][0]

        self.assertEqual(len(response.data), 1)
        # Ensure grouped by student id and relevant student id is present in the response.
        self.assertEqual(self.student_result.lti_user.user_id, response_student_id)
        # Only particular tool's student results are fetched.
        self.assertNotEqual(another_student_result.lti_user.user_id, response_student_id)

    def test_answer_history_list_success_multiple_answers(self):
        """
        Test successful call with multiple answer history entries.

        Ensure that multiple answer history entries per student id (if any)
        are provided in the response.
        """
        self.client.force_authenticate(user=self.instructor_user)
        self.set_instructor_role()

        # Set up multiple answer history entries
        another_answer_history = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label2",
            instructor_user=self.instructor_lti_user,
        )
        another_answer_history_2 = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label3",
            instructor_user=self.instructor_lti_user,
        )
        response = self.client.get(
            f"/api/v1/instructor-answer-history/list/{self.drawing_tool.uuid}/")

        self.assertEqual(len(response.data), 1)

        first_answer_entry = response.data[0][1][0]
        second_answer_entry = response.data[0][1][1]
        third_answer_entry = response.data[0][1][2]
        self.assertEqual(first_answer_entry.get("answer"), self.answer_history.answer)
        self.assertEqual(second_answer_entry.get("answer"), another_answer_history.answer)
        self.assertEqual(third_answer_entry.get("answer"), another_answer_history_2.answer)

        # Ensure grouped by student id
        response_student_ids = [entry[0] for entry in response.data]
        self.assertIn(self.student_result.lti_user.user_id, response_student_ids)

    def test_answer_history_list_success_multiple_student_results(self):
        """
        Test successful call with multiple student result entries.

        Use case prerequisite: an instructor has submitted their versions
        for multiple students (same tool).
        """
        self.client.force_authenticate(user=self.instructor_user)
        self.set_instructor_role()

        # Set up student results for a tool
        another_user = User.objects.create_user('test3', 'test3@test.com', 'test3')
        another_lti_user = LTIUserFactory.create(
            django_user=another_user,
            lti_consumer=self.lti_consumer,
            user_id='z41d8cd98f00b204e9800998ecf8427e',
        )
        another_student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=another_lti_user,
        )
        AnswerHistoryFactory.create(
            student_result=another_student_result,
            label="test_label2",
            instructor_user=self.instructor_lti_user,
        )
        response = self.client.get(
            f"/api/v1/instructor-answer-history/list/{self.drawing_tool.uuid}/")
        self.assertEqual(len(response.data), 2)

        response_student_ids = [entry[0] for entry in response.data]
        self.assertIn(self.student_result.lti_user.user_id, response_student_ids)
        self.assertIn(another_student_result.lti_user.user_id, response_student_ids)

    def test_answer_history_list_not_found(self):
        """
        Test not-found case (related LTI user not found).
        """
        instructor_user = User.objects.create_user(
            'test_instructor2', 'test_instructor2@test.com', 'test_instructor2')
        self.update_session_lti_data(user_id='ababagalamaga')
        self.client.force_authenticate(user=instructor_user)
        self.set_instructor_role()

        response = self.client.get(
            f"/api/v1/instructor-answer-history/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_answer_history_list_no_instructor_session(self):
        """
        Test invalid case when no instructor role is present in a session.

        Example of output in `response.data`
        ::

            ipdb> response.data
            [ErrorDetail(string='Instructor role is required in the current session.', code='invalid')]

        """
        self.client.force_authenticate(user=self.instructor_user)

        response = self.client.get(
            f"/api/v1/instructor-answer-history/list/{self.drawing_tool.uuid}/")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data[0], "Instructor role is required in the current session.")
        # Fix serializer validation to apply the next output format
        # self.assertEqual(response.data.get("error"), "Instructor role is required in the current session.")


@ddt
class StudentResultListViewTestCases(ApiV1Test):
    """
    Student Result by Drawing Tool list view tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
            user_id='d41d8cd98f00b204e9800998ecf8kkkk',
        )
        self.update_session_lti_data()
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )

    def test_student_result_list_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(f"/api/v1/student-result/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_student_result_list_by_instructor_success(self):
        """
        Test a typical successful call made by an instructor.

        If instructor, all student_results for the current tool are provided.
        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        response = self.client.get(f"/api/v1/student-result/list/{self.drawing_tool.uuid}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get("results")), 1)

        first_entry_results = response.data.get("results")[0]

        self.assertTrue("id" in first_entry_results)
        # self.assertTrue("grade" in first_entry_results)
        self.assertTrue("student_user" in first_entry_results)
        self.assertEqual(first_entry_results.get("id"), self.student_result.id)
        # Custom serializer fields
        self.assertTrue("tool" in first_entry_results)
        self.assertTrue("last_version" in first_entry_results)
        self.assertTrue("submissions_count" in first_entry_results)

    def test_student_result_list_by_student_success(self):
        """
        Test a typical successful call by a student.

        If student, student result for a currently authenticated
        user is provided (list with a single el).
        """
        self.client.force_authenticate(user=self.user)

        user = User.objects.create_user(f"user_test", f"user_test@example.com", "pass123")
        lti_user = LTIUserFactory.create(
            django_user=user,
            lti_consumer=self.lti_consumer,
            user_id=f'wwwd8cd98f00b204tt800998ecf8kkkr',
        )
        StudentResultFactory.create(
            tool=self.drawing_tool,
            lti_user=lti_user,
        )

        response = self.client.get(f"/api/v1/student-result/list/{self.drawing_tool.uuid}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get("results")), 1)

    @data(
        (
            "1. No submissions, one saved version.",
            {
                "submissions_number": 0,
                "saved_versions_number": 1,
            },
            {
                "submissions_count": 0,
            },
        ),
        (
            "2. One submission, one saved version.",
            {
                "submissions_number": 1,
                "saved_versions_number": 1,
            },
            {
                "submissions_count": 1,
            },
        ),
        (
            "3. No submissions, no saved versions, i.e. no answer history at all.",
            {
                "submissions_number": 0,
                "saved_versions_number": 0,
            },
            {
                "submissions_count": 0,
            },
        ),
    )
    @unpack
    def test_student_result_list_custom_fields(self, description, input_instructions, output):
        """
        Test response nomenclature: response contains all expected fields.

        A typical successful call is made by an instructor.

        Cases:

        #. No submissions, one saved version.
        #. One submission, one saved version.
        #. No submissions, no saved versions, i.e. no answer history at all.
        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()
        latest_answer_history = None

        for i in range(input_instructions["submissions_number"]):
            AnswerHistoryFactory.create(
                student_result=self.student_result,
                label=f"test_label_submission_{i}",
                is_submission=True,
            )

        for i in range(input_instructions["saved_versions_number"]):
            latest_answer_history = AnswerHistoryFactory.create(
                student_result=self.student_result,
                label=f"test_label_saved_version_{i}",
            )

        response = self.client.get(f"/api/v1/student-result/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        first_entry_results = response.data.get("results")[0]

        # Check serializer customization
        self.assertEqual(first_entry_results.get("tool", {}).get(
            "max_attempts"), self.drawing_tool.max_attempts)
        self.assertEqual(first_entry_results.get("submissions_count"), output["submissions_count"])
        if latest_answer_history:
            self.assertEqual(first_entry_results.get("last_version", {}).get(
                "answer"), latest_answer_history.answer)
            self.assertEqual(
                first_entry_results.get("last_version", {}).get("is_submission"),
                latest_answer_history.is_submission
            )
        else:
            self.assertIsNone(first_entry_results.get("last_version"))

    def test_student_result_list_success_empty_list(self):
        """
        Test a successful call when no corresponding entries are found.
        """
        self.client.force_authenticate(user=self.user)

        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id3',
        )
        response = self.client.get(f"/api/v1/student-result/list/{drawing_tool.uuid}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("results"), [])

    def test_student_result_list_pagination(self):
        """
        Test pagination (with a current user being an instructor).
        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        for i in range(8):
            user = User.objects.create_user(f"user_{i}", f"user_{i}@example.com", "pass123")
            lti_user = LTIUserFactory.create(
                django_user=user,
                lti_consumer=self.lti_consumer,
                user_id=f'wwwd8cd98f00b204tt800998ecf8kkk{i}',
            )
            StudentResultFactory.create(
                tool=self.drawing_tool,
                lti_user=lti_user,
            )

        response_page_1 = self.client.get(f"/api/v1/student-result/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response_page_1.status_code, status.HTTP_200_OK)
        self.assertEqual(response_page_1.data.get("count"), 9)
        self.assertEqual(len(response_page_1.data.get("results")), 5)

        response_page_2 = self.client.get(
            f"/api/v1/student-result/list/{self.drawing_tool.uuid}/?page=2")
        self.assertEqual(response_page_2.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_page_2.data.get("results")), 4)

    def test_student_result_list_404_tool(self):
        """
        Test 404 Not-Found (tool).

        Non-existing drawing tool's uuid is provided in the URL.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get(
            f"/api/v1/student-result/list/n0nexist-ing7-4f09-8b27-4936bb9f28e5/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_student_result_list_404_lti_user(self):
        """
        Test 404 Not-Found (LTI user).

        Currently authenticated user, i.e. LTI user id indicated in a session,
        doesn't have a corresponding LTI user entry.
        """
        user = User.objects.create_user(f"some_user", f"some_user@example.com", "pass123")
        self.update_session_lti_data(user_id="ababagalamaha")
        self.client.force_authenticate(user=user)

        response = self.client.get(f"/api/v1/student-result/list/{self.drawing_tool.uuid}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


@ddt
class CommentCreateViewTestCases(ApiV1Test):
    """
    Comment create view tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
            user_id='s31d8cd98f00b204e9800998ecf8427e',
        )
        self.update_session_lti_data()
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.answer_history = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label1",
        )

    def test_comment_creation_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.post(
            "/api/v1/comment/create/",
            data={
                "student_result_id": self.student_result.id,
                "comment": "Like the drawing!",
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @data(
        (
            "1. Comment creation w/ answer history.",
            True,
        ),
        (
            "2. Comment creation w/o answer history (it's optional).",
            False
        ),
    )
    @unpack
    def test_comment_creation_success(self, description, is_answer_history_provided):
        """
        Test typical successful calls.

        Check 201 status code, response nomenclature and data.

        Ensure an author user is the currently authenticated user (session).
        Answer history (if associated): only its id (pk) should be in the response.

        Cases:

        #. Comment creation w/ answer history.
        #. Comment creation w/o answer history (it's optional).
        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        payload = {
            "comment": "Like the drawing!",
        }
        if is_answer_history_provided:
            payload["answer_history_id"] = self.answer_history.id
        else:
            payload["student_result_id"] = self.student_result.id

        response = self.client.post("/api/v1/comment/create/", data=payload)
        response_keys = list(response.data.keys())
        response_comment = Comment.objects.first()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.all().count(), 1)

        # Check response fields nomenclature
        self.assertTrue("lti_user" in response_keys)
        self.assertTrue("student_result" in response_keys)
        self.assertTrue("answer_history" in response_keys)
        self.assertTrue("comment" in response_keys)
        self.assertTrue("submit_date" in response_keys)

        # Check related models data
        self.assertTrue(response.data["lti_user"]["user_id"], self.lti_user.id)
        self.assertTrue(response.data["student_result"]
                        ["student_user"]["user_id"], self.lti_user.id)
        self.assertEqual(response.data["comment"], response_comment.comment)

        if is_answer_history_provided:
            self.assertEqual(
                response.data.get("answer_history"),
                {
                    "id": self.answer_history.id,
                    "label": self.answer_history.label,
                }
            )
        else:
            self.assertIsNone(response.data.get("answer_history"))

        # Ensure an author user is the currently authenticated user (session)
        self.assertEqual(response_comment.lti_user.django_user, self.user)

    @data(
        (
            "1. A user saves a comment for a non-existing lti user -> 404, not-found error.",
            {
                "is_lti_init": False,
                "exists_student_result": True,
                "create_student_result": False,
                "comment": "Like the drawing!",
                "is_instructor": True,
            },
            {
                "status": status.HTTP_404_NOT_FOUND,
                "error": "No LTIUser matches the given query.",
            },
        ),
        (
            "2. A user saves a comment for a non-existing student result -> 404, not-found.",
            {
                "is_lti_init": True,
                "exists_student_result": False,
                "create_student_result": False,
                "comment": "Like the drawing!",
                "is_instructor": True,
            },
            {
                "status": status.HTTP_404_NOT_FOUND,
                "error": "No related student result object was found.",
            },
        ),
        (
            "3. A user didn't provide a comment text -> 400, bad request.",
            {
                "is_lti_init": True,
                "exists_student_result": True,
                "create_student_result": False,
                "comment": "",
                "is_instructor": True,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
            },
        ),
        (
            "4. A student is not eligible to comment on other students' results. -> 400, bad request.",  # FIXME
            {
                "is_lti_init": True,
                "exists_student_result": True,
                "create_student_result": True,
                "comment": "It's OK.",
                "is_instructor": False,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
                "non_field_errors": "A student is not eligible to comment on other students' results.",
            },
        ),
    )
    @unpack
    def test_comment_creation_errors(self, description, input_instructions, output):
        """
        Test errors of comment creation.

        Cases:

        #. A user saves a comment for a non-existing lti user -> 404, not-found .
        #. A user saves a comment for a non-existing student result -> 404, not-found.
        #. A user didn't provide a comment text -> 400, bad request.
        #. A student is not eligible to comment on other students' results. -> 400, bad request.

        Response in case of validation failure 400 Bad request
        ::

            ipdb> response.data
            {'non_field_errors': [ErrorDetail(string="A student is not eligible to comment on other students' results.", code='invalid')]}

        Response in other cases of 400 Bad request
        ::

            ipdb> response.data
            {'comment': [ErrorDetail(string='This field may not be blank.', code='blank')]}

        """
        user = self.user

        if input_instructions["is_lti_init"]:
            self.client.force_authenticate(user=self.user)
        else:
            # User w/o corresponding lti user (the one who never passed lti init flow).
            self.update_session_lti_data(user_id='ababahalamaha')
            user = User.objects.create_user('test2', 'test2@test.com', 'test2')
            self.client.force_authenticate(user=user)

        if input_instructions["is_instructor"]:
            self.set_instructor_role()

        student_result_id = self.student_result.id if input_instructions["exists_student_result"] else 111

        if input_instructions["create_student_result"]:
            another_user = User.objects.create_user(
                'test_student2', 'test_student2@test.com', 'test_student2'
            )
            lti_user = LTIUserFactory(django_user=another_user, user_id="newuserid")
            student_result = StudentResultFactory(tool=self.drawing_tool, lti_user=lti_user)
            student_result_id = student_result.id

        payload = {
            "student_result_id": student_result_id,
            "comment": input_instructions["comment"],
        }

        response = self.client.post("/api/v1/comment/create/", data=payload)

        self.assertEqual(response.status_code, output.get("status"))

        if output.get("non_field_errors"):
            self.assertEqual(response.data.get("non_field_errors")
                             [0], output.get("non_field_errors"))

        if output.get("error"):
            self.assertEqual(response.data.get("error"), output.get("error"))

    @data(
        (
            "1. Answer history (in request): a comment for a non-existing answer history -> 404, not-found.",
            {
                "exists_answer_history": False,
                "comment": "Like the drawing!",
            },
            {
                "status": status.HTTP_404_NOT_FOUND,
            },
        ),
        (
            "2. Answer history: a comment for a non-existing answer history -> 404, not-found.",
            {
                "exists_answer_history": True,
                "comment": "Like the drawing!",
            },
            {
                "status": status.HTTP_201_CREATED,
            },
        ),
    )
    @unpack
    def test_comment_creation_answer_history(self, description, input_instructions, output):
        """
        Test comment creation basing on answer history from the request.

        It's either `answer_history_id` or `student_result_id` in the request prams.

        Cases:

        #. Answer history (in request): a comment for a non-existing answer history -> 404, not-found.
        #. Answer history: a comment for a non-existing answer history -> 404, not-found.

        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        answer_history_id = self.answer_history.id if input_instructions["exists_answer_history"] else 111

        payload = {
            "answer_history_id": answer_history_id,
            "comment": input_instructions["comment"],
        }

        response = self.client.post("/api/v1/comment/create/", data=payload)

        self.assertEqual(response.status_code, output.get("status"))

        if output.get("error"):
            self.assertEqual(response.data.get("error"), output.get("error"))

    @data(
        (
            "1. Neither answer history nor student result (either should be provided) -> 400, bad request.",
            {
                "provided_student_result_id": False,  # with the request params
                "provided_answer_history_id": False,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
                "error": "Either 'student_result_id' or 'answer_history_id' must be provided."
            },
        ),
    )
    @unpack
    def test_comment_creation_request_params(self, description, input_instructions, output):
        """
        Test request params.

        It's either `answer_history_id` or `student_result_id` in the request prams.

        Cases (more cases might arrive):

        #. Neither answer history nor student result (either should be provided) -> 400, bad request.

        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        payload = {
            "comment": "Fine by me",
        }
        if input_instructions["provided_student_result_id"]:
            payload["student_result_id"] = self.student_result.id
        if input_instructions["provided_answer_history_id"]:
            payload["answer_history_id"] = self.answer_history.id

        response = self.client.post("/api/v1/comment/create/", data=payload)

        self.assertEqual(response.status_code, output.get("status"))

        if output.get("error"):
            self.assertEqual(response.data.get("error"), output.get("error"))


class CommentByStudentResultListViewTestCases(ApiV1Test):
    """
    Comment list view (per student result) tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
            user_id='bbbd8cd98f00b204e9800998ecf8aaaa',
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.answer_history = AnswerHistoryFactory.create(
            student_result=self.student_result,
            label="test_label1",
        )
        self.comment = CommentFactory.create(
            student_result=self.student_result,
            lti_user=self.lti_user,
            answer_history=self.answer_history,
        )

    def test_comment_list_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(f"/api/v1/comment/list/{self.student_result.pk}/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_comment_list_success(self):
        """
        Test typical successful call.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get(f"/api/v1/comment/list/{self.student_result.pk}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        # First and single in this case
        first_answer_entry_results = response.data[0]

        # Check response fields nomenclature
        self.assertTrue("lti_user" in first_answer_entry_results)
        self.assertTrue("student_result" in first_answer_entry_results)
        self.assertTrue("answer_history" in first_answer_entry_results)
        self.assertTrue("comment" in first_answer_entry_results)
        self.assertTrue("submit_date" in first_answer_entry_results)

        self.assertEqual(
            first_answer_entry_results.get("answer_history"),
            {
                "id": self.answer_history.id,
                "label": self.answer_history.label,
            }
        )

        # Check related models data
        self.assertTrue(first_answer_entry_results["lti_user"]["user_id"], self.lti_user.id)
        self.assertTrue(first_answer_entry_results["student_result"]
                        ["student_user"]["user_id"], self.lti_user.id)

    def test_comment_list_success_empty_list(self):
        """
        Test a successful call when no corresponding entries are found.
        """
        self.client.force_authenticate(user=self.user)

        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id2',
        )
        student_result = StudentResultFactory(
            tool=drawing_tool,
            lti_user=self.lti_user,
        )
        response = self.client.get(f"/api/v1/comment/list/{student_result.pk}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [])

    def test_comment_list_success_relevant_results(self):
        """
        Test successful call: relevant results only.

        Only particular student result's comments are provided
        (when there are more student results in the system).
        """
        self.client.force_authenticate(user=self.user)

        drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id2',
        )
        StudentResultFactory(
            tool=drawing_tool,
            lti_user=self.lti_user,
        )

        response = self.client.get(f"/api/v1/comment/list/{self.student_result.pk}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)


@ddt
class SkillGradeCreateViewTestCases(ApiV1Test):
    """
    SkillGrade create view tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
        )
        self.update_session_lti_data()
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        self.skill = SkillFactory(
            drawing_tool=self.drawing_tool,
        )

    def test_skill_grade_creation_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.post(
            "/api/v1/grade/create/",
            data={
                "student_result_id": self.student_result.id,
                "skill_id": self.skill.id,
                "grade": 100,
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_skill_grade_creation_success(self):
        """
        Test typical successful call.

        Check 201 status code, response nomenclature and data.

        Ensure an user has an instructor role.
        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        payload = {
            "student_result": self.student_result.id,
            "skill": self.skill.id,
            "grade": 99,
        }

        response = self.client.post("/api/v1/grade/create/", data=payload)

        response_keys = list(response.data.keys())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Skill.objects.all().count(), 1)
        created_skill = Skill.objects.last()

        self.assertTrue("skill" in response_keys)
        self.assertTrue("student_result" in response_keys)
        self.assertTrue("grade" in response_keys)

        self.assertEqual(response.data.get("skill"), created_skill.id)

    @data(
        (
            "1. A user grades for a non-existing student result -> 404, not-found.",
            {
                "exists_student_result": False,
                "grade": 100,
                "is_instructor": True,
            },
            {
                "status": status.HTTP_404_NOT_FOUND,
                "error": "No StudentResult matches the given query.",
            },
        ),
        (
            "2. A user didn't provide a grade -> 400, bad request.",
            {
                "exists_student_result": True,
                "grade": None,
                "is_instructor": True,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
            },
        ),
        (
            "3. A student is not eligible to grade. -> 400, bad request.",
            {
                "exists_student_result": True,
                "grade": 100,
                "is_instructor": False,
            },
            {
                "status": status.HTTP_400_BAD_REQUEST,
                "non_field_errors": "A student is not eligible to grade.",
            },
        ),
    )
    @unpack
    def test_skill_grade_creation_errors(self, description, input_instructions, output):
        """
        Test errors of skill grade creation.

        Parametrized cases:

        #. A user grades for a non-existing student result -> 404, not-found.
        #. A user didn't provide a grade -> 400, bad request.
        #. A student is not eligible to grade. -> 400, bad request.

        Response in case of validation failure 400 Bad request
        ::

            ipdb> response.data
            {'non_field_errors': [ErrorDetail(string="A student is not eligible to grade.", code='invalid')]}

        Response in other cases of 400 Bad request
        ::

            ipdb> response.data
            {'skill': [ErrorDetail(string='This field may not be blank.', code='blank')]}

        """
        self.client.force_authenticate(user=self.user)

        if input_instructions["is_instructor"]:
            self.set_instructor_role()

        student_result_id = self.student_result.id if input_instructions["exists_student_result"] \
            else 111

        payload = {
            "student_result": student_result_id,
            "skill": self.skill.id,
        }
        if input_instructions["grade"]:
            payload["grade"] = input_instructions["grade"]

        response = self.client.post("/api/v1/grade/create/", data=payload)

        self.assertEqual(response.status_code, output.get("status"))

        if output.get("non_field_errors"):
            self.assertEqual(response.data.get("non_field_errors")
                             [0], output.get("non_field_errors"))

        if output.get("error"):
            self.assertEqual(response.data.get("error"), output.get("error"))


@ddt
class SkillListViewTestCases(ApiV1Test):
    """
    Skill by Drawing Tool list view tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
        )
        self.update_session_lti_data()
        self.skill = SkillFactory(
            drawing_tool=self.drawing_tool,
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )

    def test_skill_list_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get(f"/api/v1/skill/list/{self.student_result.id}/", folow=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @data(
        (
            "1. An instructor requests a list of not graded skills -> 200, OK.",
            None
        ),
        (
            "2. An instructor requests a list of skills that are already graded -> 200, OK.",
            99
        ),
    )
    @unpack
    def test_skill_list_success(self, description, grade):
        """
        Test a typical successful call made by an instructor.
        """
        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        if grade:
            SkillGradeFactory.create(
                student_result=self.student_result,
                skill=self.skill,
                grade=grade,
            )

        response = self.client.get(f"/api/v1/skill/list/{self.student_result.id}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        first_entry = response.data[0]

        self.assertTrue("id" in first_entry)
        self.assertTrue("skill" in first_entry)
        self.assertTrue("weight" in first_entry)
        self.assertTrue("grade" in first_entry)

        self.assertEqual(first_entry.get("id"), self.skill.id)
        self.assertEqual(first_entry.get("skill"), self.skill.skill)
        self.assertEqual(first_entry.get("weight"), self.skill.weight)
        self.assertEqual(first_entry.get("grade"), grade)

    def test_skill_list_with_grade_success(self):
        """
        Test a grade was calculated for a skill.
        """

        SkillGradeFactory.create(
            student_result=self.student_result,
            skill=self.skill
        )

        self.client.force_authenticate(user=self.user)
        self.set_instructor_role()

        response = self.client.get(f"/api/v1/skill/list/{self.student_result.id}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        first_entry = response.data[0]

        self.assertTrue("id" in first_entry)
        self.assertTrue("skill" in first_entry)
        self.assertTrue("weight" in first_entry)
        self.assertTrue("grade" in first_entry)

        self.assertEqual(first_entry.get("id"), self.skill.id)
        self.assertEqual(first_entry.get("skill"), self.skill.skill)
        self.assertEqual(first_entry.get("weight"), self.skill.weight)

    def test_skill_list_success_empty_list(self):
        """
        Test a successful call when no corresponding entries are found.
        """
        self.client.force_authenticate(user=self.user)

        self.skill.delete()

        student_result = StudentResultFactory.create(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )
        response = self.client.get(f"/api/v1/skill/list/{student_result.id}/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [])

    def test_skill_list_404_student_result(self):
        """
        Test 404 Not-Found (tool).

        Non-existing drawing tool's uuid is provided in the URL.
        """
        self.client.force_authenticate(user=self.user)

        response = self.client.get(
            "/api/v1/skill/list/1000/", folow=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class SkillTitleListViewTestCases(ApiV1Test):
    """
    Skill list view (per drawing tool) tests.
    """

    def test_skill_list_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get("/api/v1/skill/list/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_skill_list_success(self):
        """
        Test typical successful call.

        Ensure only distinct skills are present in the response.
        Check response fields nomenclature.
        """
        self.client.force_authenticate(user=self.user)
        # Create skills with the same title
        skill = SkillFactory(drawing_tool=self.drawing_tool)
        SkillFactory(drawing_tool=self.drawing_tool)

        response = self.client.get("/api/v1/skill/list/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Ensure only distinct skills are present in the response
        self.assertEqual(len(response.data), 1)
        # Check response fields nomenclature
        self.assertEqual(response.data, [skill.skill])

    def test_skill_list_success_empty_list(self):
        """
        Test a successful call when no corresponding entries are found.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get("/api/v1/skill/list/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [])


class CurrentLTIUserDetailViewTestCases(ApiV1Test):
    """
    Current LTI User Detail View tests.
    """

    def setUp(self):
        super().setUp()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer,
            user_id='d41d8cd98f00b204e9800998ecf8zzzz',
        )
        self.update_session_lti_data()

    def test_lti_user_detail_view_anon(self):
        """
        Test anonymous call.
        """
        response = self.client.get("/api/v1/current-lti-user/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_lti_user_detail_view_authenticated_success(self):
        """
        Test typical successful call.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get("/api/v1/current-lti-user/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("user_id"), self.lti_user.user_id)

    def test_lti_user_detail_view_authenticated_no_lti_user(self):
        """
        Test a case when no related LTI User object exists.
        """
        user = User.objects.create_user(
            'test_instructor', 'test_instructor@test.com', 'test_instructor'
        )
        self.client.force_authenticate(user=user)
        self.update_session_lti_data(user_id='ababahalamaha')
        response = self.client.get("/api/v1/current-lti-user/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
