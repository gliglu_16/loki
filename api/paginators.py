"""
Paginators for API.
"""

from rest_framework.pagination import PageNumberPagination


class SmallResultsSetPageNumberPagination(PageNumberPagination):
    """
    Custom paginator for small results display.

    Supports page numbers as query parameters.
    """

    page_size = 5
