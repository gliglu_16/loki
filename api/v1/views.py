"""API v1 views."""
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext as _

from rest_framework import generics
from rest_framework import views
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.exceptions import ApiCustomException
from api.mixins import HandleAPIExceptionMixin, ValidateInstructorRole
from api.paginators import SmallResultsSetPageNumberPagination
from api.serializers import (AnswerHistorySerializer, CommentSerializer,
                             DrawingToolSerializer,
                             GroupedAnswerHistorySerializer,
                             LTIUserSerializer,
                             SkillGradeSerializer, SkillSerializer,
                             SkillTitleSerializer, StudentResultSerializer)
from comments.models import Comment
from drawing_tool.models import (AnswerHistory, DrawingTool, Skill, SkillGrade,
                                 StudentResult)
from lti.models import LTIUser


class DrawingToolDetailView(HandleAPIExceptionMixin, generics.RetrieveAPIView):
    """
    Drawing tool details API.

    URL: `/api/v1/drawingtool/{tool_uuid}`
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = DrawingTool.objects.all()
    serializer_class = DrawingToolSerializer


class AnswerHistoryDetailView(HandleAPIExceptionMixin, generics.RetrieveAPIView):
    """
    Answer history details API.

    URL: `/api/v1/answer-details/{answer_history_pk}`
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = AnswerHistory.objects.all()
    serializer_class = AnswerHistorySerializer


class AnswerHistoryCreateView(HandleAPIExceptionMixin, generics.CreateAPIView):
    r"""
    Answers history creation endpoint.

    URL: `/api/v1/answer-history/create/`

    Example of request params
    ::

        {
            "tool_uuid": "0bcd02ec-583d-4f09-8b27-4936bb9f28e5",
            "answer": "<svg xmlns='http://www.w3.org/2000/svg' width='200' \
                height='100' version='1.1'><rect width='200' height='100' stroke='black' \
                stroke-width='6' fill='green'/></svg>",
            "label": "test_label"
        }

    Example of response (status code 201)
    ::

        {
            "id": 1,
            "label": "test_label",
            "submit_date": "2019-12-15T15:21:38.932936Z",
            "answer": "<svg xmlns='http://www.w3.org/2000/svg' width='200' \
                height='100' version='1.1'><rect width='200' height='100' stroke='black' \
                stroke-width='6' fill='green'/></svg>"
        }
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = AnswerHistory.objects.all()
    serializer_class = AnswerHistorySerializer

    def perform_create(self, serializer):
        """
        Override `perform_create()` logic.

        Check for a `StudentResult` obj existence provided
        request params.

        NOTE: continue moving validation logic out to serializer.
        """
        student_user_id = self.request.data.get("student_user_id")
        if student_user_id:
            # An instructor is creating an entry (saving an image version) for a specified student.
            # NOTE consider moving this validation to the serializer
            student_lti_user = LTIUser.get_lti_user_or_404(
                user_id=student_user_id,
                lti_consumer_id=self.request.session.get("LTI_CONSUMER_ID"),
            )
            instructor_lti_user = LTIUser.get_lti_user_or_404(
                user_id=self.request.session.get("USER_ID"),
                lti_consumer_id=self.request.session.get("LTI_CONSUMER_ID"),
            )
            if instructor_lti_user.id == student_lti_user.id:
                raise ApiCustomException(
                    _("An instructor shouldn't be saving a version for themselves."))
            serializer.validated_data["instructor_user_id"] = instructor_lti_user.id
        else:
            # Regular student submission
            student_user_id = self.request.session.get("USER_ID")
        serializer.validated_data["student_result_id"] = StudentResult.get_student_result_by_user_tool_or_404(
            user_id=student_user_id,
            tool_uuid=self.request.data.get("tool_uuid")
        ).id
        super().perform_create(serializer)

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        context = super().get_serializer_context()

        student_user_id = self.request.data.get("student_user_id")

        context["tool_uuid"] = self.request.data.get("tool_uuid")
        context["student_user_id"] = self.request.data.get("student_user_id")
        context["roles"] = tuple(self.request.session.get("ROLES", []))

        if not student_user_id:
            context["current_student_user_id"] = self.request.session.get("USER_ID")

        return context


class StudentAnswerHistoryListView(HandleAPIExceptionMixin, generics.ListAPIView):
    """
    Student Answer History list API.

    Fetch answer history list per drawing tool and current user.

    URL: `/api/v1/student-answer-history/list/{tool_uuid}`

    Example of response (status code 200)
    ::

        [
            {
                "id": 1,
                "label": "test_label1",
                "submit_date": "2019-12-11T10:19:07.591860Z",
                "answer": "valid xml",
                "is_submission": True,
                "instructor_user": None,
            },
            {
                "id": 2,
                "label": "test_label2",
                "submit_date": "2019-12-11T10:28:44.831660Z",
                "answer": "valid xml",
                "is_submission": True,
                "instructor_user": None,
            }
        ]
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = AnswerHistorySerializer
    pagination_class = SmallResultsSetPageNumberPagination

    def get_queryset(self):
        """
        Fetch a list of all `AnswerHistory` models by student result.

        Based on tool uuid provided in the request.
        """
        last_entry = 'last-entry' in self.request.query_params
        try:
            student_result = StudentResult.get_student_result_by_user_tool_or_404(
                user_id=self.request.session.get("USER_ID"),
                tool_uuid=self.kwargs.get("pk"),
            )
        except Http404:
            return AnswerHistory.objects.none()
        queryset = AnswerHistory.objects.filter(student_result=student_result)
        if last_entry and queryset:
            answer = queryset.latest('submit_date')
            queryset = AnswerHistory.objects.filter(student_result=student_result, pk=answer.pk)
        return queryset


class AnswerHistoryByStudentResultListView(HandleAPIExceptionMixin, generics.ListAPIView):
    """
    Answer History By Student Result list API.

    Fetch answer history list per student result.

    URL: `/api/v1/student-answer-history/list/{student_result_pk}/`

    Example of response (status code 200)
    ::

        [
            {
                "id": 1,
                "label": "test_label1",
                "submit_date": "2019-12-11T10:19:07.591860Z",
                "answer": "valid xml"
            },
            {
                "id": 2,
                "label": "test_label2",
                "submit_date": "2019-12-11T10:28:44.831660Z",
                "answer": "valid xml"
            }
        ]
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = AnswerHistorySerializer
    pagination_class = SmallResultsSetPageNumberPagination

    def get_queryset(self):
        """
        Fetch a list of all `AnswerHistory` models by student result.

        Based on the StudentResult pk provided in the request.
        """
        student_result = get_object_or_404(StudentResult, pk=self.kwargs.get("pk"))
        return AnswerHistory.objects.filter(student_result=student_result)


class InstructorAnswerHistoryListView(HandleAPIExceptionMixin, generics.ListAPIView):
    """
    Instructor Answer History list API.

    Fetch answer history list per drawing tool and current user
    (current user as instructor). Results are grouped by student result
    (student user id as a key).

    URL: `/api/v1/instructor-answer-history/list/{tool_uuid}`

    Example of response (status code 200)
    ::

        [
            [
                "d41d8cd98f00b204e9800998ecf8427e", [
                    {
                        "id": 1,
                        "label": "test_label1",
                        "submit_date": "2019-12-11T10:19:07.591860Z",
                        "answer": "valid xml",
                        "is_submission": False,
                        "instructor_user": {
                            "user_id": "z41d8cd98f00b204e9800998ecf8427e",
                            "user_username": "staff",
                            "user_fullname": "Staff Staffovich"
                        }
                    },
                    {
                        "id": 1,
                        "label": "test_label2",
                        "submit_date": "2019-12-11T10:28:44.831660Z",
                        "answer": "valid xml"
                        "is_submission": False,
                        "instructor_user": {
                            "user_id": "z41d8cd98f00b204e9800998ecf8427e",
                            "user_username": "test_staff",
                            "user_fullname": "Staff Staffovich"
                        }
                    }
                ]
            ]
        ]
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = GroupedAnswerHistorySerializer

    def get_queryset(self):
        """
        Fetch a list of `AnswerHistory` models by student result for an instructor.

        Based on tool uuid provided in the request.
        """
        instructor_user = LTIUser.get_lti_user_or_404(
            user_id=self.request.session.get("USER_ID"),
            lti_consumer_id=self.request.session.get("LTI_CONSUMER_ID"),
        )
        queryset = AnswerHistory.objects.filter(
            instructor_user=instructor_user,
            student_result__tool_id=self.kwargs.get("pk"),
        )
        return queryset

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        context = super().get_serializer_context()
        context["roles"] = tuple(self.request.session.get("ROLES", []))
        return context


class StudentResultListView(HandleAPIExceptionMixin, generics.ListAPIView, ValidateInstructorRole):
    """
    Student Result by Drawing Tool list API.

    Scenarios:

    #. If instructor, all student_results for the current tool are provided.
    #. If student, student result for a currently authenticated user is provided.

    URL: `/api/v1/student-result/list/{tool_uuid}/`

    Example of response (status code 200)
    ::

        [
            {
                "id": 1,
                "student_user": {
                    "user_id": "aaad8cd98f00b204e9800998ecf84bbb",
                    "user_username": "test_username",
                    "user_fullname": "Student Student"
                },
                "last_version": {
                    "answer": "valid XML",
                    "is_submission": True
                },
                "tool": {
                    "max_attempts": 100
                },
                "submissions_count": 33
            }
        ]
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = StudentResultSerializer
    pagination_class = SmallResultsSetPageNumberPagination

    def get_queryset(self):
        """
        Fetch a list of `StudentResult` models by drawing tool.

        Based on the `DrawingTool` uuid provided in the request.
        """
        tool = get_object_or_404(DrawingTool, uuid=self.kwargs.get("pk"))
        if self._is_instructor():
            return StudentResult.objects.filter(
                tool=tool,
            )
        return StudentResult.objects.filter(
            tool=tool,
            lti_user=LTIUser.get_lti_user_or_404(
                tool=tool,
                user_id=self.request.session.get("USER_ID"),
            )
        )


class CommentCreateView(HandleAPIExceptionMixin, generics.CreateAPIView):
    """
    Comment creation endpoint.

    URL: `/api/v1/comment/create/`

    Example of request params
    ::

        {
            "student_result_id": 1,
            "version_id": 1,
            "comment": "Like the drawing!"
        }

    Example of response (status code 201)
    ::

        {
            "submit_date": "2020-01-09T15:21:38.932936Z",
            "comment": "Like the drawing!",
            "lti_user": {
                "user_id": "z41d8cd98f00b204e9800998ecf8427e",
                "user_username": "test_username",
                "user_fullname": "Peter Student"
            },
            "student_result": {
                "student_user": {
                    "user_id": "a41d8cd98f00b204e9800998ecf84rty",
                    "user_username": "test_username",
                    "user_fullname": "Student Student"
                }
            },
            "answer_history": {
                "id": 1
            }
        }
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def post(self, request, *args, **kwargs):
        self._validate_request_params()
        return super().post(request, *args, **kwargs)

    def perform_create(self, serializer):
        """
        Override `perform_create()` logic.
        """
        student_result_id = self.request.data.get("student_result_id")
        answer_history_id = self.request.data.get("answer_history_id")
        lti_user = LTIUser.get_lti_user_or_404(
            user_id=self.request.session.get("USER_ID"),
            lti_consumer_id=self.request.session.get("LTI_CONSUMER_ID"),
        )
        serializer.validated_data["lti_user_id"] = lti_user.id

        if answer_history_id:
            answer_history = AnswerHistory.get_answer_history(pk=int(answer_history_id))
            serializer.validated_data["answer_history_id"] = answer_history.id
            serializer.validated_data["student_result_id"] = answer_history.student_result.id
        else:
            student_result = StudentResult.get_student_result_by_id_or_404(
                pk=int(student_result_id))
            serializer.validated_data["student_result_id"] = student_result.id

        super().perform_create(serializer)

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        context = super().get_serializer_context()

        student_result_id = self.request.data.get("student_result_id")
        answer_history_id = self.request.data.get("answer_history_id")

        # NOTE: figure out a way to get rid of duplicate getters calls
        lti_user = LTIUser.get_lti_user_or_404(
            user_id=self.request.session.get("USER_ID"),
            lti_consumer_id=self.request.session.get("LTI_CONSUMER_ID"),
        )
        context["current_user_id"] = lti_user.id

        if answer_history_id:
            answer_history = AnswerHistory.get_answer_history(pk=int(answer_history_id))
            context["student_result_user_id"] = answer_history.student_result.lti_user.id

        elif student_result_id:
            student_result = StudentResult.get_student_result_by_id_or_404(
                pk=int(student_result_id))
            context["student_result_user_id"] = student_result.lti_user.id

        else:
            # Request params get validated; either `answer_history_id`
            # or `student_result_id` is required.
            pass

        context["roles"] = tuple(self.request.session.get("ROLES", []))

        return context

    def _validate_request_params(self):
        """
        Validate request params.

        It's either `answer_history_id` or `student_result_id`.
        """
        student_result_id = self.request.data.get("student_result_id")
        answer_history_id = self.request.data.get("answer_history_id")

        if not (student_result_id or answer_history_id):
            raise ApiCustomException(_(
                "Either 'student_result_id' or 'answer_history_id' must be provided."
            ))


class CommentByStudentResultListView(HandleAPIExceptionMixin, generics.ListAPIView):
    """
    Comment By Student Result list API.

    Fetch comments list per student result.

    URL: `/api/v1/comment/list/{student_result_pk}/`

    Example of response (status code 200)
    ::

        [
            {
                "submit_date": "2020-01-09T15:21:38.932936Z",
                "comment": "Like the drawing!",
                "lti_user": {
                    "user_id": "z41d8cd98f00b204e9800998ecf8427e",
                    "user_username": "test_username",
                    "user_fullname": "Peter User"
                },
                "student_result": {
                    "student_user": {
                        "user_id": "a41d8cd98f00b204e9800998ecf84rty",
                        "user_username": "student_username",
                        "user_fullname": "Peter Student"
                    }
                },
                "answer_history": {
                    "id": 1
                }
            }
        ]
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CommentSerializer

    def get_queryset(self):
        """
        Fetch a list of all `Comment` models by student result.

        Based on the StudentResult pk provided in the request.
        """
        student_result = get_object_or_404(StudentResult, pk=self.kwargs.get("pk"))
        return Comment.objects.filter(student_result=student_result)


class SkillListView(HandleAPIExceptionMixin, generics.ListAPIView):
    """
    Skill list by student result API.

    URL: `/api/v1/skill/list/{student_result_id}/`

    Example of response (status code 200)
    ::

        [
            {
                "id": "2",
                "skill": "Skill 1",
                "weight": 1,
                "grade": 100,
            },
            {
                "id": "4",
                "skill": "Skill 2",
                "weight": 2,
                "grade": 85,
            }
        ]
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = SkillSerializer

    def get_queryset(self):
        """
        Fetch a list of `Skill` objects by drawing tool.

        Based on the `StudentResult` pk provided in the request.
        """
        student_result = StudentResult.get_student_result_by_id_or_404(
            pk=int(self.kwargs.get("pk")))
        return Skill.objects.filter(drawing_tool=student_result.tool)

    def get_serializer_context(self):
        """
        Context provided to the serializer to calculate grade based on the `StudentResult` pk.
        """
        context = super().get_serializer_context()

        student_result = StudentResult.get_student_result_by_id_or_404(
            pk=int(self.kwargs.get("pk")))
        skills = Skill.objects.filter(drawing_tool=student_result.tool)
        context['skill_grades'] = SkillGrade.objects.filter(
            skill__in=skills,
            student_result=student_result
        )
        return context


class SkillGradeCreateView(HandleAPIExceptionMixin, generics.CreateAPIView):
    """
    Comment creation endpoint.

    URL: `/api/v1/grade/create/`

    Example of request params
    ::

        {
            "skill": 1,
            "student_result": 1,
            "grade": 100
        }

    Example of response (status code 201)
    ::

        {
            "skill": 1,
            "student_result": 1,
            "grade": 100
        }
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = SkillGrade.objects.all()
    serializer_class = SkillGradeSerializer

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        context = super().get_serializer_context()

        student_result_id = self.request.data.get("student_result")
        skill_id = self.request.data.get("skill")

        get_object_or_404(Skill, pk=int(skill_id))
        context["skill_id"] = skill_id
        get_object_or_404(StudentResult, pk=int(student_result_id))
        context["student_result_id"] = student_result_id

        context["roles"] = tuple(self.request.session.get("ROLES", []))

        return context


class SkillTitleListView(generics.ListAPIView):
    """
    Get all skills, for all drawing tools.

    ::

        [
            "Javascript",
            "CSS",
            "HTML",
            "Python",
            "Java"
        ]

    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = SkillTitleSerializer
    queryset = Skill.objects.distinct("skill")


class CurrentLTIUserDetailView(views.APIView):
    """
    Current LTI user details API.

    URL: `/api/v1/current-lti-user/`
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Fetch an LTI user object.
        """
        lti_user = LTIUser.get_lti_user_or_404(
            user_id=self.request.session.get("USER_ID"),
            lti_consumer_id=self.request.session.get("LTI_CONSUMER_ID"),
        )
        data = LTIUserSerializer(lti_user).data
        return Response(data)
