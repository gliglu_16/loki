"""
API v1 paths.
"""
from django.urls import path

from .views import (
    AnswerHistoryByStudentResultListView,
    AnswerHistoryCreateView,
    AnswerHistoryDetailView,
    CommentByStudentResultListView,
    CommentCreateView,
    CurrentLTIUserDetailView,
    DrawingToolDetailView,
    InstructorAnswerHistoryListView,
    StudentAnswerHistoryListView,
    StudentResultListView,
    SkillListView,
    SkillGradeCreateView,
    SkillTitleListView,
)


urlpatterns = [
    path('drawingtool/<uuid:pk>/',
         DrawingToolDetailView.as_view(),
         name='drawing-tool-details'),
    path('answer-details/<int:pk>/',
         AnswerHistoryDetailView.as_view(),
         name='answer-history-details'),
    path('answer-history/create/',
         AnswerHistoryCreateView.as_view(),
         name='create-answer-history'),
    path('student-answer-history/list/<uuid:pk>/',
         StudentAnswerHistoryListView.as_view(),
         name='list-student-answer-history'),
    path('student-answer-history/list/<int:pk>/',
         AnswerHistoryByStudentResultListView.as_view(),
         name='list-student-result-answer'),
    path('instructor-answer-history/list/<uuid:pk>/',
         InstructorAnswerHistoryListView.as_view(),
         name='list-instructor-answer-history'),
    path('student-result/list/<uuid:pk>/',
         StudentResultListView.as_view(),
         name='list-student-result'),
    path('comment/create/', CommentCreateView.as_view(),
         name='create-comment'),
    path('comment/list/<int:pk>/',
         CommentByStudentResultListView.as_view(),
         name='list-student-result-comment'),
    path('skill/list/<int:pk>/', SkillListView.as_view(), name='list-skill'),
    path('skill/list/', SkillTitleListView.as_view(), name='list-skill-title'),
    path('grade/create/', SkillGradeCreateView.as_view(), name='grade-skill'),
    path('current-lti-user/', CurrentLTIUserDetailView.as_view(), name='current-user'),
]
