"""Collection of useful mixins."""

from django.http import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from api.exceptions import ApiCustomException
from loki.constants import INSTRUCTOR


class HandleAPIExceptionMixin(APIView):
    """Mixin to override handle_exception method in rest_framework CBS views."""

    def handle_exception(self, exc):
        """Handle custom exceptions."""
        if isinstance(exc, Http404):
            return Response({'error': str(exc)},
                            status=status.HTTP_404_NOT_FOUND)
        if isinstance(exc, ApiCustomException):
            return Response({"error": exc.message},
                            status=status.HTTP_400_BAD_REQUEST)
        return super().handle_exception(exc)


class ValidateInstructorRole:
    """Mixin to provide is_instructor check."""

    def _is_instructor(self):
        """
        Check if the authenticated user holds the instructor role.
        """
        if hasattr(self, "request"):
            return INSTRUCTOR in self.request.session.get("ROLES", [])
        if hasattr(self, "context"):
            return INSTRUCTOR in self.context.get("roles", [])
        return False
