"""
Custom API exceptions.
"""


class ApiCustomException(Exception):
    """
    Class for edX API exceptions.

    Subclasses (if any) should provide
    a `.default_msg` property.
    """

    default_msg = "An API exception occurred."

    def __init__(self, message=None):
        """
        Initialization of exceptions class object.
        """
        self.message = message or self.default_msg
        super().__init__(message)

    def __str__(self):
        """
        Override string representation of exceptions class object.
        """
        return self.message
