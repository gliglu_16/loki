Frontend Views
======================

Introduction
--------------------

Tests designed specifically for Frontend views.


Integration Tests
--------------------


Frontend Views Test
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: frontend.tests.test_integration.FrontendViewsTest
      :members:
