Drawing Tool
======================

Introduction
--------------------

Tests specifically for Drawing Tool app.


Unit Tests
--------------------


Drawing Tool Model
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: drawing_tool.tests.test_models.DrawingToolTestCase
      :members:

Instructor Form
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: drawing_tool.tests.test_forms.InstructorFormTestCase
      :members:


Background Task to Send Grades to edX platform
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: drawing_tool.tests.test_tasks.GradingTaskTest
      :members:
