API
======================

Introduction
--------------------

REST API endpoints expose various LTI provider resources.


API V1 Docs
--------------------

API V1 endpoint unit and integration tests are described below.

Note: session auth is used.


GET Drawing Tool Details
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Endpoint
**********************

  .. autoclass:: api.v1.views.DrawingToolDetailView

Integration Tests
**********************

  .. autoclass:: api.tests.v1.test_integration.DrawingToolDetailViewTestCases
      :members:

POST Answer History Creation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Endpoint
**********************

  .. autoclass:: api.v1.views.AnswerHistoryCreateView

Integration Tests
**********************

  .. autoclass:: api.tests.v1.test_integration.AnswerHistoryCreateViewTestCases
      :members:


GET Answer History Details
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Endpoint
**********************

  .. autoclass:: api.v1.views.AnswerHistoryDetailView

Integration Tests
**********************

  .. autoclass:: api.tests.v1.test_integration.AnswerHistoryDetailViewTestCases
      :members:


GET Answer History List
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Endpoints
**********************

  .. autoclass:: api.v1.views.StudentAnswerHistoryListView
  .. autoclass:: api.v1.views.AnswerHistoryByStudentResultListView
  .. autoclass:: api.v1.views.InstructorAnswerHistoryListView

Integration Tests
**********************

  .. autoclass:: api.tests.v1.test_integration.StudentAnswerHistoryListViewTestCases
      :members:

  .. autoclass:: api.tests.v1.test_integration.AnswerHistoryByStudentResultListViewTestCases
       :members:

  .. autoclass:: api.tests.v1.test_integration.InstructorAnswerHistoryListViewTestCases
       :members:

GET Skill List
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Endpoints
**********************

  .. autoclass:: api.v1.views.SkillListView
  .. autoclass:: api.v1.views.SkillTitleListView

Integration Tests
**********************

  .. autoclass:: api.tests.v1.test_integration.SkillListViewTestCases
      :members:

  .. autoclass:: api.tests.v1.test_integration.SkillTitleListViewTestCases
      :members:


GET Current LTI User
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Endpoints
**********************

  .. autoclass:: api.v1.views.CurrentLTIUserDetailView

Integration Tests
**********************

  .. autoclass:: api.tests.v1.test_integration.CurrentLTIUserDetailViewTestCases
      :members:


Unit Tests
--------------------

API related unit tests go below.

  .. automodule:: api.tests.test_utils
      :members:
