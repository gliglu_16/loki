LTI
======================

Introduction
--------------------

Tests for different LTI requests.

Integration Tests
--------------------

Integration tests are described below.

Methods
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: lti.tests.test_integration.LTIInitMethodsTest
      :members:

Drawing Tool
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: lti.tests.test_integration.LTIInitDrawingToolDirectTest
      :members:

Params, Exception, Model
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: lti.tests.test_integration.LTIInitParamsTest
      :members:

  .. autoclass:: lti.tests.test_integration.LTIInitExceptionTest
      :members:

  .. autoclass:: lti.tests.test_integration.LTIModelTest
      :members:

Acceptance
~~~~~~~~~~~~~~~~~~~~~~~~~~

  .. autoclass:: lti.tests.test_integration.LTIInitAcceptanceTests
      :members:


Unit Tests
--------------------

  .. automodule:: lti.tests.test_utils
      :members:
