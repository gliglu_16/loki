.. Loki documentation master file, created by
   sphinx-quickstart on Mon Dec 16 16:58:43 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Loki's documentation!
================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   api
   drawing_tool
   lti
   frontend


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
