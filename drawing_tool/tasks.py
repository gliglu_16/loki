"""
Celery tasks.
"""
import logging

from drawing_tool.grading import send_score_update
from drawing_tool.models import StudentResult
from loki.celery import app

log = logging.getLogger(__name__)


@app.task()
def send_outcomes():
    """
    Send students results grades to edX platform.
    """

    students_results_qs = StudentResult.objects.exclude(
        grade_sending_status=True,
    )

    for stud_res in students_results_qs.iterator():
        score = stud_res.edx_grade
        if score is not None:
            if stud_res.lis_outcome_service_url:
                log.debug(f"Sending the grade {score} to edX for '{stud_res}'.")
                response = send_score_update(
                    assignment=stud_res,
                    score=score,
                    lti_user=stud_res.lti_user,
                )
                if response:
                    stud_res.grade_sending_status = response.ok
                    stud_res.save()
                else:
                    log.debug(
                        "Didn't get the response from the campus system - "
                        "won't mark the student result as processed."
                    )
            else:
                log.debug(
                    f"Won't send the grade {score} to edX as the respective component "
                    f"doesn't seem to be marked for grading in LMS; "
                    f"student result: {stud_res}"
                )
        else:
            log.debug(
                f"The score of '{stud_res}' isn't ready to be pushed to edX."
            )
