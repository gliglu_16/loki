/**
 * Custom business logic for the instructor form.
 */

$(function() {

    // Hide skills_formset errors e.g. "This field is required." (it's all captured by non_form_errors).
    $(".errorlist").hide();

});
