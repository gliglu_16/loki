from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import UpdateView

from drawing_tool.forms import InstructorForm, SkillFormSet
from drawing_tool.models import DrawingTool
from lti.utils import only_lti


decorators = [
    only_lti,
    login_required,
    csrf_exempt,
    xframe_options_exempt,
]


@method_decorator(decorators, name='dispatch')
class InstructorUpdateView(SuccessMessageMixin, UpdateView):
    """
    Render drawing tool update form for instructor role.
    """
    form_class = InstructorForm
    model = DrawingTool
    template_name = 'drawing_tool/instructor_form.html'
    success_message = 'Set Up form has been submitted successfully.'

    def get_context_data(self, **kwargs):
        """
        Return an instance of the form to be used in this view.

        Provide skills forms with the context.
        """
        context = super().get_context_data(**kwargs)
        context["skills_formset"] = self._get_skills_formset()
        return context

    def get_form_kwargs(self):
        kwargs = super(InstructorUpdateView, self).get_form_kwargs()
        kwargs["skills_formset"] = self._get_skills_formset()
        return kwargs

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.

        Handle skills data.
        """
        context = self.get_context_data()
        skills_formset = context["skills_formset"]
        if skills_formset.is_valid():
            skills_formset.instance = self.object
            skills_formset.save()
        return super().form_valid(form)

    def _get_skills_formset(self):
        """
        Get a skills formset.
        """
        return SkillFormSet(
            self.request.POST,
            instance=self.object,
        ) if self.request.POST else SkillFormSet(instance=self.object)
