from django.contrib import admin

from drawing_tool.models import (AnswerHistory, DrawingTool,
                                 DrawingToolLTIData, DrawingToolLTIEditHistory,
                                 Skill, SkillGrade, StudentResult)


@admin.register(DrawingTool)
class DrawingToolAdmin(admin.ModelAdmin):
    list_display = (
        'uuid', 'display_name', 'question', 'hints', 'answer', 'show_answer',
        'enable_grading', 'resource_link_id', 'lti_consumer',
    )


@admin.register(StudentResult)
class StudentResultAdmin(admin.ModelAdmin):
    list_display = (
        'tool', 'lti_user', 'grade_sending_status',
    )


@admin.register(AnswerHistory)
class AnswerHistoryAdmin(admin.ModelAdmin):
    list_display = (
        'label', 'student_result', 'submit_date', 'answer', 'is_submission'
    )


@admin.register(DrawingToolLTIData)
class DrawingToolLTIDataAdmin(admin.ModelAdmin):
    list_display = (
        'tool', 'lti_user', 'instructor_role', 'extra_data',
    )


@admin.register(DrawingToolLTIEditHistory)
class DrawingToolLTIEditHistoryAdmin(admin.ModelAdmin):
    list_display = (
        'tool', 'lti_user', 'timestamp', 'diff',
    )


@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    list_display = (
        'drawing_tool', 'skill', 'weight',
    )


@admin.register(SkillGrade)
class SkillGradeAdmin(admin.ModelAdmin):
    list_display = (
        'student_result', 'skill', 'grade',
    )
