from django import forms
from django.forms import BaseInlineFormSet, inlineformset_factory
from django.forms.formsets import TOTAL_FORM_COUNT
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Layout
from drawing_tool.models import DrawingTool, Skill


class InstructorForm(forms.ModelForm):
    """
    ModelForm for Drawing tool model.
    """

    def __init__(self, *args, **kwargs):
        """
        Add skill formset and initialize crispy_forms helper.
        """
        self.skills_formset = kwargs.pop("skills_formset", ())
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout.insert(0, Layout(HTML("""
         {% load fullurl %}
            <div class="direct-link row">
                <label class="control-label col-lg-2">Drawing Tool URL</label>
                <div class="col-lg-8 controls">
                    <a href="{% fullurl 'lti:lti_init' object.uuid %}">
                        {% fullurl 'lti:lti_init' object.uuid %}
                    </a>
                    <span id="helpBlock" class="help-block">
                        You can use this link to insert Tool in other units or courses.
                    </span>
                </div>
            </div>
        """)))
        self.helper.layout.insert(7, Layout(HTML("""
            <div class="form-group skills-formset"
                {% if not object.enable_grading %}
                style="display: none"
                {% endif %}
            >
                <div class="controls col-lg-offset-2 col-lg-8">
                    {% for skills_form in skills_formset %}
                        <div class="skill-forms-container">
                            {{ skills_form }}
                        </div>
                    {% endfor %}
                    {{ skills_formset.management_form }}
                </div>
            </div>
        """)))
        self.helper.layout.extend(Layout(
            HTML(
                """
                <div class="form-group">
                    <div class="controls col-lg-2"></div>
                    <div class="controls col-lg-8">
                      <div class="submit">
                        {% if messages %}
                            {% for message in messages %}
                            <span{% if message.tags %} class="messages {{ message.tags }}"{% endif %}>{{ message }}</span>
                            {% endfor %}
                        {% endif %}   
                        <input type="submit" name="submit" value="Submit" class="btn btn-primary"> 
                      </div>
                    </div>                
                </div>
                """
            ),
        ))
        self.helper.form_id = 'id-instructor-form'
        self.helper.form_method = 'post'

    class Meta:
        model = DrawingTool
        fields = [
            'display_name', 'question', 'question_image', 'background_image', 'hints',
            'answer', 'show_answer', 'enable_grading', 'enable_comments', 'max_attempts',
        ]
        widgets = {
            'hints': forms.Textarea(attrs={'cols': 40, 'rows': 5}),
            'enable_grading': forms.CheckboxInput(
                attrs={'onchange': 'toggleGradingSkillsDisplay()'}),
        }
        help_texts = {
            'question_image': _(
                'Image that will be displayed below the Instructions text. '
                'Recommended extensions are .jpeg, .png, and .gif. '
                'The best aspect ratio is 16:9.'
            ),
            'enable_grading': _(
                'Once checked, remember to enable grading in your LMS as well.'
            ),
            'enable_comments': _(
                'Check to enable the Comments section. Uncheck to disable the Comments section.'
            ),
            'background_image': _(
                'Image that will be set to the Drawing Tool background.'
            ),
        }

    def clean(self):
        """
        Run custom validation.

        Validate skills formset: at least one skill should
        remain indicated in the formset.
        """
        if self.cleaned_data.get("enable_grading"):
            skill_indicated = self._check_remaining_skill()
            if not skill_indicated:
                raise forms.ValidationError(
                    _("With grading enabled, at least one skill and its weight should be filled.")
                )
        return super().clean()

    def clean_display_name(self):
        """
        Validate default value for the field display_name was changed.
        """
        data = self.cleaned_data.get('display_name')
        if data == 'Change display name...':
            raise forms.ValidationError(_("This is a dummy display name, please change it."))
        return data

    def clean_question(self):
        """
        Validate default value for the field question was changed.
        """
        data = self.cleaned_data.get('question')
        if data == 'Enter instructions here...':
            raise forms.ValidationError(_("This is a dummy question, please change it."))
        return data

    def _check_remaining_skill(self):
        """
        Ensure at least one skills remains defined.

        Exclude fields marked for deletion from the analysis.
        """
        for form in self.skills_formset:
            try:
                if not form["DELETE"].value() and form["skill"].value() and form["weight"].value():
                    return True
            except KeyError:
                raise forms.ValidationError(_("Skills data have to be defined. "))

        return False


class SkillInlineFormSet(BaseInlineFormSet):
    """
    Skills formset.
    """

    def total_form_count(self):
        """
        Return the total number of forms in this FormSet.

        Show extra fields only when no initial data set.

        Ref.: https://stackoverflow.com/a/7401075
        """
        if self.data or self.files:
            return self.management_form.cleaned_data[TOTAL_FORM_COUNT]
        if self.initial_form_count() > 0:
            total_forms = self.initial_form_count()
        else:
            total_forms = self.initial_form_count() + self.extra
        if total_forms > self.max_num > 0:
            total_forms = self.max_num
        return total_forms


class SkillForm(forms.ModelForm):
    """
    ModelForm for the `Skill` model.
    """

    class Meta:
        model = Skill
        fields = [
            'skill',
            'weight',
        ]
        widgets = {
            'weight': forms.NumberInput(attrs={'style': 'width:5ch'}),
        }


# `extra` should be 1, otherwise dynamic formsets flow will break (ref.: `jquery.formset.js`).
SkillFormSet = inlineformset_factory(
    parent_model=DrawingTool,
    model=Skill,
    fk_name='drawing_tool',
    extra=1,
    can_delete=True,
    form=SkillForm,
    formset=SkillInlineFormSet,
)
