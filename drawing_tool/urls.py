from django.urls import path

from drawing_tool.views import InstructorUpdateView

app_name = 'drawing_tool'

urlpatterns = [
    path('edit/<uuid:pk>/', InstructorUpdateView.as_view(), name='edit'),
]
