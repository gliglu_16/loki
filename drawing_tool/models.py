from django.apps import apps
from django.contrib.postgres.fields import JSONField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import gettext as _

from loki.models import CommonTool


def answer_directory_path(instance, filename):
    """Define path to store answer images."""
    return f'answers/{instance.pk}/{filename}'


class DrawingTool(CommonTool):
    """
    Drawing tool settings for one particular resource.
    """
    answer = models.FileField(upload_to=answer_directory_path, blank=True)
    show_answer = models.BooleanField(verbose_name='Show Answer', default=False)
    enable_comments = models.BooleanField(verbose_name='Enable Comments', default=True)

    def __str__(self):
        return self.resource_link_id[:32]

    def get_absolute_url(self):
        return reverse('drawing-tool:edit', args=[self.uuid])

    def save(self, *args, **kwargs):
        if self.show_answer and not self.answer:
            self.show_answer = False
        super().save(*args, **kwargs)

    @staticmethod
    def validate_attempts(tool_uuid, user_id):
        """
        Validate submissions attempts.
        """
        tool = DrawingTool.objects.filter(uuid=tool_uuid).first()
        LTIUser = apps.get_model('lti', 'LTIUser')
        lti_user = LTIUser.get_lti_user_or_404(user_id=user_id, tool=tool)
        if tool and tool.max_attempts and lti_user:
            student_result = StudentResult.objects.filter(
                lti_user=lti_user,
                tool=tool,
            ).first()
            if student_result:
                submissions_count = AnswerHistory.objects.filter(
                    student_result=student_result,
                    is_submission=True,
                ).count()
                if submissions_count >= tool.max_attempts:
                    return False
        return True


class StudentResult(models.Model):
    """
    User result related to one tool.

    Notes:
        grade_sending_status: True if successfully sent to edX,
            False otherwise.
    """
    tool = models.ForeignKey('DrawingTool', on_delete=models.CASCADE)
    lti_user = models.ForeignKey('lti.LTIUser', on_delete=models.CASCADE)
    grade_sending_status = models.BooleanField(default=False)
    lis_result_sourcedid = models.CharField(max_length=255, blank=True, null=True)
    lis_outcome_service_url = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        unique_together = (
            'tool',
            'lti_user',
        )

    def __str__(self):
        return f'{self.id}:{self.tool}:{self.lti_user.user_id}'

    @property
    def grade(self):
        """
        Calculate grade for a StudentResult based on the related skill grades.
        """
        grades = self.skillgrade_set
        if not grades.count() or grades.count() != self.tool.skill_set.count():
            return None

        grades_weights_list = grades.values_list('skill__weight', 'grade')
        weighted_grades = sum(map(lambda x: x[0] * x[1], grades_weights_list))
        sum_weights = sum(map(lambda x: x[0], grades_weights_list)) or 1

        grade = weighted_grades // sum_weights
        return grade

    @property
    def edx_grade(self):
        """
        Form a grade for a StudentResult suitable for Edx platform.
        """
        return self.grade / 100 if self.grade is not None else self.grade

    @property
    def submitted(self):
        """
        Check if StudentResult has at least one related submitted version.
        """
        return self.answerhistory_set.filter(is_submission=True).exists()

    @staticmethod
    def get_student_result_by_user_tool_or_404(user_id, tool_uuid):
        """
        Get a StudentResult with lti user id and tool uuid.
        """
        tool = get_object_or_404(DrawingTool, uuid=tool_uuid)
        LTIUser = apps.get_model('lti', 'LTIUser')
        lti_user = LTIUser.get_lti_user_or_404(user_id=user_id, tool=tool)
        student_result = get_object_or_404(StudentResult, lti_user=lti_user, tool=tool)
        return student_result

    @staticmethod
    def get_student_result_by_id_or_404(pk):
        """
        Fetch student result obj by pk.
        """
        obj = StudentResult.objects.filter(id=pk).first()
        if not obj:
            raise Http404(_("No related student result object was found."))
        return obj


class AnswerHistory(models.Model):
    """
    User answer related to student result.
    """
    label = models.CharField(max_length=255, blank=True)
    student_result = models.ForeignKey('StudentResult', on_delete=models.CASCADE)
    submit_date = models.DateTimeField(blank=True, auto_now_add=True, editable=False)
    answer = models.TextField(blank=False)
    instructor_user = models.ForeignKey(
        'lti.LTIUser',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    is_submission = models.BooleanField(verbose_name='Is Submitted', default=False)

    class Meta:
        verbose_name_plural = 'answers history'
        ordering = ['-submit_date']

    def __str__(self):
        return f'id:{self.id}-{self.label}'

    @staticmethod
    def get_answer_history(pk):
        """
        Fetch answer history obj by pk.
        """
        obj = AnswerHistory.objects.filter(id=pk).first()
        if not obj:
            raise Http404(_("No related answer history object was found."))
        return obj


class DrawingToolLTIData(models.Model):
    """
    LTI request data for user and particular tool.
    """
    tool = models.ForeignKey('DrawingTool', on_delete=models.CASCADE)
    lti_user = models.ForeignKey('lti.LTIUser', on_delete=models.CASCADE, related_name='tool_data')
    instructor_role = models.BooleanField(default=False)
    extra_data = JSONField(blank=True, null=True)

    class Meta:
        unique_together = (
            'tool',
            'lti_user',
            'instructor_role',
        )
        verbose_name_plural = 'drawing tool LTI data'

    def __str__(self):
        return f'{self.tool}:{self.lti_user.user_id}-instructor:{self.instructor_role}'


class DrawingToolLTIEditHistory(models.Model):
    """
    LTI change history by user.
    """
    tool = models.ForeignKey('DrawingTool', on_delete=models.CASCADE)
    lti_user = models.ForeignKey('lti.LTIUser', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(blank=True, auto_now_add=True, editable=False)
    diff = models.TextField(blank=True)

    class Meta:
        unique_together = (
            'tool',
            'lti_user',
        )
        verbose_name_plural = 'Drawing Tool LTI Edit History'


class Skill(models.Model):
    """
    Skill for a particular drawing tool.
    """
    skill = models.CharField(max_length=255)
    weight = models.PositiveSmallIntegerField(default=1)
    drawing_tool = models.ForeignKey('DrawingTool', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.skill}'


class SkillGrade(models.Model):
    """
    Grade for a student result by a particular skill.
    """
    grade = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(100), MinValueValidator(0)]
    )
    student_result = models.ForeignKey('StudentResult', on_delete=models.CASCADE)
    skill = models.ForeignKey('Skill', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.skill}:{self.grade}'

    class Meta:
        unique_together = (
            'student_result',
            'skill',
        )
        verbose_name_plural = "Skills' Grades"
