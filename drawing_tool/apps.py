from django.apps import AppConfig


class DrawingToolConfig(AppConfig):
    name = 'drawing_tool'
    verbose_name = 'Drawing Tool'
