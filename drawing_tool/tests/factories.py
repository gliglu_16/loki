import factory

from drawing_tool import models
from lti.tests.factories import LTIConsumerFactory, LTIUserFactory


class DrawingToolFactory(factory.DjangoModelFactory):
    """
    DrawingTool model factory for tests.
    """

    class Meta:
        model = models.DrawingTool

    lti_consumer = factory.SubFactory(LTIConsumerFactory)
    resource_link_id = 'test-context'
    display_name = 'Test Task'
    question = 'Test question'
    hints = '||Test hints||,||hint 22||'
    enable_grading = True
    max_attempts = 0
    show_answer = False
    answer = ''


class StudentResultFactory(factory.DjangoModelFactory):
    """
    StudentResult model factory for tests.
    """

    class Meta:
        model = models.StudentResult
        django_get_or_create = (
            "tool",
            "lti_user",
        )

    lti_user = factory.SubFactory(LTIUserFactory)
    tool = factory.SubFactory(DrawingToolFactory)


class AnswerHistoryFactory(factory.DjangoModelFactory):
    """
    AnswerHistory model factory for tests.
    """

    class Meta:
        model = models.AnswerHistory

    student_result = factory.SubFactory(StudentResultFactory)
    instructor_user = factory.SubFactory(LTIUserFactory)
    answer = "<svg xmlns='http://www.w3.org/2000/svg' width='200' height='100' version='1.1'>" \
             "<rect width='200' height='100' stroke='black' stroke-width='6' fill='green'/></svg>"


class SkillFactory(factory.DjangoModelFactory):
    """
    SkillFactory model factory for tests.
    """

    skill = "Skill 1"
    weight = 1
    drawing_tool = factory.SubFactory(DrawingToolFactory)

    class Meta:
        model = models.Skill
        django_get_or_create = (
            "drawing_tool",
        )


class SkillGradeFactory(factory.DjangoModelFactory):
    """
    SkillGradeFactory model factory for tests.
    """

    student_result = factory.SubFactory(StudentResultFactory)
    grade = 90
    skill = factory.SubFactory(SkillFactory)

    class Meta:
        model = models.SkillGrade
        django_get_or_create = (
            "skill",
            "student_result",
        )
