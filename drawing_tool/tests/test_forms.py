import shutil
import tempfile

import mock
from ddt import ddt, data, unpack
from django.core.files import File
from django.test import TestCase, override_settings

from drawing_tool.forms import InstructorForm, SkillFormSet
from drawing_tool.models import DrawingTool
from lti.tests.factories import LTIConsumerFactory

MEDIA_ROOT = tempfile.mkdtemp()


@ddt
@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class InstructorFormTestCase(TestCase):
    """
    Tests for an instructor Drawing Tool form.
    """

    def setUp(self):
        """
        Initialize all necessary variables.
        """
        self.lti_consumer = LTIConsumerFactory.create()

        self.file_mock = mock.MagicMock(spec=File)
        self.file_mock.name = 'test.file'

        self.test_data = {
            'display_name': 'Task',
            'question': 'What to do?',
            'hints': '||Hint 1.||',
            'answer': self.file_mock,
            'show_answer': True,
            'enable_grading': False,
        }

    @classmethod
    def tearDownClass(cls):
        """
        Remove test media folder after tests.
        """
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()

    def test_form_valid(self):
        """
        Test a valid form.
        """
        instructor_form = InstructorForm(self.test_data, instance=DrawingTool())
        self.assertTrue(instructor_form.is_valid())

    def test_required_fields(self):
        """
        Test required fields.
        """
        test_required = {
            'display_name': '',
            'question': '',
        }

        self.test_data.update(test_required)
        instructor_form = InstructorForm(self.test_data, instance=DrawingTool())
        self.assertFalse(instructor_form.is_valid())
        self.assertEqual(len(instructor_form.errors), 2)
        for field in instructor_form.errors.keys():
            self.assertEqual(instructor_form.errors.get(field, [])[0], 'This field is required.')

    def test_defaults_required_fields(self):
        """
        Test required fields defaults.
        """
        test_required = {
            'display_name': 'Change display name...',
            'question': 'Enter instructions here...',
        }
        errors = {
            'display_name': 'This is a dummy display name, please change it.',
            'question': 'This is a dummy question, please change it.'
        }
        self.test_data.update(test_required)
        instructor_form = InstructorForm(self.test_data, instance=DrawingTool())

        self.assertFalse(instructor_form.is_valid())
        self.assertEqual(len(instructor_form.errors), 2)
        for field in errors:
            self.assertIn(field, instructor_form.errors.keys())
            self.assertEqual(instructor_form.errors.get(field, [])[0], errors.get(field))

    @data(
        (
            "1. Grading is enabled; the form is VALID if one skill-weight bundle is provided.",
            True,
            {
                'skill_set-0-skill': 'Python',
                'skill_set-0-weight': '1',
                'skill_set-0-DELETE': '',
                'skill_set-0-id': '51',

                'skill_set-1-skill': 'Docker',
                'skill_set-1-weight': '1',
                'skill_set-1-DELETE': '',
                'skill_set-1-id': '52',
            },
            True,
        ),
        (
            "2. Grading is enabled; the form is INVALID if no skill-weight bundle is provided.",
            True,
            {},
            False,
        ),
        (
            "3. Grading is disabled; VALID if no skill-weight bundle is provided (no validation w/o grading)",
            False,
            {},
            True,
        ),
        (
            "4. Grading is enabled; VALID if a skill-weight bundle is left after deletion.",
            True,
            {
                'skill_set-0-skill': 'Python',
                'skill_set-0-weight': '1',
                'skill_set-0-DELETE': 'True',
                'skill_set-0-id': '51',

                'skill_set-1-skill': 'Docker',
                'skill_set-1-weight': '1',
                'skill_set-1-DELETE': '',
                'skill_set-1-id': '52',
            },
            True,
        ),
        (
            "5. Grading is enabled; INVALID if no skill-weight bundle is left after deletion.",
            True,
            {
                'skill_set-0-skill': 'Python',
                'skill_set-0-weight': '1',
                'skill_set-0-DELETE': 'True',
                'skill_set-0-id': '51',
            },
            False,
        ),
        (
            "6. Grading is disabled; VALID if no skill-weight bundle is left after deletion.",
            False,
            {
                'skill_set-0-skill': 'Python',
                'skill_set-0-weight': '1',
                'skill_set-0-DELETE': 'True',
                'skill_set-0-id': '51',
            },
            True,
        ),
        (
            "7. Grading is enabled; INVALID if skill title isn't provided.",
            True,
            {
                'skill_set-0-weight': '1',
                'skill_set-0-DELETE': 'True',
                'skill_set-0-id': '51',
            },
            False,
        ),
        (
            "8. Grading is enabled; INVALID if skill weight isn't provided.",
            True,
            {
                'skill_set-0-skill': 'Python',
                'skill_set-0-DELETE': 'True',
                'skill_set-0-id': '51',
            },
            False,
        ),
        (
            "9. Grading is enabled; VALID if a skill DELETE attribute isn't explicitly provided.",
            True,
            {
                'skill_set-0-skill': 'Python',
                'skill_set-0-weight': '1',
                'skill_set-0-id': '51',
            },
            True,
        ),
    )
    @unpack
    def test_skills_formset_validation(self, description, enable_grading, skills_data, is_valid):
        """
        Test skills configuration formset validation (regular form saving, record deletion).

        Cases:

        #. Grading is enabled; the form is VALID if one skill-weight bundle is provided.
        #. Grading is enabled; the form is INVALID if no skill-weight bundle is provided.
        #. Grading is disabled; the form is VALID if no skill-weight bundle is provided.
        #. Grading is enabled; VALID if no skill-weight bundle is left after deletion.
        #. Grading is enabled; INVALID if no skill-weight bundle is left after deletion.
        #. Grading is disabled; VALID if no skill-weight bundle is left after deletion (no validation w/o grading).
        #. Grading is enabled; INVALID if skill title isn't provided.
        #. Grading is enabled; INVALID if skill weight isn't provided.
        #. Grading is enabled; VALID if a skill DELETE attribute isn't explicitly provided.
        """
        skills_data.update({
            # Management form data
            'skill_set-TOTAL_FORMS': '2',
            'skill_set-INITIAL_FORMS': '0',
            'skill_set-MAX_NUM_FORMS': '',
        })
        skills_formset = SkillFormSet(skills_data, instance=DrawingTool())
        self.test_data["enable_grading"] = enable_grading
        instructor_form = InstructorForm(self.test_data, instance=DrawingTool(), skills_formset=skills_formset)
        self.assertEqual(instructor_form.is_valid(), is_valid)
