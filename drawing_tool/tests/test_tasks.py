"""
Test Celery tasks.
"""

from ddt import data, ddt, unpack
from django.contrib.auth.models import User
from django.test import TestCase
from mock import patch
import requests

from drawing_tool.models import Skill, SkillGrade, StudentResult
from drawing_tool.tasks import send_outcomes
from drawing_tool.tests.factories import (
    DrawingToolFactory,
    StudentResultFactory,
    SkillFactory,
    SkillGradeFactory,
)
from lti.tests.factories import LTIConsumerFactory, LTIUserFactory


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code
        self.ok = (self.status_code < 400)

    def json(self):
        return self.json_data


@ddt
@patch('drawing_tool.tasks.send_score_update')
class GradingTaskTest(TestCase):
    """
    Test the `drawing_tool.tasks.send_outcomes` task.
    """

    def setUp(self):
        self.user = User.objects.create_user('test', 'test@test.com', 'test')
        self.lti_consumer = LTIConsumerFactory.create()
        self.lti_user = LTIUserFactory(
            django_user=self.user,
            lti_consumer=self.lti_consumer
        )

    def tearDown(self):
        SkillGrade.objects.all().delete()
        Skill.objects.all().delete()

    def _set_up_grades(self, grade, lms_marked_for_scoring=True):
        self.drawing_tool = DrawingToolFactory.create(
            lti_consumer=self.lti_consumer,
            resource_link_id='test-id1',
        )
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
            lis_outcome_service_url="https://service.com" if lms_marked_for_scoring else None,
        )
        self.skill = SkillFactory(
            drawing_tool=self.drawing_tool,
        )
        if grade is not None:
            self.skill_grade = SkillGradeFactory.create(
                student_result=self.student_result,
                skill=self.skill,
                grade=grade,
            )

    @data(
        (
            "1. A tool is marked for scoring, grades are ready and successfully pushed to edX, "
            "student result is marked as successfully sent to edX.",
            {
                "lms_marked_for_scoring": True,  # A component is marked for scoring in LMS.
                "grade": 50,
            },
            {
                "grade_is_sent": True,
                "lms_response": MockResponse({}, requests.codes.ok),
                "grade_sending_status": True,
            },
        ),
        (
            "2. Grades are ready, but a tool is not marked for scoring in LMS, "
            "so grades are not sent.",
            {
                "lms_marked_for_scoring": False,
                "grade": 50,
            },
            {
                "grade_is_sent": False,
                "lms_response": None,
                "grade_sending_status": False,
            },
        ),
        (
            "3. Grades are not ready (there was no grading), so nothing is sent to edX.",
            {
                "lms_marked_for_scoring": True,
                "grade": None,
            },
            {
                "grade_is_sent": False,
                "lms_response": None,
                "grade_sending_status": False,
            },
        ),
        (
            "4. Grade is ready and a component is marked for scoring in LMS, but edX is down.",
            {
                "lms_marked_for_scoring": True,
                "grade": 50,
            },
            {
                "grade_is_sent": True,
                "lms_response": MockResponse({}, requests.codes.service_unavailable),
                "grade_sending_status": False,
            },
        ),
        (
            "5. A tool is marked for scoring, student's work was graded at minimum score (zero), "
            "student result is marked as successfully sent to edX.",
            {
                "lms_marked_for_scoring": True,
                "grade": 0,
            },
            {
                "grade_is_sent": True,
                "lms_response": MockResponse({}, requests.codes.ok),
                "grade_sending_status": True,
            },
        ),
    )
    @unpack
    def test_score_pushing(self, description, input_instructions, output, mocked):
        """
        Test scores pushing to the campus system.

        Cases:

        #. Grades are ready and successfully pushed to edX, student result is marked with code 200.
        #. Grades are ready, but a tool is not marked for scoring in LMS, so grades are not sent.
        #. Grades are not ready, so nothing is sent to edX.
        #. Grade is ready and a component is marked for scoring in LMS, but edX is down.
        #. Student's work was graded at minimum score (zero), the grade is sent to edX.
        """
        self._set_up_grades(
            lms_marked_for_scoring=input_instructions["lms_marked_for_scoring"],
            grade=input_instructions["grade"],
        )
        mocked.return_value = output["lms_response"]
        send_outcomes()
        self.assertEqual(mocked.call_count, 1 if output["grade_is_sent"] else 0)
        stud_res = StudentResult.objects.last()
        self.assertEqual(stud_res.grade_sending_status, output["grade_sending_status"])
