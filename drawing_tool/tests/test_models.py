import shutil
import tempfile

import mock
from django.core.files import File
from django.test import TestCase, override_settings

from drawing_tool.models import DrawingTool, Skill, SkillGrade
from drawing_tool.tests.factories import (DrawingToolFactory, StudentResultFactory,
                                          SkillFactory, SkillGradeFactory)
from lti.tests.factories import LTIConsumerFactory, LTIUserFactory


MEDIA_ROOT = tempfile.mkdtemp()


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class DrawingToolTestCase(TestCase):
    """
    Tests DrawingTool model logic.
    """

    def setUp(self):
        """
        Initialize all necessary variables.
        """
        lti_consumer = LTIConsumerFactory.create()

        self.file_mock = mock.MagicMock(spec=File)
        self.file_mock.name = 'test.file'

        self.test_data = {
            'display_name': 'Task',
            'question': 'Follow instructions ";".',
            'hints': '||Finish your thoughts|| ||Don\'t leave empty||',
            'enable_grading': True,
            'resource_link_id': 'unittest-id1',
            'lti_consumer': lti_consumer,
            'answer': self.file_mock,
            'show_answer': True
        }

    @classmethod
    def tearDownClass(cls):
        """
        Remove test media folder after tests.
        """
        shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()

    def test_show_answer(self):
        """
        Test show_answer checkbox toggles to False if no answer file was uploaded.
        """
        tool_with_answer = DrawingTool.objects.create(**self.test_data)

        self.test_data['answer'] = ""
        self.test_data['resource_link_id'] = "unittest-id2"
        tool_without_answer = DrawingTool.objects.create(**self.test_data)

        self.assertTrue(tool_with_answer.show_answer)
        self.assertFalse(tool_without_answer.show_answer)


class StudentResultTestCase(TestCase):
    """
    Tests StudentResult model logic.
    """

    def setUp(self):
        """
        Initialize all necessary variables.
        """
        self.drawing_tool = DrawingToolFactory()
        self.lti_user = LTIUserFactory()
        self.student_result = StudentResultFactory(
            tool=self.drawing_tool,
            lti_user=self.lti_user,
        )

    def test_one_grade(self):
        """
        Test successful grade calculation for one graded skill.
        """
        skill = SkillFactory(drawing_tool=self.drawing_tool)
        SkillGradeFactory(skill=skill, student_result=self.student_result, grade=90)
        self.assertEqual(self.student_result.grade, 90)

    def test_many_grades(self):
        """
        Test successful grade calculation for multiple graded skills.
        """
        for i in range(3):
            skill = Skill(skill=f"skill_{i}", weight=1, drawing_tool=self.drawing_tool)
            skill.save()
            SkillGrade(skill=skill, student_result=self.student_result, grade=i).save()
        self.assertEqual(self.student_result.grade, 1)

    def test_no_grades(self):
        """
        Test grade is not calculated when no grades are added.
        """
        SkillFactory(drawing_tool=self.drawing_tool)
        self.assertIsNone(self.student_result.grade)

    def test_not_all_skills_graded(self):
        """
        Test grade is not calculated when at least one skill isn't graded.
        """
        Skill(skill="test_skill2", weight=1, drawing_tool=self.drawing_tool).save()
        skill = Skill(skill="test_skill1", weight=1, drawing_tool=self.drawing_tool)
        skill.save()
        SkillGrade(skill=skill, student_result=self.student_result, grade=90)
        self.assertIsNone(self.student_result.grade)

    def test_zero_weights(self):
        """
        Test grade is 0 when skill weight set to 0.
        """
        skill = SkillFactory(skill="test_skill1", weight=0, drawing_tool=self.drawing_tool)
        SkillGradeFactory(skill=skill, student_result=self.student_result, grade=90)
        self.assertEqual(self.student_result.grade, 0)
