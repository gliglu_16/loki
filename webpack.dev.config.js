const path = require( 'path' )
const webpack = require( 'webpack' )

module.exports = {
  mode: 'development',
  entry: {
    frontend: './frontend/src/index.js',
  },
  output: {
    path: path.resolve( __dirname, 'frontend/dist/js/' ),
    filename: 'frontend.js',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin( {
      Cookies: 'js-cookie',
      Promise: ['es6-promise', 'Promise'],
    } ),
  ],
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      include: [path.resolve( __dirname, 'frontend/src' )],
      exclude: [
        path.resolve( __dirname, 'frontend/src/*/tests/' ),
      ],
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-react'],
      },
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader',
        'postcss-loader',
      ],
    },
        {
               test: /\.(ttf|otf|woff|eot)$/,
               type: 'asset', 
             },
        {
               test: /\.(jpg|jpeg|svg)$/,
               type: 'asset', 
             },
    ],
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve( __dirname, 'frontend/static' ),
      path.resolve( __dirname, 'frontend/src' ),
    ],
  },
}
