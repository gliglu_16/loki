Loki
===========

Loki is the LTI provider.

Local development environment
-------------------------------

To build and run project locally for development:
::

    make run_dev

To build and run project locally with production settings:
::

    export $(grep -v '^#' envs/prod.env | xargs) && make run_prod

To up all containers:
::

    make up

To develop/debug project locally:
::

    make debug

To build app image:
::

    make build

To restart all containers:
::

    make restart

To get into the app container shell:
::

    make sh

To run tests:
::

    make test

To stop containers:
::

    make stop

To clean/rm stopped containers:
::

    make rm

Maintenance commands
-------------------------------

To run tests:
::

    make test

To run migration:
::

    make update_db

To create new migrations:
::

    make makemigrations

To collect static files:
::

    make collectstatic

Production commands
-------------------------------

To run project on production system make sure that all required environment variables are exported and execute:
::

    make run_prod

To rebuild containers in case of application image update:
::

    make rebuild_prod
