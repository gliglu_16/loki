const path = require( 'path' )
const webpack = require( 'webpack' )
const HtmlWebpackPlugin = require( 'html-webpack-plugin' )
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' )

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: {
    frontend: './frontend/src/index.js',
  },
  output: {
    path: path.resolve( __dirname, 'frontend/dist/js/' ),
    filename: 'frontend.js',
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      include: [path.resolve( __dirname, 'frontend/src' )],
      exclude: [
        path.resolve( __dirname, 'frontend/src/*/tests/' ),
      ],
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-react'],
      },
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader',
        'postcss-loader',
      ],
    },
    {
      test: /\.(ttf|otf|woff|eot)$/,
      loader: 'url-loader?importLoaders=1&limit=10000000',
      include: path.join( __dirname, 'frontend/static/fonts' ),
    },
    {
      test: /\.(jpg|jpeg|svg)$/,
      loader: 'url-loader?importLoaders=1&limit=10000000',
    },
    ],
  },
  plugins: [
    new webpack.ProvidePlugin( {
      Cookies: 'js-cookie',
      Promise: ['es6-promise', 'Promise'],
    } ),
    new MiniCssExtractPlugin( {
      filename: '[name].css',
      chunkFilename: '[id].css',
    } ),
    new HtmlWebpackPlugin( {
      title: 'LOKI',
      hash: true,
    } ),
  ],
  resolve: {
    modules: [
      'node_modules',
      path.resolve( __dirname, 'frontend/static' ),
      path.resolve( __dirname, 'frontend/src' ),
    ],
  },
}
